// dtkThreeDee+PointSetAlgorithm.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QQmlEngine>

#include <vtkPointSetAlgorithm.h>

#include "dtkThreeDee+Algorithm.hpp"

class dtkThreeDeePointSetAlgorithm : public dtkThreeDeeAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(PointSetAlgorithm)
    QML_UNCREATABLE("")

public:
    dtkThreeDeePointSetAlgorithm(vtkSmartPointer<vtkPointSetAlgorithm>);
};

//
// dtkThreeDee+PointSetAlgorithm.hpp ends here
