// dtkThreeDee+PLYReader.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+AbstractPolyDataReader.hpp"

#include <QQmlEngine>

#include <vtkPLYReader.h>

class dtkThreeDeePLYReader : public dtkThreeDeeAbstractPolyDataReader
{
    Q_OBJECT

    QML_NAMED_ELEMENT(PLYReader)

public:
    dtkThreeDeePLYReader();
};

//
// dtkThreeDee+PLYReader.hpp ends here
