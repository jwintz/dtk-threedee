// dtkThreeDee+OBJReader.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+AbstractPolyDataReader.hpp"

#include <QQmlEngine>

#include <vtkOBJReader.h>

class dtkThreeDeeOBJReader : public dtkThreeDeeAbstractPolyDataReader
{
    Q_OBJECT

    QML_NAMED_ELEMENT(OBJReader)

public:
    dtkThreeDeeOBJReader();
};

//
// dtkThreeDee+OBJReader.hpp ends here
