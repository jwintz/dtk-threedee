// dtkThreeDee+FboRenderer.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+FboRenderer.hpp"
#include "dtkThreeDee+FboOffscreenWindow.hpp"
#include "dtkThreeDee+Viewer.hpp"

#ifdef _MSC_VER
#include "dtkThreeDee+Win32Interactor.hpp"
#endif

#ifdef __linux__
#include "dtkThreeDee+GenericInteractor.hpp"
#endif

#include <vtkRendererCollection.h>
#include <vtkCamera.h>
#include <vtkCommand.h>

dtkThreeDeeFboRenderer::dtkThreeDeeFboRenderer(dtkThreeDeeFboOffscreenWindow *offscreenWindow) : m_fboOffscreenWindow(offscreenWindow), m_fbo(0) {

    m_fboOffscreenWindow->Register(NULL);
    m_fboOffscreenWindow->QtParentRenderer = this;

#ifdef __APPLE__
    m_interactor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
#endif

#ifdef __linux__
    m_interactor = vtkSmartPointer<dtkThreeDeeGenericInteractor>::New();
#endif

#ifdef _MSC_VER
    m_interactor = vtkSmartPointer<dtkThreeDeeWin32Interactor>::New();
#endif

    m_interactorStyle = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();

    m_interactor->SetRenderWindow(offscreenWindow);
    m_interactor->SetInteractorStyle(m_interactorStyle);
}

auto dtkThreeDeeFboRenderer::synchronize(QQuickFramebufferObject* fbo) -> void {

    if (!m_fbo) {
        auto viewer = static_cast<dtkThreeDeeViewer*>(fbo);
        viewer->init();
    }
}

auto dtkThreeDeeFboRenderer::render() -> void {

    m_interactor->Disable();
    m_fboOffscreenWindow->PushState();
    m_fboOffscreenWindow->OpenGLInitState();
    m_fboOffscreenWindow->InternalRender();
    m_fboOffscreenWindow->PopState();
    m_interactor->Enable();
}

auto dtkThreeDeeFboRenderer::createFramebufferObject(const QSize& size) -> QOpenGLFramebufferObject * {

    QOpenGLFramebufferObjectFormat format;
    format.setAttachment(QOpenGLFramebufferObject::Depth);
    m_fbo = new QOpenGLFramebufferObject(size, format);
    m_fboOffscreenWindow->SetFramebufferObject(m_fbo);

    return m_fbo;
}

auto dtkThreeDeeFboRenderer::onMouseMoveEvent(QMouseEvent* event) -> void
{
    this->m_interactor->SetEventInformationFlipY(event->position().x(), event->position().y(), (event->modifiers() & Qt::ControlModifier), (event->modifiers() & Qt::ShiftModifier));
    this->m_interactor->InvokeEvent(vtkCommand::MouseMoveEvent, event);
}

auto dtkThreeDeeFboRenderer::onMouseEvent(QMouseEvent* event) -> void {

    if(this->m_interactor == nullptr || event == nullptr)
        return;

    if(!this->m_interactor->GetEnabled())
        return;

    this->m_interactor->SetEventInformationFlipY(event->position().x(), event->position().y(), (event->modifiers() & Qt::ControlModifier), (event->modifiers() & Qt::ShiftModifier));

    auto command = vtkCommand::NoEvent;

    if (event->type() == QEvent::MouseButtonPress || event->type() == QEvent::MouseButtonDblClick)
    {
        switch (event->button()) {
            case Qt::LeftButton:    command = vtkCommand::LeftButtonPressEvent; break;
            case Qt::RightButton:   command = vtkCommand::RightButtonPressEvent; break;
            case Qt::MiddleButton:  command = vtkCommand::MiddleButtonPressEvent; break;
            default: break;
        }
    } else if (event->type() == QEvent::MouseButtonRelease) {
        switch (event->button()) {
            case Qt::LeftButton:    command = vtkCommand::LeftButtonReleaseEvent; break;
            case Qt::RightButton:   command = vtkCommand::RightButtonReleaseEvent; break;
            case Qt::MiddleButton:  command = vtkCommand::MiddleButtonReleaseEvent; break;
            default: break;
        }
    }

    this->m_interactor->InvokeEvent(command, event);
}

auto dtkThreeDeeFboRenderer::onWheelEvent(QWheelEvent* event) -> void {

    if(this->m_interactor == nullptr || event == nullptr)
        return;

    if(!this->m_interactor->GetEnabled())
        return;

    auto command = vtkCommand::NoEvent;
    //qDebug()<<Q_FUNC_INFO<<event->angleDelta();
    if (event->angleDelta().y() > 0) {
        command = vtkCommand::MouseWheelForwardEvent;
    } else {
        command = vtkCommand::MouseWheelBackwardEvent;
    }

    this->m_interactor->InvokeEvent(command, event);
}

dtkThreeDeeFboRenderer::~dtkThreeDeeFboRenderer(void) {
    m_fboOffscreenWindow->QtParentRenderer = 0;
    m_fboOffscreenWindow->Delete();
}

//
// dtkThreeDee+FboRenderer.cpp ends here
