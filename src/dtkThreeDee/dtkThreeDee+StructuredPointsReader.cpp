// dtkThreeDee+StructuredPointsReader.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+StructuredPointsReader.hpp"

dtkThreeDeeStructuredPointsReader::dtkThreeDeeStructuredPointsReader() : dtkThreeDeeDataReader(vtkSmartPointer<vtkStructuredPointsReader>::New())
{

}

//
// dtkThreeDee+StructuredPointsReader.cpp ends here
