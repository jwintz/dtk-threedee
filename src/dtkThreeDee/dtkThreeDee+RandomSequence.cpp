// dtkThreeDee+RandomSequence.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+RandomSequence.hpp"

dtkThreeDeeRandomSequence::dtkThreeDeeRandomSequence(vtkSmartPointer<vtkRandomSequence> vtkObject) : dtkThreeDeeObject(dtkThreeDeeObject::Type::Other), m_object(vtkObject)
{

}

auto dtkThreeDeeRandomSequence::get() -> vtkSmartPointer<vtkRandomSequence>
{
    return this->m_object;
}

auto dtkThreeDeeRandomSequence::setSeed(int seed) -> void
{
    this->m_seed = seed;
    this->m_object->Initialize(seed);
}

auto dtkThreeDeeRandomSequence::getSeed() -> int
{
    return this->m_seed;
}

auto dtkThreeDeeRandomSequence::initialize() -> void
{
    if (this->m_object != nullptr) {
        this->m_object->Initialize(this->m_seed);
    }
}

//
// dtkThreeDee+RandomSequence.cpp ends here
