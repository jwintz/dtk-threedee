// dtkThreeDee+Vector3.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Vector3.hpp"

dtkThreeDeeVector3::dtkThreeDeeVector3(cb_t&& cb, array_t values) : m_callback(cb), m_values(values)
{

}

void dtkThreeDeeVector3::notify(void)
{
    this->m_callback.operator()();
}

void dtkThreeDeeVector3::setX(double x)
{
    this->m_values[0] = x;
    emit this->xChanged();
    this->notify();
}

void dtkThreeDeeVector3::setY(double y)
{
    this->m_values[1] = y;
    emit this->yChanged();
    this->notify();
}

void dtkThreeDeeVector3::setZ(double z)
{
    this->m_values[2] = z;
    emit this->zChanged();
    this->notify();
}

double dtkThreeDeeVector3::getX(void)
{
    return this->m_values[0];
}

double dtkThreeDeeVector3::getY(void)
{
    return this->m_values[1];
}

double dtkThreeDeeVector3::getZ(void)
{
    return this->m_values[2];
}

dtkThreeDeeVector3::array_t dtkThreeDeeVector3::getValues(void)
{
    return this->m_values;
}

//
// dtkThreeDee+Vector3.cpp ends here
