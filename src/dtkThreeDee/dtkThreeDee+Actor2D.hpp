// dtkThreeDee+Actor2D.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Mapper2D.hpp"
#include "dtkThreeDee+Prop.hpp"
#include "dtkThreeDee+Property2D.hpp"

#include <QQmlEngine>

#include <vtkSmartPointer.h>
#include <vtkActor2D.h>

class dtkThreeDeeActor2D : public dtkThreeDeeProp
{
    Q_OBJECT

    Q_PROPERTY(dtkThreeDeeProperty2D* property READ getProperty CONSTANT);
    Q_PROPERTY(dtkThreeDeeMapper2D* mapper READ getMapper WRITE setMapper NOTIFY mapperChanged);
    Q_PROPERTY(qreal width READ getWidth WRITE setWidth NOTIFY widthChanged);
    Q_PROPERTY(qreal height READ getHeight WRITE setHeight NOTIFY heightChanged);

    Q_CLASSINFO("DefaultProperty", "mapper");

    QML_NAMED_ELEMENT(Actor2D)

public:
             dtkThreeDeeActor2D(void);
             dtkThreeDeeActor2D(vtkSmartPointer<vtkActor2D>);
    virtual ~dtkThreeDeeActor2D(void);

    vtkSmartPointer<vtkActor2D> get(void);
    dtkThreeDeeProperty2D *getProperty(void);
    void setMapper(dtkThreeDeeMapper2D *);
    dtkThreeDeeMapper2D *getMapper(void);
    void setWidth(qreal);
    qreal getWidth(void);
    void setHeight(qreal);
    qreal getHeight(void);

signals:
    void widthChanged(void);
    void heightChanged(void);
    void mapperChanged(void);

private:
    vtkSmartPointer<vtkActor2D> m_object = nullptr;

private:
    dtkThreeDeeMapper2D* m_mapper = nullptr;
    dtkThreeDeeProperty2D* m_property = nullptr;
};

//
// dtkThreeDee+Actor2D.hpp ends here
