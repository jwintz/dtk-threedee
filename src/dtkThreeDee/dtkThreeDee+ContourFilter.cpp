// dtkThreeDee+ContourFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ContourFilter.hpp"

dtkThreeDeeContourFilter::dtkThreeDeeContourFilter(void) : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkContourFilter>::New())
{
    this->m_object = vtkContourFilter::SafeDownCast(dtkThreeDeePolyDataAlgorithm::get());
}

dtkThreeDeeContourFilter::dtkThreeDeeContourFilter(vtkSmartPointer<vtkContourFilter> vtkObject) : dtkThreeDeePolyDataAlgorithm(vtkObject)
{
    this->m_object = vtkContourFilter::SafeDownCast(dtkThreeDeePolyDataAlgorithm::get());
}

auto dtkThreeDeeContourFilter::get(void) -> vtkSmartPointer<vtkContourFilter>
{
    return this->m_object;
}

dtkThreeDeeContourFilter::~dtkThreeDeeContourFilter(void)
{
    this->m_object = nullptr;
}

//
// dtkThreeDee+ContourFilter.cpp ends here
