// dtkThreeDee+AdaptiveSubdivisionFilter.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkAdaptiveSubdivisionFilter.h>

class dtkThreeDeeAdaptiveSubdivisionFilter : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(qreal maximumEdgeLength READ getMaximumEdgeLength WRITE setMaximumEdgeLength NOTIFY maximumEdgeLengthChanged);
    Q_PROPERTY(qreal maximumTriangleArea READ getMaximumTriangleArea WRITE setMaximumTriangleArea NOTIFY maximumTriangleAreaChanged);
    Q_PROPERTY(int maximumNumberOfTriangles READ getMaximumNumberOfTriangles WRITE setMaximumNumberOfTriangles NOTIFY maximumNumberOfTrianglesChanged);
    Q_PROPERTY(int maximumNumberOfPasses READ getMaximumNumberOfPasses WRITE setMaximumNumberOfPasses NOTIFY maximumNumberOfPassesChanged);

    QML_NAMED_ELEMENT(AdaptiveSubdivisionFilter)

public:
    dtkThreeDeeAdaptiveSubdivisionFilter(void);

    auto setMaximumEdgeLength(qreal) -> void;
    auto getMaximumEdgeLength() -> qreal;
    auto setMaximumTriangleArea(qreal) -> void;
    auto getMaximumTriangleArea() -> qreal;
    auto setMaximumNumberOfTriangles(int) -> void;
    auto getMaximumNumberOfTriangles() -> int;
    auto setMaximumNumberOfPasses(int) -> void;
    auto getMaximumNumberOfPasses() -> int;

signals:
    void maximumEdgeLengthChanged(void);
    void maximumTriangleAreaChanged(void);
    void maximumNumberOfTrianglesChanged(void);
    void maximumNumberOfPassesChanged(void);

private:
    vtkSmartPointer<vtkAdaptiveSubdivisionFilter> m_object = nullptr;
};

//
// dtkThreeDee+AdaptiveSubdivisionFilter.hpp ends here
