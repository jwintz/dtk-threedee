// dtkThreeDee+RibbonFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+RibbonFilter.hpp"

dtkThreeDeeRibbonFilter::dtkThreeDeeRibbonFilter() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkRibbonFilter>::New()) {
    this->m_object = vtkRibbonFilter::SafeDownCast(dtkThreeDeeAlgorithm::get());

    this->m_defaultNormal = new dtkThreeDeeVector3([this](){
        this->m_object->SetDefaultNormal(this->m_defaultNormal->getValues().data());
        this->update();
    });
}

auto dtkThreeDeeRibbonFilter::setVaryWidth(bool varyWidth) -> void {
    this->m_object->SetVaryWidth(varyWidth);
    this->update();
    emit this->varyWidthChanged();
}

auto dtkThreeDeeRibbonFilter::getVaryWidth() -> bool {
    return this->m_object->GetVaryWidth();
}

auto dtkThreeDeeRibbonFilter::setGenerateTCoords(TCoords tCoords) -> void {
    this->m_object->SetGenerateTCoords(tCoords);
    this->update();
    emit this->generateTCoordsChanged();
}

auto dtkThreeDeeRibbonFilter::getGenerateTCoords() -> TCoords {
    return static_cast<TCoords>(this->m_object->GetGenerateTCoords());
}

auto dtkThreeDeeRibbonFilter::setWidth(qreal width) -> void {
    this->m_object->SetWidth(width);
    this->update();
    emit this->widthChanged();
}

auto dtkThreeDeeRibbonFilter::getWidth() -> qreal {
    return this->m_object->GetWidth();
}

auto dtkThreeDeeRibbonFilter::setTextureLength(qreal textureLength) -> void {
    this->m_object->SetTextureLength(textureLength);
    this->update();
    emit this->textureLengthChanged();
}

auto dtkThreeDeeRibbonFilter::getTextureLength() -> qreal {
    return this->m_object->GetTextureLength();
}

auto dtkThreeDeeRibbonFilter::setWidthFactor(qreal widthFactor) -> void {
    this->m_object->SetWidthFactor(widthFactor);
    this->update();
    emit this->widthFactorChanged();
}

auto dtkThreeDeeRibbonFilter::getWidthFactor() -> qreal {
    return this->m_object->GetWidthFactor();
}

auto dtkThreeDeeRibbonFilter::setUseDefaultNormal(bool useDefaultNormal) -> void {
    this->m_object->SetUseDefaultNormal(useDefaultNormal);
    this->update();
    emit this->useDefaultNormalChanged();
}

auto dtkThreeDeeRibbonFilter::getUseDefaultNormal() -> bool {
    return this->m_object->GetUseDefaultNormal();
}

auto dtkThreeDeeRibbonFilter::getDefaultNormal() -> dtkThreeDeeVector3* {
    return this->m_defaultNormal;
}

//
// dtkThreeDee+RibbonFilter.cpp ends here
