// dtkThreeDee+Algorithm.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Object.hpp"

#include <QQmlEngine>

#include <vtkSmartPointer.h>
#include <vtkAlgorithm.h>

class dtkThreeDeeProp;
class dtkThreeDeeDataObject;

class dtkThreeDeeAlgorithm : public dtkThreeDeeObject
{
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<dtkThreeDeeAlgorithm> input READ getInput NOTIFY inputChanged);

    Q_CLASSINFO("DefaultProperty", "input");

    QML_NAMED_ELEMENT(Algorithm)
    QML_UNCREATABLE("")

public:
     dtkThreeDeeAlgorithm(void) = delete;
     dtkThreeDeeAlgorithm(vtkSmartPointer<vtkAlgorithm>);
    ~dtkThreeDeeAlgorithm(void);

    auto setProp(dtkThreeDeeProp*) -> void;
    auto setVtkAlgorithm(vtkSmartPointer<vtkAlgorithm>) -> void;
    auto getInput() -> QQmlListProperty<dtkThreeDeeAlgorithm>;
    auto update() -> void;
    auto get() -> vtkSmartPointer<vtkAlgorithm>;
    virtual auto isValid() -> bool;
    static auto appendInput(QQmlListProperty<dtkThreeDeeAlgorithm>*, dtkThreeDeeAlgorithm*) -> void;
#if QT_VERSION >= 0x060000
    static auto inputCount(QQmlListProperty<dtkThreeDeeAlgorithm>*) -> qsizetype;
#else
    static auto inputCount(QQmlListProperty<dtkThreeDeeAlgorithm>*) -> int;
#endif
#if QT_VERSION >= 0x060000
    static auto inputAt(QQmlListProperty<dtkThreeDeeAlgorithm>*, qsizetype) -> dtkThreeDeeAlgorithm*;
#else
    static auto inputAt(QQmlListProperty<dtkThreeDeeAlgorithm>*, int) -> dtkThreeDeeAlgorithm*;
#endif
    static auto clearInputs(QQmlListProperty<dtkThreeDeeAlgorithm>*) -> void;

signals:
    void inputChanged(void);

private:
    QList<dtkThreeDeeAlgorithm*> m_input;
    vtkSmartPointer<vtkAlgorithm> m_object = nullptr;
    dtkThreeDeeProp* m_prop = nullptr;
};

//
// dtkThreeDee+Algorithm.hpp ends here
