// dtkThreeDee+PointSource.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"
#include "dtkThreeDee+RandomSequence.hpp"
#include "dtkThreeDee+Vector3.hpp"

#include <QQmlEngine>

#include <vtkPointSource.h>

class dtkThreeDeePointSource : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(PointSource)

public:
    enum Distribution {
        UniformDistribution = VTK_POINT_UNIFORM,
        ShellDistribution = VTK_POINT_SHELL
    };

private:
    Q_ENUM(Distribution);
    Q_PROPERTY(dtkThreeDeeVector3* center READ getCenter CONSTANT);
    Q_PROPERTY(Distribution distribution READ getDistribution WRITE setDistribution NOTIFY distributionChanged);
    Q_PROPERTY(dtkThreeDeeRandomSequence* randomSequence READ getRandomSequence WRITE setRandomSequence NOTIFY randomSequenceChanged);
    Q_PROPERTY(int numberOfPoints READ getNumberOfPoints WRITE setNumberOfPoints NOTIFY numberOfPointsChanged);
    Q_PROPERTY(qreal radius READ getRadius WRITE setRadius NOTIFY radiusChanged);
    Q_CLASSINFO("DefaultProperty", "randomSequence");

private:
    vtkSmartPointer<vtkPointSource> m_object = nullptr;
    dtkThreeDeeVector3* m_center = nullptr;
    dtkThreeDeeRandomSequence* m_randomSequence = nullptr;

private:
    auto resetSeed() -> void;

public:
    dtkThreeDeePointSource();
    auto setDistribution(Distribution) -> void;
    auto getDistribution() -> Distribution;
    auto setNumberOfPoints(int) -> void;
    auto getNumberOfPoints() -> int;
    auto setRadius(qreal) -> void;
    auto getRadius() -> qreal;
    auto getCenter() -> dtkThreeDeeVector3*;
    auto setRandomSequence(dtkThreeDeeRandomSequence*) -> void;
    auto getRandomSequence() -> dtkThreeDeeRandomSequence*;

signals:
    void randomSequenceChanged();
    void distributionChanged();
    void numberOfPointsChanged();
    void radiusChanged();
};

//
// dtkThreeDee+PointSource.hpp ends here
