// dtkThreeDee+ColorTransferFunction.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ColorTransferFunction.hpp"

dtkThreeDeeColorTransferFunction::dtkThreeDeeColorTransferFunction(vtkSmartPointer<vtkColorTransferFunction> vtkObject, cb_t&& callback) : m_object(vtkObject), m_callback(callback)
{

}

void dtkThreeDeeColorTransferFunction::clear() {
    this->m_colors.clear();
    this->m_values.clear();

    emit this->sizeChanged();

    this->m_object->RemoveAllPoints();
    this->update();
}

void dtkThreeDeeColorTransferFunction::add(const QColor& color, double value) {
    this->m_colors.append(color);
    this->m_values.append(value);

    emit this->sizeChanged();

    this->m_object->AddRGBPoint(value, color.redF(), color.greenF(), color.blueF());
    this->update();
}

double dtkThreeDeeColorTransferFunction::getValue(int i) {
    return i < this->m_values.length() && i >= 0 ? this->m_values.at(i) : 0;
}

QColor dtkThreeDeeColorTransferFunction::getColor(int i) {
    return i < this->m_colors.length() && i >= 0 ? QColor(this->m_colors.at(i)) : QColor("#ff00ff");
}

auto dtkThreeDeeColorTransferFunction::update() -> void {
    this->m_callback();
}

auto dtkThreeDeeColorTransferFunction::getValues() -> QList<double> {
    return this->m_values;
}

auto dtkThreeDeeColorTransferFunction::getColors() -> QList<QColor> {
    return this->m_colors;
}

auto dtkThreeDeeColorTransferFunction::setClamping(bool clamping) -> void {
    this->m_object->SetClamping(clamping);
    this->update();
}

auto dtkThreeDeeColorTransferFunction::getClamping() -> bool {
    return this->m_object->GetClamping();
}

auto dtkThreeDeeColorTransferFunction::getSize() -> int {
    return this->m_values.length();
}

//
// dtkThreeDee+ColorTransferFunction.cpp ends here
