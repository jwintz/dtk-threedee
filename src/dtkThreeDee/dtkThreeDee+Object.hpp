// dtkThreeDee+Object.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QObject>
#include <QQmlEngine>

class dtkThreeDeeObject : public QObject
{
    Q_OBJECT

    QML_NAMED_ELEMENT(Object)
    QML_UNCREATABLE("")

public:
    enum class Type {
        Data,
        Prop,
        Algorithm,
        Widget,
        Other
    };

public:
    dtkThreeDeeObject(Type);

public:
    Type getType(void);

private:
    Type m_type;
};

//
// dtkThreeDee+Object.hpp ends here
