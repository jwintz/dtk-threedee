// dtkThreeDee+RotationalExtrusionFilter.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkRotationalExtrusionFilter.h>

class dtkThreeDeeRotationalExtrusionFilter : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(int resolution READ getResolution WRITE setResolution NOTIFY resolutionChanged);
    Q_PROPERTY(bool capping READ getCapping WRITE setCapping NOTIFY cappingChanged);
    Q_PROPERTY(qreal angle READ getAngle WRITE setAngle NOTIFY angleChanged);
    Q_PROPERTY(qreal translation READ getTranslation WRITE setTranslation NOTIFY translationChanged);
    Q_PROPERTY(qreal deltaRadius READ getDeltaRadius WRITE setDeltaRadius NOTIFY deltaRadiusChanged);

    QML_NAMED_ELEMENT(RotationalExtrusionFilter)

private:
    vtkSmartPointer<vtkRotationalExtrusionFilter> m_object = nullptr;

public:
    dtkThreeDeeRotationalExtrusionFilter();
    auto setResolution(int) -> void;
    auto getResolution() -> int;
    auto setCapping(bool) -> void;
    auto getCapping() -> bool;
    auto setAngle(qreal) -> void;
    auto getAngle() -> qreal;
    auto setTranslation(qreal) -> void;
    auto getTranslation() -> qreal;
    auto setDeltaRadius(qreal) -> void;
    auto getDeltaRadius() -> qreal;

signals:
    void resolutionChanged();
    void cappingChanged();
    void angleChanged();
    void translationChanged();
    void deltaRadiusChanged();
};

//
// dtkThreeDee+RotationalExtrusionFilter.hpp ends here
