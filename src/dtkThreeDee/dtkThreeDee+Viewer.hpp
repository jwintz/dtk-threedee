// dtkThreeDee+Viewer.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QImage>
#include <QQuickFramebufferObject>
#include <QQmlEngine>

#include <vtkSmartPointer.h>
#include <vtkActor.h>
#include <vtkProp.h>
#include <vtkImageData.h>

class vtkGenericOpenGLRenderWindow;

class dtkThreeDeeObject;
class dtkThreeDeeFboRenderer;
class dtkThreeDeeFboOffscreenWindow;

class dtkThreeDeeViewer : public QQuickFramebufferObject
{
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<dtkThreeDeeObject> input READ getInput NOTIFY inputChanged);
    Q_PROPERTY(bool hoverEnabled READ getHoverEnabled WRITE setHoverEnabled NOTIFY hoverEnabledChanged);
    Q_PROPERTY(bool mouseEnabled READ getMouseEnabled WRITE setMouseEnabled NOTIFY mouseEnabledChanged);

    Q_CLASSINFO("DefaultProperty", "input");

    QML_NAMED_ELEMENT(Viewer)

signals:
    void initialised(void);
    void captured(void);
    void captureRetrieved(void);

public:
    Q_INVOKABLE void setCameraPosition(double, double, double);
    Q_INVOKABLE void setCameraFocalPoint(double, double, double);
    Q_INVOKABLE void setCameraUp(double, double, double);

    Q_INVOKABLE QList<double> bounds(void);

private:
    friend class dtkThreeDeeFboRenderer;
    QList<dtkThreeDeeObject*> m_input;
    bool m_initialized = false;
    bool m_hoverEnabled = false;
    bool m_mouseEnabled = false;
    bool m_externalEnabled = false;
    vtkSmartPointer<vtkRenderer> m_renderer;

public:
     dtkThreeDeeViewer();
    ~dtkThreeDeeViewer();
    Renderer *createRenderer() const override;
    vtkGenericOpenGLRenderWindow* GetRenderWindow() const;
    vtkRenderer* GetRenderer() const;

public slots:
    Q_INVOKABLE void render(void);

public:
    virtual auto init() -> void;
    auto update() -> void;
    auto removeData(dtkThreeDeeObject*) -> void;
    auto setHoverEnabled(bool) -> void;
    auto setMouseEnabled(bool) -> void;
    auto setExternalEnabled(bool) -> void;
    auto getInput() -> QQmlListProperty<dtkThreeDeeObject>;
    auto getHoverEnabled() -> bool;
    auto getMouseEnabled() -> bool;

    static auto appendInput(QQmlListProperty<dtkThreeDeeObject>*, dtkThreeDeeObject*) -> void;
#if QT_VERSION >= 0x060000
    static auto inputCount(QQmlListProperty<dtkThreeDeeObject>*) -> qsizetype;
#else
    static auto inputCount(QQmlListProperty<dtkThreeDeeObject>*) -> int;
#endif
#if QT_VERSION >= 0x060000
    static auto inputAt(QQmlListProperty<dtkThreeDeeObject>*, qsizetype) -> dtkThreeDeeObject*;
#else
    static auto inputAt(QQmlListProperty<dtkThreeDeeObject>*, int) -> dtkThreeDeeObject*;
#endif
    static auto clearInputs(QQmlListProperty<dtkThreeDeeObject>*) -> void;

protected:
    mutable dtkThreeDeeFboRenderer* m_fboRenderer = nullptr;
    dtkThreeDeeFboOffscreenWindow *m_win;
    auto mousePressEvent(QMouseEvent*) -> void override;
    auto mouseReleaseEvent(QMouseEvent*) -> void override;
    auto mouseMoveEvent(QMouseEvent*) -> void override;
    auto hoverMoveEvent(QHoverEvent*) -> void override;
    auto wheelEvent(QWheelEvent*) -> void override;

signals:
    void inputChanged();
    void hoverEnabledChanged();
    void mouseEnabledChanged();
};

//
// dtkThreeDee+Viewer.hpp ends here
