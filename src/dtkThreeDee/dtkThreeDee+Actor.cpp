// dtkThreeDee+Actor.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Actor.hpp"
#include "dtkThreeDee+Mapper.hpp"
#include "dtkThreeDee+Texture.hpp"
#include "dtkThreeDee+Property.hpp"

dtkThreeDeeActor::dtkThreeDeeActor(void) : dtkThreeDeeProp3D(vtkSmartPointer<vtkActor>::New())
{
    this->m_object = vtkActor::SafeDownCast(dtkThreeDeeProp::get());
    this->m_property = new dtkThreeDeeProperty(this);
    this->m_texture = new dtkThreeDeeTexture(this->m_object->GetTexture(), [this] (void) {
        this->update();
    });
}

dtkThreeDeeActor::dtkThreeDeeActor(vtkSmartPointer<vtkActor> vtkObject) : dtkThreeDeeProp3D(vtkObject)
{
    this->m_object = vtkObject;
    this->m_property = new dtkThreeDeeProperty(this);
}

dtkThreeDeeActor::~dtkThreeDeeActor(void)
{
    delete this->m_property;
}

vtkSmartPointer<vtkActor> dtkThreeDeeActor::get(void)
{
    return this->m_object;
}

dtkThreeDeeProperty *dtkThreeDeeActor::getProperty(void)
{
    return this->m_property;
}

void dtkThreeDeeActor::setMapper(dtkThreeDeeMapper *mapper)
{
    this->m_mapper = mapper;
    this->m_object->SetMapper(mapper->get());
    mapper->setProp(this);

    emit this->mapperChanged();

    this->update();
}

dtkThreeDeeMapper *dtkThreeDeeActor::getMapper(void)
{
    return this->m_mapper;
}

dtkThreeDeeTexture *dtkThreeDeeActor::getTexture(void)
{
    return this->m_texture;
}

//
// dtkThreeDee+Actor.cpp ends here
