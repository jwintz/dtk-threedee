// dtkThreeDee+STLReader.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+AbstractPolyDataReader.hpp"

#include <QQmlEngine>

#include <vtkSTLReader.h>

class dtkThreeDeeSTLReader : public dtkThreeDeeAbstractPolyDataReader
{
    Q_OBJECT

    QML_NAMED_ELEMENT(STLReader)

public:
    dtkThreeDeeSTLReader();
};

//
// dtkThreeDee+STLReader.hpp ends here
