// dtkThreeDee+SmartVolumeMapper.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+SmartVolumeMapper.hpp"

dtkThreeDeeSmartVolumeMapper::dtkThreeDeeSmartVolumeMapper() : dtkThreeDeeVolumeMapper(vtkSmartPointer<vtkSmartVolumeMapper>::New()) {
    this->m_object = vtkSmartVolumeMapper::SafeDownCast(this->dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeSmartVolumeMapper::setFinalColorWindow(qreal finalColorWindow) -> void {
    this->m_object->SetFinalColorWindow(finalColorWindow);
    emit this->finalColorWindowChanged();
    this->update();
}

auto dtkThreeDeeSmartVolumeMapper::getFinalColorWindow() -> qreal {
    return this->m_object->GetFinalColorWindow();
}

auto dtkThreeDeeSmartVolumeMapper::setFinalColorLevel(qreal finalColorLevel) -> void {
    this->m_object->SetFinalColorLevel(finalColorLevel);
    emit this->finalColorLevelChanged();
    this->update();
}

auto dtkThreeDeeSmartVolumeMapper::getFinalColorLevel() -> qreal {
    return this->m_object->GetFinalColorLevel();
}

//
// dtkThreeDee+SmartVolumeMapper.cpp ends here
