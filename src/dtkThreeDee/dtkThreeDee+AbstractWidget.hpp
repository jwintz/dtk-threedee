// dtkThreeDee+AbstractWidget.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Object.hpp"

#include <QQmlEngine>

#include <vtkSmartPointer.h>
#include <vtkAbstractWidget.h>

class dtkThreeDeeAbstractWidget : public dtkThreeDeeObject
{
    Q_OBJECT
    Q_PROPERTY(bool enabled READ getEnabled WRITE setEnabled NOTIFY enabledChanged);
    QML_NAMED_ELEMENT(AbstractWidget)
    QML_UNCREATABLE("")

public:
    dtkThreeDeeAbstractWidget(void) = delete;
    dtkThreeDeeAbstractWidget(vtkSmartPointer<vtkAbstractWidget>);

    void setEnabled(bool);
    bool getEnabled(void);

    vtkSmartPointer<vtkAbstractWidget> get(void);

signals:
    void enabledChanged(void);

private:
    bool m_enabled = true;
    vtkSmartPointer<vtkAbstractWidget> m_object = nullptr;
};

//
// dtkThreeDee+AbstractWidget.hpp ends here
