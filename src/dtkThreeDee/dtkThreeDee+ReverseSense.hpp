// dtkThreeDee+ReverseSense.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkReverseSense.h>

class dtkThreeDeeReverseSense : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(bool reverseCells READ getReverseCells WRITE setReverseCells NOTIFY reverseCellsChanged);
    Q_PROPERTY(bool reverseNormals READ getReverseNormals WRITE setReverseNormals NOTIFY reverseNormalsChanged);

    QML_NAMED_ELEMENT(ReverseSense)

private:
    vtkSmartPointer<vtkReverseSense> m_object = nullptr;

public:
    dtkThreeDeeReverseSense();
    auto setReverseCells(bool) -> void;
    auto getReverseCells() -> bool;
    auto setReverseNormals(bool) -> void;
    auto getReverseNormals() -> bool;

signals:
    void reverseCellsChanged();
    void reverseNormalsChanged();
};

//
// dtkThreeDee+ReverseSense.hpp ends here
