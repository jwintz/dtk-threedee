// dtkThreeDee+VolumeMapper.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+VolumeMapper.hpp"

dtkThreeDeeVolumeMapper::dtkThreeDeeVolumeMapper(vtkSmartPointer<vtkVolumeMapper> vtkObject) : dtkThreeDeeAbstractVolumeMapper(vtkObject)
{
    this->m_object = vtkVolumeMapper::SafeDownCast(vtkObject);
}

auto dtkThreeDeeVolumeMapper::setBlendMode(BlendMode blendMode) -> void
{
    this->m_object->SetBlendMode(blendMode);
    emit this->blendModeChanged();
    this->update();
}

auto dtkThreeDeeVolumeMapper::getBlendMode() -> BlendMode
{
    return static_cast<BlendMode>(this->m_object->GetBlendMode());
}

auto dtkThreeDeeVolumeMapper::setCropping(bool cropping) -> void
{
    this->m_object->SetCropping(cropping);
    emit this->croppingChanged();
    this->update();
}

auto dtkThreeDeeVolumeMapper::getCropping() -> bool
{
    return this->m_object->GetCropping();
}

//
// dtkThreeDee+VolumeMapper.cpp ends here
