// dtkThreeDee+AbstractMapper.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Algorithm.hpp"

#include <QQmlEngine>

#include <vtkSmartPointer.h>
#include <vtkAbstractMapper.h>

class dtkThreeDeeAbstractMapper : public dtkThreeDeeAlgorithm
{
    Q_OBJECT
    QML_NAMED_ELEMENT(AbstractMapper)
    QML_UNCREATABLE("")

public:
    dtkThreeDeeAbstractMapper(vtkSmartPointer<vtkAbstractMapper>);
};

//
// dtkThreeDee+AbstractMapper.hpp ends here
