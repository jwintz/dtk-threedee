// dtkThreeDee+BoxWidget2.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+BoxWidget2.hpp"

dtkThreeDeeBoxWidget2::dtkThreeDeeBoxWidget2() : dtkThreeDeeAbstractWidget(vtkSmartPointer<vtkBoxWidget2>::New())
{
    this->m_object = vtkBoxWidget2::SafeDownCast(this->get());
}

//
// dtkThreeDee+BoxWidget2.cpp ends here
