// dtkThreeDee+SampleFunction.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+SampleFunction.hpp"

dtkThreeDeeSampleFunction::dtkThreeDeeSampleFunction(void) : dtkThreeDeeImageAlgorithm(vtkSmartPointer<vtkSampleFunction>::New())
{
    this->m_object = vtkSampleFunction::SafeDownCast(dtkThreeDeeImageAlgorithm::get());

    this->m_sampleDimension = new dtkThreeDeeVector3([this](){
        this->m_object->SetSampleDimensions(this->m_sampleDimension->getX(), this->m_sampleDimension->getY(), this->m_sampleDimension->getZ());
        this->update();
    });

}

dtkThreeDeeSampleFunction::dtkThreeDeeSampleFunction(vtkSmartPointer<vtkSampleFunction> object) : dtkThreeDeeImageAlgorithm(object)
{
    this->m_object = vtkSampleFunction::SafeDownCast(dtkThreeDeeImageAlgorithm::get());
}

auto dtkThreeDeeSampleFunction::get(void) -> vtkSmartPointer<vtkSampleFunction>
{
    return this->m_object;
}

dtkThreeDeeSampleFunction::~dtkThreeDeeSampleFunction(void)
{
    this->m_object = nullptr;
}

//
// dtkThreeDee+SampleFunction.cpp ends here
