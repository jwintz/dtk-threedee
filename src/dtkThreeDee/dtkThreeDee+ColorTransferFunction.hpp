// dtkThreeDee+ColorTransferFunction.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QObject>
#include <QColor>
#include <QList>
#include <QQmlEngine>

#include <vtkSmartPointer.h>
#include <vtkColorTransferFunction.h>

class dtkThreeDeeColorTransferFunction : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int size READ getSize NOTIFY sizeChanged);
    Q_PROPERTY(bool clamping READ getClamping WRITE setClamping NOTIFY clampingChanged);

    QML_NAMED_ELEMENT(ColorTransferFunction)
    QML_UNCREATABLE("")

private:
    using cb_t = std::function<void()>;
    using vtk_t = vtkSmartPointer<vtkColorTransferFunction>;
    vtk_t m_object = nullptr;
    cb_t m_callback;
    QList<double> m_values;
    QList<QColor> m_colors;

public:
    dtkThreeDeeColorTransferFunction(vtk_t, cb_t&&);
    auto update() -> void;
    auto getSize() -> int;
    auto setClamping(bool) -> void;
    auto getClamping() -> bool;
    auto getValues() -> QList<double>;
    auto getColors() -> QList<QColor>;
    public slots:
    void clear();
    void add(const QColor&, double);
    double getValue(int);
    QColor getColor(int);

signals:
    void sizeChanged();
    void clampingChanged();
};

//
// dtkThreeDee+ColorTransferFunction.hpp ends here
