// dtkThreeDee+CylinderSource.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+CylinderSource.hpp"

dtkThreeDeeCylinderSource::dtkThreeDeeCylinderSource() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkCylinderSource>::New())
{
    this->m_object = vtkCylinderSource::SafeDownCast(dtkThreeDeeAlgorithm::get());

    this->m_center = new dtkThreeDeeVector3([this](){
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->update();
    });
}

auto dtkThreeDeeCylinderSource::getCenter() -> dtkThreeDeeVector3* {
    return this->m_center;
}

auto dtkThreeDeeCylinderSource::setHeight(qreal height) -> void {
    this->m_object->SetHeight(height);
    emit this->heightChanged();
    this->update();
}

auto dtkThreeDeeCylinderSource::getHeight() -> qreal {
    return this->m_object->GetHeight();
}

auto dtkThreeDeeCylinderSource::setRadius(qreal radius) -> void {
    this->m_object->SetRadius(radius);
    emit this->radiusChanged();
    this->update();
}

auto dtkThreeDeeCylinderSource::getRadius() -> qreal {
    return this->m_object->GetRadius();
}

auto dtkThreeDeeCylinderSource::setResolution(int resolution) -> void {
    this->m_object->SetResolution(resolution);
    emit this->resolutionChanged();
    this->update();
}

auto dtkThreeDeeCylinderSource::getResolution() -> int {
    return this->m_object->GetResolution();
}

auto dtkThreeDeeCylinderSource::setCapping(bool capping) -> void {
    this->m_object->SetCapping(capping);
    emit this->cappingChanged();
    this->update();
}

auto dtkThreeDeeCylinderSource::getCapping() -> bool {
    return this->m_object->GetCapping();
}

//
// dtkThreeDee+CylinderSource.cpp ends here
