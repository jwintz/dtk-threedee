// dtkThreeDee+ConeSource.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ConeSource.hpp"

dtkThreeDeeConeSource::dtkThreeDeeConeSource() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkConeSource>::New())
{
    this->m_object = vtkConeSource::SafeDownCast(dtkThreeDeeAlgorithm::get());

    this->m_center = new dtkThreeDeeVector3([this](){
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->update();
    });

    this->m_direction = new dtkThreeDeeVector3([this](){
        this->m_object->SetDirection(this->m_direction->getValues().data());
        this->update();
    });
}

auto dtkThreeDeeConeSource::getCenter() -> dtkThreeDeeVector3* {
    return this->m_center;
}

auto dtkThreeDeeConeSource::getDirection() -> dtkThreeDeeVector3* {
    return this->m_direction;
}

auto dtkThreeDeeConeSource::setHeight(qreal height) -> void {
    this->m_object->SetHeight(height);
    emit this->heightChanged();
    emit this->angleChanged();
    this->update();
}

auto dtkThreeDeeConeSource::getHeight() -> qreal {
    return this->m_object->GetHeight();
}

auto dtkThreeDeeConeSource::setRadius(qreal radius) -> void {
    this->m_object->SetRadius(radius);
    emit this->radiusChanged();
    this->update();
}

auto dtkThreeDeeConeSource::getRadius() -> qreal {
    return this->m_object->GetRadius();
}

auto dtkThreeDeeConeSource::setAngle(qreal angle) -> void {
    this->m_object->SetAngle(angle);
    emit this->angleChanged();
    emit this->heightChanged();
    this->update();
}

auto dtkThreeDeeConeSource::getAngle() -> qreal {
    return this->m_object->GetAngle();
}

auto dtkThreeDeeConeSource::setResolution(int resolution) -> void {
    this->m_object->SetResolution(resolution);
    emit this->resolutionChanged();
    this->update();
}

auto dtkThreeDeeConeSource::getResolution() -> int {
    return this->m_object->GetResolution();
}

auto dtkThreeDeeConeSource::setCapping(bool capping) -> void {
    this->m_object->SetCapping(capping);
    emit this->cappingChanged();
    this->update();
}

auto dtkThreeDeeConeSource::getCapping() -> bool {
    return this->m_object->GetCapping();
}

//
// dtkThreeDee+ConeSource.cpp ends here
