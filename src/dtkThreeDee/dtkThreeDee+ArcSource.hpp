// dtkThreeDee+ArcSource.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"
#include "dtkThreeDee+Vector3.hpp"

#include <QQmlEngine>

#include <vtkArcSource.h>

class dtkThreeDeeArcSource : public dtkThreeDeePolyDataAlgorithm
{
  Q_OBJECT

  Q_PROPERTY(dtkThreeDeeVector3 *point1 READ getPoint1 CONSTANT);
  Q_PROPERTY(dtkThreeDeeVector3 *point2 READ getPoint2 CONSTANT);
  Q_PROPERTY(dtkThreeDeeVector3 *center READ getCenter CONSTANT);
  Q_PROPERTY(dtkThreeDeeVector3 *polarVector READ getPolarVector CONSTANT);
  Q_PROPERTY(qreal angle READ getAngle WRITE setAngle NOTIFY angleChanged);
  Q_PROPERTY(int resolution READ getResolution WRITE setResolution NOTIFY resolutionChanged);
  Q_PROPERTY(bool negative READ getNegative WRITE setNegative NOTIFY negativeChanged);
  Q_PROPERTY(bool useNormalAndAngle READ getUseNormalAndAngle WRITE setUseNormalAndAngle NOTIFY useNormalAndAngleChanged);

  QML_NAMED_ELEMENT(ArcSource)

private:
  vtkSmartPointer<vtkArcSource> m_object = nullptr;
  dtkThreeDeeVector3 *m_point1 = nullptr;
  dtkThreeDeeVector3 *m_point2 = nullptr;
  dtkThreeDeeVector3 *m_center = nullptr;
  dtkThreeDeeVector3 *m_polarVector = nullptr;

public:
  dtkThreeDeeArcSource();
  auto getPoint1() -> dtkThreeDeeVector3 *;
  auto getPoint2() -> dtkThreeDeeVector3 *;
  auto getCenter() -> dtkThreeDeeVector3 *;
  auto getPolarVector() -> dtkThreeDeeVector3 *;
  auto setAngle(qreal) -> void;
  auto getAngle() -> qreal;
  auto setResolution(int) -> void;
  auto getResolution() -> int;
  auto setNegative(bool) -> void;
  auto getNegative() -> bool;
  auto setUseNormalAndAngle(bool) -> void;
  auto getUseNormalAndAngle() -> bool;

signals:
  void angleChanged();
  void resolutionChanged();
  void negativeChanged();
  void useNormalAndAngleChanged();
};

//
// dtkThreeDee+ArcSource.hpp ends here
