// dtkThreeDee+DiskSource.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkDiskSource.h>

class dtkThreeDeeDiskSource : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(qreal innerRadius READ getInnerRadius WRITE setInnerRadius NOTIFY innerRadiusChanged);
    Q_PROPERTY(qreal outerRadius READ getOuterRadius WRITE setOuterRadius NOTIFY outerRadiusChanged);
    Q_PROPERTY(int radialResolution READ getRadialResolution WRITE setRadialResolution NOTIFY radialResolutionChanged);
    Q_PROPERTY(int circumferentialResolution READ getCircumferentialResolution WRITE setCircumferentialResolution NOTIFY circumferentialResolutionChanged);

    QML_NAMED_ELEMENT(DiskSource)

private:
    vtkSmartPointer<vtkDiskSource> m_object = nullptr;

public:
    dtkThreeDeeDiskSource();
    auto setInnerRadius(qreal) -> void;
    auto setOuterRadius(qreal) -> void;
    auto setRadialResolution(int) -> void;
    auto setCircumferentialResolution(int) -> void ;
    auto getInnerRadius() -> qreal;
    auto getOuterRadius() -> qreal;
    auto getRadialResolution() -> int;
    auto getCircumferentialResolution() -> int;

signals:
    void innerRadiusChanged();
    void outerRadiusChanged();
    void radialResolutionChanged();
    void circumferentialResolutionChanged();
};

//
// dtkThreeDee+DiskSource.hpp ends here
