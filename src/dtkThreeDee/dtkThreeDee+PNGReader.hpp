// dtkThreeDee+PNGReader.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+ImageReader2.hpp"

#include <QQmlEngine>

#include <vtkPNGReader.h>

class dtkThreeDeePNGReader : public dtkThreeDeeImageReader2
{
    Q_OBJECT

    QML_NAMED_ELEMENT(PNGReader)

public:
    dtkThreeDeePNGReader();
};

//
// dtkThreeDee+PNGReader.hpp ends here
