// dtkThreeDee+StructuredPointsReader.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+DataReader.hpp"

#include <QQmlEngine>

#include <vtkStructuredPointsReader.h>

class dtkThreeDeeStructuredPointsReader : public dtkThreeDeeDataReader
{
    Q_OBJECT

    QML_NAMED_ELEMENT(StructuredPointsReader)

public:
    dtkThreeDeeStructuredPointsReader();
};

//
// dtkThreeDee+StructuredPointsReader.hpp ends here
