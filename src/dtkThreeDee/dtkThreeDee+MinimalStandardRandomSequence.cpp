// dtkThreeDee+MinimalStandardRandomSequence.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+MinimalStandardRandomSequence.hpp"

dtkThreeDeeMinimalStandardRandomSequence::dtkThreeDeeMinimalStandardRandomSequence() : dtkThreeDeeRandomSequence(vtkSmartPointer<vtkMinimalStandardRandomSequence>::New())
{
    this->m_object = vtkMinimalStandardRandomSequence::SafeDownCast(dtkThreeDeeRandomSequence::get());
}    

//
// dtkThreeDee+MinimalStandardRandomSequence.cpp ends here
