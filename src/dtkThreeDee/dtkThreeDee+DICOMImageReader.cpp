// dtkThreeDee+DICOMImageReader.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+DICOMImageReader.hpp"

dtkThreeDeeDICOMImageReader::dtkThreeDeeDICOMImageReader() : dtkThreeDeeImageReader2(vtkSmartPointer<vtkDICOMImageReader>::New()) {
    this->m_object = vtkDICOMImageReader::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeDICOMImageReader::setDirectoryName(const QString& directoryName) -> void {
    this->m_directoryName = directoryName;
    emit this->directoryNameChanged();
    this->m_object->SetDirectoryName(directoryName.toStdString().c_str());
    this->m_object->Update();
    this->update();
}

auto dtkThreeDeeDICOMImageReader::getDirectoryName() -> QString {
    return this->m_directoryName;
}

//
// dtkThreeDee+DICOMImageReader.cpp ends here
