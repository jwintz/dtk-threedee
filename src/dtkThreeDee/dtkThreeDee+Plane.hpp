// dtkThreeDee+Plane.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Vector3.hpp"
#include "dtkThreeDee+ImplicitFunction.hpp"

#include <QQmlEngine>

#include <vtkPlane.h>

class dtkThreeDeePlane : public dtkThreeDeeImplicitFunction
{
    Q_OBJECT

    Q_PROPERTY(dtkThreeDeeVector3* origin READ getOrigin CONSTANT);
    Q_PROPERTY(dtkThreeDeeVector3* normal READ getNormal CONSTANT);

    QML_NAMED_ELEMENT(Plane)

private:
    vtkSmartPointer<vtkPlane> m_object = nullptr;
    dtkThreeDeeVector3* m_origin = nullptr;
    dtkThreeDeeVector3* m_normal = nullptr;

public:
    dtkThreeDeePlane();
    auto getOrigin() -> dtkThreeDeeVector3*;
    auto getNormal() -> dtkThreeDeeVector3*;
};

//
// dtkThreeDee+Plane.hpp ends here
