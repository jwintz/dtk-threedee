// dtkThreeDee+DataSetMapper.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:


#pragma once

#include "dtkThreeDee+Mapper.hpp"
#include "dtkThreeDee+Algorithm.hpp"

#include <QQmlEngine>

#include <vtkDataSetMapper.h>

class dtkThreeDeeDataSetMapper : public dtkThreeDeeMapper
{
    Q_OBJECT

    QML_NAMED_ELEMENT(DataSetMapper)

public:
    dtkThreeDeeDataSetMapper();
};


//
// dtkThreeDee+DataSetMapper.hpp ends here
