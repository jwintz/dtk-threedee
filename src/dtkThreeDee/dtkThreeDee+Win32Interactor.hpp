#pragma once

#if defined(Q_OS_WIN32)

#include <vtkWin32RenderWindowInteractor.h>

class dtkThreeDeeWin32Interactor : public vtkWin32RenderWindowInteractor
{
public:
    vtkTypeMacro(dtkThreeDeeWin32Interactor, vtkWin32RenderWindowInteractor);
    static dtkThreeDeeWin32Interactor *New(void);
    void Initialize(void) override;

protected:
             dtkThreeDeeWin32Interactor(void) = default;
    virtual ~dtkThreeDeeWin32Interactor(void) = default;
};

#endif
