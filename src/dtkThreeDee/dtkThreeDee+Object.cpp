// dtkThreeDee+Object.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Object.hpp"

dtkThreeDeeObject::dtkThreeDeeObject(Type type) : m_type(type)
{

}

dtkThreeDeeObject::Type dtkThreeDeeObject::getType(void)
{
    return this->m_type;
}

//
// dtkThreeDee+Object.cpp ends here
