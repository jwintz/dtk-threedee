// dtkThreeDee+SectorSource.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+SectorSource.hpp"

dtkThreeDeeSectorSource::dtkThreeDeeSectorSource() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkSectorSource>::New()) {
    this->m_object = vtkSectorSource::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeSectorSource::setInnerRadius(qreal radius) -> void {
    this->m_object->SetInnerRadius(radius);
    emit this->innerRadiusChanged();
    this->update();
}

auto dtkThreeDeeSectorSource::getInnerRadius() -> qreal {
    return this->m_object->GetInnerRadius();
}

auto dtkThreeDeeSectorSource::setOuterRadius(qreal radius) -> void {
    this->m_object->SetOuterRadius(radius);
    emit this->outerRadiusChanged();
    this->update();
}

auto dtkThreeDeeSectorSource::getOuterRadius() -> qreal {
    return this->m_object->GetOuterRadius();
}

auto dtkThreeDeeSectorSource::setRadialResolution(int value) -> void {
    this->m_object->SetRadialResolution(value);
    emit this->radialResolutionChanged();
    this->update();
}

auto dtkThreeDeeSectorSource::getRadialResolution() -> int {
    return this->m_object->GetRadialResolution();
}

auto dtkThreeDeeSectorSource::setCircumferentialResolution(int value) -> void {
    this->m_object->SetCircumferentialResolution(value);
    emit this->circumferentialResolutionChanged();
    this->update();
}

auto dtkThreeDeeSectorSource::getCircumferentialResolution() -> int {
    return this->m_object->GetCircumferentialResolution();
}

auto dtkThreeDeeSectorSource::setStartAngle(qreal startAngle) -> void {
    this->m_object->SetStartAngle(startAngle);
    this->update();
    emit this->startAngleChanged();
}

auto dtkThreeDeeSectorSource::getStartAngle() -> qreal {
    return this->m_object->GetStartAngle();
}

auto dtkThreeDeeSectorSource::setEndAngle(qreal endAngle) -> void {
    this->m_object->SetEndAngle(endAngle);
    this->update();
    emit this->endAngleChanged();
}

auto dtkThreeDeeSectorSource::getEndAngle() -> qreal {
    return this->m_object->GetEndAngle();
}

auto dtkThreeDeeSectorSource::setZCoord(qreal zCoord) -> void {
    this->m_object->SetZCoord(zCoord);
    this->update();
    emit this->zCoordChanged();
}

auto dtkThreeDeeSectorSource::getZCoord() -> qreal {
    return this->m_object->GetZCoord();
}

//
// dtkThreeDee+SectorSource.cpp ends here
