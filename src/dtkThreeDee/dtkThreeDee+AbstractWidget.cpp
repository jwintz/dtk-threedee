// dtkThreeDee+AbstractWidget.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+AbstractWidget.hpp"

dtkThreeDeeAbstractWidget::dtkThreeDeeAbstractWidget(vtkSmartPointer<vtkAbstractWidget> vtkObject) : dtkThreeDeeObject(dtkThreeDeeObject::Type::Widget), m_object(vtkObject)
{

}

vtkSmartPointer<vtkAbstractWidget> dtkThreeDeeAbstractWidget::get(void)
{
    return this->m_object;
}

void dtkThreeDeeAbstractWidget::setEnabled(bool enabled)
{
    this->m_object->SetEnabled(enabled);
    emit this->enabledChanged();
}

bool dtkThreeDeeAbstractWidget::getEnabled(void)
{
    return this->m_object->GetEnabled();
}

//
// dtkThreeDee+AbstractWidget.cpp ends here
