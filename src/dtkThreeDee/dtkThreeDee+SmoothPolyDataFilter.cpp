// dtkThreeDee+SmoothPolyDataFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+SmoothPolyDataFilter.hpp"

dtkThreeDeeSmoothPolyDataFilter::dtkThreeDeeSmoothPolyDataFilter() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkSmoothPolyDataFilter>::New()) {
    this->m_object = vtkSmoothPolyDataFilter::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeSmoothPolyDataFilter::setConvergence(qreal convergence) -> void {
    this->m_object->SetConvergence(convergence);
    this->update();
    emit this->convergenceChanged();
}

auto dtkThreeDeeSmoothPolyDataFilter::getConvergence() -> qreal {
    return this->m_object->GetConvergence();
}

auto dtkThreeDeeSmoothPolyDataFilter::setNumberOfIterations(int numberOfIterations) -> void {
    this->m_object->SetNumberOfIterations(numberOfIterations);
    this->update();
    emit this->numberOfIterationsChanged();
}

auto dtkThreeDeeSmoothPolyDataFilter::setRelaxationFactor(qreal relaxationFactor) -> void {
    this->m_object->SetRelaxationFactor(relaxationFactor);
    this->update();
    emit this->relaxationFactorChanged();
}

auto dtkThreeDeeSmoothPolyDataFilter::getRelaxationFactor() -> qreal {
    return this->m_object->GetRelaxationFactor();
}

auto dtkThreeDeeSmoothPolyDataFilter::getNumberOfIterations() -> int {
    return this->m_object->GetNumberOfIterations();
}

auto dtkThreeDeeSmoothPolyDataFilter::setFeatureEdgeSmoothing(bool featureEdgeSmoothing) -> void {
    this->m_object->SetFeatureEdgeSmoothing(featureEdgeSmoothing);
    this->update();
    emit this->featureEdgeSmoothingChanged();
}

auto dtkThreeDeeSmoothPolyDataFilter::getFeatureEdgeSmoothing() -> bool {
    return this->m_object->GetFeatureEdgeSmoothing();
}

auto dtkThreeDeeSmoothPolyDataFilter::setFeatureAngle(qreal featureAngle) -> void {
    this->m_object->SetFeatureAngle(featureAngle);
    this->update();
    emit this->featureAngleChanged();
}

auto dtkThreeDeeSmoothPolyDataFilter::getFeatureAngle() -> qreal {
    return this->m_object->GetFeatureAngle();
}

auto dtkThreeDeeSmoothPolyDataFilter::setEdgeAngle(qreal edgeAngle) -> void {
    this->m_object->SetEdgeAngle(edgeAngle);
    this->update();
    emit this->edgeAngleChanged();
}

auto dtkThreeDeeSmoothPolyDataFilter::getEdgeAngle() -> qreal {
    return this->m_object->GetEdgeAngle();
}

auto dtkThreeDeeSmoothPolyDataFilter::setBoundarySmoothing(bool boundarySmoothing) -> void {
    this->m_object->SetBoundarySmoothing(boundarySmoothing);
    this->update();
    emit this->boundarySmoothingChanged();
}

auto dtkThreeDeeSmoothPolyDataFilter::getBoundarySmoothing() -> bool {
    return this->m_object->GetBoundarySmoothing();
}

auto dtkThreeDeeSmoothPolyDataFilter::setGenerateErrorScalars(bool generateErrorScalars) -> void {
    this->m_object->SetGenerateErrorScalars(generateErrorScalars);
    this->update();
    emit this->generateErrorScalarsChanged();
}

auto dtkThreeDeeSmoothPolyDataFilter::getGenerateErrorScalars() -> bool {
    return this->m_object->GetGenerateErrorScalars();
}

auto dtkThreeDeeSmoothPolyDataFilter::setGenerateErrorVectors(bool generateErrorVectors) -> void {
    this->m_object->SetGenerateErrorScalars(generateErrorVectors);
    this->update();
    emit this->generateErrorVectorsChanged();
}

auto dtkThreeDeeSmoothPolyDataFilter::getGenerateErrorVectors() -> bool {
    return this->m_object->GetGenerateErrorVectors();
}

//
// dtkThreeDee+SmoothPolyDataFilter.cpp ends here
