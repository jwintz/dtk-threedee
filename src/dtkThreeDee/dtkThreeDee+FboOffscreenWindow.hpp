#pragma once

#include <QOpenGLFunctions>
#include <QOpenGLFramebufferObject>

#include <vtkExternalOpenGLRenderWindow.h>

#include <QVTKRenderWindowAdapter.h>

class dtkThreeDeeFboRenderer;

class dtkThreeDeeFboOffscreenWindow : public QObject, public vtkExternalOpenGLRenderWindow, protected QOpenGLFunctions {

    Q_OBJECT

public:
    dtkThreeDeeFboRenderer* QtParentRenderer = nullptr;
    static auto New() -> dtkThreeDeeFboOffscreenWindow*;
    virtual auto OpenGLInitState() -> void override;
    auto Render() -> void override;
    auto InternalRender() -> void;
    auto SetFramebufferObject(QOpenGLFramebufferObject*) -> void;

public:
    static void setupGraphicsBackend(void);

protected:
     dtkThreeDeeFboOffscreenWindow();
    ~dtkThreeDeeFboOffscreenWindow();

private:
    QOpenGLFramebufferObject *m_fbo = 0;

private:
    QVTKRenderWindowAdapter *m_adapter = 0;

private:
    bool m_initialised = false;
};
