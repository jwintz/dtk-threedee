// dtkThreeDee+Cone.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+ImplicitFunction.hpp"

#include <QQmlEngine>

#include <vtkCone.h>

class dtkThreeDeeCone : public dtkThreeDeeImplicitFunction
{
    Q_OBJECT

    Q_PROPERTY(qreal angle READ getAngle WRITE setAngle NOTIFY angleChanged);

    QML_NAMED_ELEMENT(Cone)

public:
    dtkThreeDeeCone();
    auto setAngle(qreal) -> void;
    auto getAngle() -> qreal;

signals:
    void angleChanged();

private:
    vtkSmartPointer<vtkCone> m_object = nullptr;
};

//
// dtkThreeDee+Cone.hpp ends here
