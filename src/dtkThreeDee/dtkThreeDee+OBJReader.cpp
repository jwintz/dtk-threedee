// dtkThreeDee+OBJReader.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+OBJReader.hpp"

dtkThreeDeeOBJReader::dtkThreeDeeOBJReader() : dtkThreeDeeAbstractPolyDataReader(vtkSmartPointer<vtkOBJReader>::New())
{

}

//
// dtkThreeDee+OBJReader.cpp ends here
