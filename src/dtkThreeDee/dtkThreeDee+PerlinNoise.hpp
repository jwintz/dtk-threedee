// dtkThreeDee+PerlinNoise.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Vector3.hpp"
#include "dtkThreeDee+ImplicitFunction.hpp"

#include <QQmlEngine>

#include <vtkPerlinNoise.h>

class dtkThreeDeePerlinNoise : public dtkThreeDeeImplicitFunction
{
    Q_OBJECT

    Q_PROPERTY(qreal amplitude READ getAmplitude WRITE setAmplitude NOTIFY amplitudeChanged);
    Q_PROPERTY(dtkThreeDeeVector3* frequency READ getFrequency CONSTANT);
    Q_PROPERTY(dtkThreeDeeVector3* phase READ getPhase CONSTANT);

    QML_NAMED_ELEMENT(PerlinNoise)

private:
    vtkSmartPointer<vtkPerlinNoise> m_object = nullptr;
    dtkThreeDeeVector3* m_frequency = nullptr;
    dtkThreeDeeVector3* m_phase = nullptr;

public:
    dtkThreeDeePerlinNoise();
    auto setAmplitude(qreal) -> void;
    auto getAmplitude() -> qreal;
    auto getFrequency() -> dtkThreeDeeVector3*;
    auto getPhase() -> dtkThreeDeeVector3*;

signals:
    void amplitudeChanged();
};

//
// dtkThreeDee+PerlinNoise.hpp ends here
