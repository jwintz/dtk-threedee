// dtkThreeDee+DataReader.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+DataReader.hpp"

dtkThreeDeeDataReader::dtkThreeDeeDataReader(vtkSmartPointer<vtkDataReader> vtkObject) : dtkThreeDeeAlgorithm(vtkObject) {
    this->m_object = vtkObject;
}

auto dtkThreeDeeDataReader::setFileName(const QString& fileName) -> void {
    this->m_object->SetFileName(fileName.toStdString().c_str());
    this->m_object->Update();
    this->update();

    emit this->fileNameChanged();
}

auto dtkThreeDeeDataReader::getFileName() -> QString {
    return this->m_object->GetFileName();
}

//
// dtkThreeDee+DataReader.cpp ends here
