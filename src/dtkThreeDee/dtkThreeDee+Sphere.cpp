// dtkThreeDee+Sphere.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Sphere.hpp"

dtkThreeDeeSphere::dtkThreeDeeSphere() : dtkThreeDeeImplicitFunction(vtkSmartPointer<vtkSphere>::New()) {
    this->m_object = vtkSphere::SafeDownCast(dtkThreeDeeImplicitFunction::get());

    this->m_center = new dtkThreeDeeVector3([this](){
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->update();
    });
}

auto dtkThreeDeeSphere::getCenter() -> dtkThreeDeeVector3* {
    return this->m_center;
}

qreal dtkThreeDeeSphere::evaluateFunction(qreal x, qreal y, qreal z) {
    return this->m_object->EvaluateFunction(x, y, z);
}

auto dtkThreeDeeSphere::setRadius(qreal radius) -> void {
    this->m_object->SetRadius(radius);
    emit this->radiusChanged();
}

auto dtkThreeDeeSphere::getRadius() -> qreal {
    return this->m_object->GetRadius();
}

//
// dtkThreeDee+Sphere.cpp ends here
