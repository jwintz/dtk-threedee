// dtkThreeDee+DataSetAlgorithm.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QQmlEngine>

#include <vtkDataSetAlgorithm.h>

#include "dtkThreeDee+Algorithm.hpp"

class dtkThreeDeeDataSetAlgorithm : public dtkThreeDeeAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(DataSetAlgorithm)
    QML_UNCREATABLE("")

public:
    dtkThreeDeeDataSetAlgorithm(vtkSmartPointer<vtkDataSetAlgorithm>);
};

//
// dtkThreeDee+DataSetAlgorithm.hpp ends here
