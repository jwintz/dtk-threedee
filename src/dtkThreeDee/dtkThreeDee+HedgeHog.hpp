// dtkThreeDee+HedgeHog.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkHedgeHog.h>

class dtkThreeDeeHedgeHog : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(HedgeHog)

public:
    enum VectorMode {
        UseVector = VTK_USE_VECTOR,
        UseNormal = VTK_USE_NORMAL
    };
    Q_ENUM(VectorMode);
    Q_PROPERTY(qreal scaleFactor READ getScaleFactor WRITE setScaleFactor NOTIFY scaleFactorChanged);
    Q_PROPERTY(VectorMode vectorMode READ getVectorMode WRITE setVectorMode NOTIFY vectorModeChanged);

private:
    vtkSmartPointer<vtkHedgeHog> m_object = nullptr;

public:
    dtkThreeDeeHedgeHog();
    auto setScaleFactor(qreal) -> void;
    auto getScaleFactor() -> qreal;
    auto setVectorMode(VectorMode) -> void;
    auto getVectorMode() -> VectorMode;

signals:
    void scaleFactorChanged();
    void vectorModeChanged();
};

//
// dtkThreeDee+HedgeHog.hpp ends here
