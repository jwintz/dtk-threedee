// dtkThreeDee+RotationalExtrusionFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+RotationalExtrusionFilter.hpp"

dtkThreeDeeRotationalExtrusionFilter::dtkThreeDeeRotationalExtrusionFilter() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkRotationalExtrusionFilter>::New()) {
    this->m_object = vtkRotationalExtrusionFilter::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeRotationalExtrusionFilter::setResolution(int resolution) -> void {
    this->m_object->SetResolution(resolution);
    emit this->resolutionChanged();
    this->update();
}

auto dtkThreeDeeRotationalExtrusionFilter::getResolution() -> int {
    return this->m_object->GetResolution();
}

auto dtkThreeDeeRotationalExtrusionFilter::setCapping(bool capping) -> void {
    this->m_object->SetCapping(capping);
    emit this->cappingChanged();
    this->update();
}

auto dtkThreeDeeRotationalExtrusionFilter::getCapping() -> bool {
    return this->m_object->GetCapping();
}

auto dtkThreeDeeRotationalExtrusionFilter::setAngle(qreal angle) -> void {
    this->m_object->SetAngle(angle);
    emit this->angleChanged();
    this->update();
}

auto dtkThreeDeeRotationalExtrusionFilter::getAngle() -> qreal {
    return this->m_object->GetAngle();
}

auto dtkThreeDeeRotationalExtrusionFilter::setTranslation(qreal translation) -> void {
    this->m_object->SetTranslation(translation);
    emit this->translationChanged();
    this->update();
}

auto dtkThreeDeeRotationalExtrusionFilter::getTranslation() -> qreal {
    return this->m_object->GetTranslation();
}

auto dtkThreeDeeRotationalExtrusionFilter::setDeltaRadius(qreal deltaRadius) -> void {
    this->m_object->SetDeltaRadius(deltaRadius);
    emit this->deltaRadiusChanged();
    this->update();
}

auto dtkThreeDeeRotationalExtrusionFilter::getDeltaRadius() -> qreal {
    return this->m_object->GetDeltaRadius();
}

//
// dtkThreeDee+RotationalExtrusionFilter.cpp ends here
