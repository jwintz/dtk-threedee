// dtkThreeDee+Mapper.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Mapper.hpp"

dtkThreeDeeMapper::dtkThreeDeeMapper(vtkSmartPointer<vtkMapper> vtkObject) : dtkThreeDeeAbstractMapper3D(vtkObject)
{
    this->m_object = vtkObject;
}

auto dtkThreeDeeMapper::get() -> vtkSmartPointer<vtkMapper>
{
    return vtkMapper::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeMapper::setScalarMode(ScalarMode scalarMode) -> void
{
    this->m_object->SetScalarMode(scalarMode);
    emit this->scalarModeChanged();
    this->update();
}

auto dtkThreeDeeMapper::getScalarMode() -> ScalarMode
{
    return static_cast<ScalarMode>(this->m_object->GetScalarMode());
}

auto dtkThreeDeeMapper::setScalarVisibility(bool val) -> void
{
    this->m_object->SetScalarVisibility(val);
    emit this->scalarVisibilityChanged();
    this->update();
}

auto dtkThreeDeeMapper::getScalarVisibility() -> bool
{
    return this->m_object->GetScalarVisibility();
}

//
// dtkThreeDee+Mapper.cpp ends here
