// dtkThreeDee+WarpTo.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+WarpTo.hpp"

dtkThreeDeeWarpTo::dtkThreeDeeWarpTo() : dtkThreeDeePointSetAlgorithm(vtkSmartPointer<vtkWarpTo>::New()) {
    this->m_object = vtkWarpTo::SafeDownCast(dtkThreeDeeAlgorithm::get());
    this->m_position = new dtkThreeDeeVector3([this](){
        this->m_object->SetPosition(this->m_position->getValues().data());
        this->update();
    });
}

auto dtkThreeDeeWarpTo::getPosition() -> dtkThreeDeeVector3* {
    return this->m_position;
}

auto dtkThreeDeeWarpTo::setScaleFactor(qreal scaleFactor) -> void {
    this->m_object->SetScaleFactor(scaleFactor);
    emit this->scaleFactorChanged();
    this->update();
}

auto dtkThreeDeeWarpTo::getScaleFactor() -> qreal {
    return this->m_object->GetScaleFactor();
}

auto dtkThreeDeeWarpTo::setAbsolute(bool absolute) -> void {
    this->m_object->SetAbsolute(absolute);
    emit this->absoluteChanged();
    this->update();
}

auto dtkThreeDeeWarpTo::getAbsolute() -> bool {
    return this->m_object->GetAbsolute();
}

//
// dtkThreeDee+WarpTo.cpp ends here
