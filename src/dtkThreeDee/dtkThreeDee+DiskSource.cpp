// dtkThreeDee+DiskSource.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+DiskSource.hpp"

dtkThreeDeeDiskSource::dtkThreeDeeDiskSource() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkDiskSource>::New()) {
    this->m_object = vtkDiskSource::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeDiskSource::setInnerRadius(qreal radius) -> void {
    this->m_object->SetInnerRadius(radius);
    emit this->innerRadiusChanged();
    this->update();
}

auto dtkThreeDeeDiskSource::setOuterRadius(qreal radius) -> void {
    this->m_object->SetOuterRadius(radius);
    emit this->outerRadiusChanged();
    this->update();
}

auto dtkThreeDeeDiskSource::setRadialResolution(int value) -> void {
    this->m_object->SetRadialResolution(value);
    emit this->radialResolutionChanged();
    this->update();
}

auto dtkThreeDeeDiskSource::setCircumferentialResolution(int value) -> void {
    this->m_object->SetCircumferentialResolution(value);
    emit this->circumferentialResolutionChanged();
    this->update();
}

auto dtkThreeDeeDiskSource::getInnerRadius() -> qreal {
    return this->m_object->GetInnerRadius();
}

auto dtkThreeDeeDiskSource::getOuterRadius() -> qreal {
    return this->m_object->GetOuterRadius();
}

auto dtkThreeDeeDiskSource::getRadialResolution() -> int {
    return this->m_object->GetRadialResolution();
}

auto dtkThreeDeeDiskSource::getCircumferentialResolution() -> int {
    return this->m_object->GetCircumferentialResolution();
}

//
// dtkThreeDee+DiskSource.cpp ends here
