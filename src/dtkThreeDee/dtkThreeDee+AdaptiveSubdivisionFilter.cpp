// dtkThreeDee+AdaptiveSubdivisionFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+AdaptiveSubdivisionFilter.hpp"

dtkThreeDeeAdaptiveSubdivisionFilter::dtkThreeDeeAdaptiveSubdivisionFilter(void) : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkAdaptiveSubdivisionFilter>::New())
{
    this->m_object = vtkAdaptiveSubdivisionFilter::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeAdaptiveSubdivisionFilter::setMaximumEdgeLength(qreal maximumEdgeLength) -> void
{
    this->m_object->SetMaximumEdgeLength(maximumEdgeLength);
    emit this->maximumEdgeLengthChanged();
    this->update();
}

auto dtkThreeDeeAdaptiveSubdivisionFilter::getMaximumEdgeLength(void) -> qreal
{
    return this->m_object->GetMaximumEdgeLength();
}

auto dtkThreeDeeAdaptiveSubdivisionFilter::setMaximumTriangleArea(qreal maximumTriangleArea) -> void
{
    this->m_object->SetMaximumTriangleArea(maximumTriangleArea);
    emit this->maximumTriangleAreaChanged();
    this->update();
}

auto dtkThreeDeeAdaptiveSubdivisionFilter::getMaximumTriangleArea() -> qreal
{
    return this->m_object->GetMaximumTriangleArea();
}

auto dtkThreeDeeAdaptiveSubdivisionFilter::setMaximumNumberOfTriangles(int maximumNumberOfTriangles) -> void
{
    this->m_object->SetMaximumNumberOfTriangles(maximumNumberOfTriangles);
    emit this->maximumNumberOfTrianglesChanged();
    this->update();
}

auto dtkThreeDeeAdaptiveSubdivisionFilter::getMaximumNumberOfTriangles() -> int
{
    return this->m_object->GetMaximumNumberOfTriangles();
}

auto dtkThreeDeeAdaptiveSubdivisionFilter::setMaximumNumberOfPasses(int maximumNumberOfPasses) -> void
{
    this->m_object->SetMaximumNumberOfPasses(maximumNumberOfPasses);
    emit this->maximumNumberOfPassesChanged();
    this->update();
}

auto dtkThreeDeeAdaptiveSubdivisionFilter::getMaximumNumberOfPasses() -> int
{
    return this->m_object->GetMaximumNumberOfPasses();
}

//
// dtkThreeDee+AdaptiveSubdivisionFilter.cpp ends here
