// dtkThreeDee+ApproximatingSubdivisionFilter.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkApproximatingSubdivisionFilter.h>

class dtkThreeDeeApproximatingSubdivisionFilter : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT
    Q_PROPERTY(int numberOfSubdivisions READ getNumberOfSubdivisions WRITE setNumberOfSubdivisions NOTIFY numberOfSubdivisionsChanged);
    QML_NAMED_ELEMENT(ApproximatingSubdivisionFilter)
    QML_UNCREATABLE("")

public:
    dtkThreeDeeApproximatingSubdivisionFilter(vtkSmartPointer<vtkApproximatingSubdivisionFilter>);
    auto setNumberOfSubdivisions(int) -> void;
    auto getNumberOfSubdivisions() -> int;

signals:
    void numberOfSubdivisionsChanged();

private:
    vtkSmartPointer<vtkApproximatingSubdivisionFilter> m_object = nullptr;
};

//
// dtkThreeDee+ApproximatingSubdivisionFilter.hpp ends here
