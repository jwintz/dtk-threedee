// dtkThreeDee+LinearSubdivisionFilter.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+InterpolatingSubdivisionFilter.hpp"

#include <QQmlEngine>

#include <vtkLinearSubdivisionFilter.h>

class dtkThreeDeeLinearSubdivisionFilter : public dtkThreeDeeInterpolatingSubdivisionFilter
{
    Q_OBJECT

    QML_NAMED_ELEMENT(LinearSubdivisionFilter)

public:
    dtkThreeDeeLinearSubdivisionFilter();
};

//
// dtkThreeDee+LinearSubdivisionFilter.hpp ends here
