// dtkThreeDee+PolyDataMapper.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QQmlEngine>

#include "dtkThreeDee+Mapper.hpp"
#include "dtkThreeDee+Algorithm.hpp"

#include <QQmlEngine>

#include <vtkPolyDataMapper.h>

class dtkThreeDeePolyDataAlgorithm;

class dtkThreeDeePolyDataMapper : public dtkThreeDeeMapper
{
    Q_OBJECT

    QML_NAMED_ELEMENT(PolyDataMapper)

public:
    dtkThreeDeePolyDataMapper();
};

//
// dtkThreeDee+PolyDataMapper.hpp ends here
