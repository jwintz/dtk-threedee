// dtkThreeDee+AbstractPolyDataReader.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkAbstractPolyDataReader.h>

class dtkThreeDeeAbstractPolyDataReader : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(QString fileName READ getFileName WRITE setFileName NOTIFY fileNameChanged);

    QML_NAMED_ELEMENT(AbstractPolyDataReader)
    QML_UNCREATABLE("")

private:
    vtkSmartPointer<vtkAbstractPolyDataReader> m_vtkObject = nullptr;
    QString m_fileName;

public:
    dtkThreeDeeAbstractPolyDataReader(vtkSmartPointer<vtkAbstractPolyDataReader>);
    auto isValid() -> bool override;
    auto setFileName(const QString&) -> void;
    auto getFileName() -> QString;

signals:
    void fileNameChanged();
};

//
// dtkThreeDee+AbstractPolyDataReader.hpp ends here
