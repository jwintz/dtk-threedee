// dtkThreeDee+ReverseSense.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ReverseSense.hpp"

dtkThreeDeeReverseSense::dtkThreeDeeReverseSense() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkReverseSense>::New()) {
    this->m_object = vtkReverseSense::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeReverseSense::setReverseCells(bool reverseCells) -> void {
    this->m_object->SetReverseCells(reverseCells);
    this->update();
    emit this->reverseCellsChanged();
}

auto dtkThreeDeeReverseSense::getReverseCells() -> bool {
    return this->m_object->GetReverseCells();
}

auto dtkThreeDeeReverseSense::setReverseNormals(bool reverseNormals) -> void {
    this->m_object->SetReverseNormals(reverseNormals);
    this->update();
    emit this->reverseNormalsChanged();
}

auto dtkThreeDeeReverseSense::getReverseNormals() -> bool {
    return this->m_object->GetReverseNormals();
}

//
// dtkThreeDee+ReverseSense.cpp ends here
