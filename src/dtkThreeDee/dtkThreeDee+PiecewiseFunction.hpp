// dtkThreeDee+PiecewiseFunction.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QObject>
#include <QList>
#include <QQmlEngine>

#include <vtkSmartPointer.h>
#include <vtkPiecewiseFunction.h>

class dtkThreeDeePiecewiseFunction : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int size READ getSize NOTIFY sizeChanged);

    QML_NAMED_ELEMENT(PiecewiseFunction)
    QML_UNCREATABLE("")

private:
    using cb_t = std::function<void()>;
    using vtk_t = vtkSmartPointer<vtkPiecewiseFunction>;
    vtk_t m_object = nullptr;
    cb_t m_callback;
    QList<double> m_xValues;
    QList<double> m_yValues;

public:
    dtkThreeDeePiecewiseFunction(vtk_t, cb_t&&);
    auto update() -> void;
    auto getSize() -> int;
    auto setClamping(bool) -> void;
    auto getClamping() -> bool;

public slots:
    void clear();
    void add(double, double);
    double getX(int);
    double getY(int);

signals:
    void sizeChanged();
    void clampingChanged();
};

//
// dtkThreeDee+PiecewiseFunction.hpp ends here
