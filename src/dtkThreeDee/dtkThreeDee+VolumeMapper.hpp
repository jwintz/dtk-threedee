// dtkThreeDee+VolumeMapper.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+AbstractVolumeMapper.hpp"

#include <QQmlEngine>

#include <vtkVolumeMapper.h>

class dtkThreeDeeVolumeMapper : public dtkThreeDeeAbstractVolumeMapper
{
    Q_OBJECT

    QML_NAMED_ELEMENT(VolumeMapper)
    QML_UNCREATABLE("")

public:
    enum BlendMode {
        CompositeBlend = vtkVolumeMapper::COMPOSITE_BLEND,
        MaximumIntensityBlend = vtkVolumeMapper::MAXIMUM_INTENSITY_BLEND,
        MinimumIntensityBlend = vtkVolumeMapper::MINIMUM_INTENSITY_BLEND,
        AdditiveBlend = vtkVolumeMapper::ADDITIVE_BLEND
    };

private:
    Q_ENUM(BlendMode);
    Q_PROPERTY(BlendMode blendMode READ getBlendMode WRITE setBlendMode NOTIFY blendModeChanged);
    Q_PROPERTY(bool cropping READ getCropping WRITE setCropping NOTIFY croppingChanged);

private:
    vtkSmartPointer<vtkVolumeMapper> m_object = nullptr;

public:
    dtkThreeDeeVolumeMapper(vtkSmartPointer<vtkVolumeMapper>);
    auto setBlendMode(BlendMode) -> void;
    auto getBlendMode() -> BlendMode;
    auto setCropping(bool) -> void;
    auto getCropping() -> bool;

signals:
    void blendModeChanged();
    void croppingChanged();
};

//
// dtkThreeDee+VolumeMapper.hpp ends here
