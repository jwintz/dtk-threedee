// dtkThreeDee+ButterflySubdivisionFilter.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+InterpolatingSubdivisionFilter.hpp"

#include <QQmlEngine>

#include <vtkButterflySubdivisionFilter.h>

class dtkThreeDeeButterflySubdivisionFilter : public dtkThreeDeeInterpolatingSubdivisionFilter
{
    Q_OBJECT

    QML_NAMED_ELEMENT(ButterflySubdivisionFilter)

public:
    dtkThreeDeeButterflySubdivisionFilter();
};

//
// dtkThreeDee+ButterflySubdivisionFilter.hpp ends here
