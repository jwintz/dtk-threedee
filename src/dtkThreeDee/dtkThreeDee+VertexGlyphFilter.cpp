// dtkThreeDee+VertexGlyphFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+VertexGlyphFilter.hpp"

dtkThreeDeeVertexGlyphFilter::dtkThreeDeeVertexGlyphFilter() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkVertexGlyphFilter>::New())
{

}

//
// dtkThreeDee+VertexGlyphFilter.cpp ends here
