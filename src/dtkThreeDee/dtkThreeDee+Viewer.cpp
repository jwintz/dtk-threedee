// dtkThreeDee+Viewer.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Viewer.hpp"

#include "dtkThreeDee+Prop.hpp"
#include "dtkThreeDee+FboRenderer.hpp"
#include "dtkThreeDee+AbstractWidget.hpp"
#include "dtkThreeDee+FboOffscreenWindow.hpp"

#include <QOpenGLFunctions>
#include <QQuickFramebufferObject>
#include <QOpenGLFramebufferObject>

#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkRendererCollection.h>
#include <vtkObjectFactory.h>
#include <vtkSmartPointer.h>
#include <vtkRenderer.h>
#include <vtkCamera.h>

dtkThreeDeeViewer::dtkThreeDeeViewer() {
    this->m_renderer = 0;
    m_win = dtkThreeDeeFboOffscreenWindow::New();
//  this->createRenderer();
    this->setMirrorVertically(true);
//  this->init();
//  this->update();
}

auto dtkThreeDeeViewer::init() -> void {

    auto rw = this->GetRenderWindow();
    this->m_renderer = vtkSmartPointer<vtkRenderer>::New();

    if (!this->m_externalEnabled)
        rw->AddRenderer(m_renderer);

    this->m_initialized = true;

    for (auto object : this->m_input) {
        if (object->getType() == dtkThreeDeeObject::Type::Prop) {
            auto prop = reinterpret_cast<dtkThreeDeeProp*>(object);
            prop->linkViewer(this);
            this->m_renderer->AddActor(prop->get());
        }
        else if (object->getType() == dtkThreeDeeObject::Type::Widget) {
            auto widget = reinterpret_cast<dtkThreeDeeAbstractWidget*>(object);
            auto vtkWidget = widget->get();
            vtkWidget->CreateDefaultRepresentation();
            vtkWidget->SetInteractor(this->GetRenderWindow()->GetInteractor());
            vtkWidget->SetManagesCursor(true);
            vtkWidget->SetPickingManaged(true);
            vtkWidget->On();
        }
    }

    this->update();

    emit initialised();
}

void dtkThreeDeeViewer::render(void)
{
    this->GetRenderWindow()->GetInteractor()->Render();
}

auto dtkThreeDeeViewer::update() -> void {

    if (!this->m_initialized) {
        return;
    }

    if (this->m_fboRenderer) {
        QQuickFramebufferObject::update();
    }
}

void dtkThreeDeeViewer::setCameraPosition(double x, double y, double z)
{
    this->m_renderer->GetActiveCamera()->SetPosition(x, y, z);
    this->m_renderer->ResetCameraClippingRange();
}

void dtkThreeDeeViewer::setCameraFocalPoint(double x, double y, double z)
{
    this->m_renderer->GetActiveCamera()->SetFocalPoint(x, y, z);
    this->m_renderer->ResetCameraClippingRange();
}

void dtkThreeDeeViewer::setCameraUp(double x, double y, double z)
{
    this->m_renderer->GetActiveCamera()->SetViewUp(x, y, z);
    this->m_renderer->ResetCameraClippingRange();
}

QList<double> dtkThreeDeeViewer::bounds(void)
{
    double bounds[6];

    this->m_renderer->ComputeVisiblePropBounds(bounds);

    return QList<double>() << bounds[0] << bounds[1] << bounds[2] << bounds[3] << bounds[4] << bounds[5];
}

auto dtkThreeDeeViewer::removeData(dtkThreeDeeObject* object) -> void {
}

auto dtkThreeDeeViewer::setHoverEnabled(bool hoverEnabled) -> void {
    this->m_hoverEnabled = hoverEnabled;

    setAcceptHoverEvents(hoverEnabled);

    emit this->hoverEnabledChanged();
}

auto dtkThreeDeeViewer::setMouseEnabled(bool mouseEnabled) -> void {
    this->m_mouseEnabled = mouseEnabled;

    if (mouseEnabled) {
        setAcceptedMouseButtons(Qt::AllButtons);
    }
    else {
        setAcceptedMouseButtons(Qt::NoButton);
    }

    emit this->mouseEnabledChanged();
}

auto dtkThreeDeeViewer::setExternalEnabled(bool enabled) -> void {
    this->m_externalEnabled = enabled;
}

auto dtkThreeDeeViewer::mousePressEvent(QMouseEvent* event) -> void
{
    this->m_fboRenderer->onMouseEvent(event);
    this->forceActiveFocus();
    this->update();
}

auto dtkThreeDeeViewer::mouseReleaseEvent(QMouseEvent* event) -> void
{
    this->m_fboRenderer->onMouseEvent(event);
    this->update();
}

auto dtkThreeDeeViewer::mouseMoveEvent(QMouseEvent* event) -> void
{
    this->m_fboRenderer->onMouseMoveEvent(event);
    this->update();
}

auto dtkThreeDeeViewer::hoverMoveEvent(QHoverEvent* event) -> void
{

}

auto dtkThreeDeeViewer::wheelEvent(QWheelEvent* event) -> void
{
    this->m_fboRenderer->onWheelEvent(event);
    this->update();
}

auto dtkThreeDeeViewer::createRenderer() const -> QQuickFramebufferObject::Renderer* {
    this->m_fboRenderer = new dtkThreeDeeFboRenderer(static_cast<dtkThreeDeeFboOffscreenWindow*>(m_win));

    const_cast<dtkThreeDeeViewer *>(this)->init();
    const_cast<dtkThreeDeeViewer *>(this)->update();

    return this->m_fboRenderer;
}

auto dtkThreeDeeViewer::GetRenderWindow() const -> vtkGenericOpenGLRenderWindow* {
    return m_win;
}
auto dtkThreeDeeViewer::GetRenderer() const -> vtkRenderer* {
    return m_renderer;
}

auto dtkThreeDeeViewer::getInput() -> QQmlListProperty<dtkThreeDeeObject> {
    return QQmlListProperty<dtkThreeDeeObject>(this, 0, &appendInput, &inputCount, &inputAt, &clearInputs);
}

auto dtkThreeDeeViewer::appendInput(QQmlListProperty<dtkThreeDeeObject>* list, dtkThreeDeeObject* object) -> void {

    auto viewer = qobject_cast<dtkThreeDeeViewer*>(list->object);

    if (object && viewer) {
        viewer->m_input.append(object);
    }

    if(viewer && viewer->m_renderer && object) {

        if (object->getType() == dtkThreeDeeObject::Type::Prop) {
            auto prop = reinterpret_cast<dtkThreeDeeProp*>(object);
            viewer->m_renderer->AddActor(prop->get());
        }
        else if (object->getType() == dtkThreeDeeObject::Type::Widget) {
            auto widget = reinterpret_cast<dtkThreeDeeAbstractWidget*>(object);
            auto vtkWidget = widget->get();
            vtkWidget->SetInteractor(viewer->GetRenderWindow()->GetInteractor());
            vtkWidget->CreateDefaultRepresentation();
            vtkWidget->On();
        }

        emit viewer->inputChanged();
        viewer->update();
    }
}

#if QT_VERSION >= 0x060000
auto dtkThreeDeeViewer::inputCount(QQmlListProperty<dtkThreeDeeObject>* list) -> qsizetype {
#else
auto dtkThreeDeeViewer::inputCount(QQmlListProperty<dtkThreeDeeObject>* list) -> int {
#endif
    auto viewer = qobject_cast<dtkThreeDeeViewer*>(list->object);

    if (viewer) {
        return viewer->m_input.count();
    }

    return 0;
}

#if QT_VERSION >= 0x060000
auto dtkThreeDeeViewer::inputAt(QQmlListProperty<dtkThreeDeeObject>* list, qsizetype i) -> dtkThreeDeeObject* {
#else
auto dtkThreeDeeViewer::inputAt(QQmlListProperty<dtkThreeDeeObject>* list, int i) -> dtkThreeDeeObject* {
#endif
    auto viewer = qobject_cast<dtkThreeDeeViewer*>(list->object);

    if (viewer) {
        return viewer->m_input.at(i);
    }

    return 0;
}

auto dtkThreeDeeViewer::clearInputs(QQmlListProperty<dtkThreeDeeObject>*list) -> void {
    auto viewer = qobject_cast<dtkThreeDeeViewer*>(list->object);

    if (viewer) {
        for (auto object : viewer->m_input) {
            if (object->getType() == dtkThreeDeeObject::Type::Prop) {
                auto prop = reinterpret_cast<dtkThreeDeeProp*>(object);
                viewer->m_renderer->RemoveActor(prop->get());
            }
        }

        viewer->m_input.clear();
        emit viewer->inputChanged();
        viewer->update();
    }
}

auto dtkThreeDeeViewer::getHoverEnabled() -> bool {
    return this->m_hoverEnabled;
}

auto dtkThreeDeeViewer::getMouseEnabled() -> bool {
    return this->m_mouseEnabled;
}

dtkThreeDeeViewer::~dtkThreeDeeViewer() {
    m_win->Delete();
}

//
// dtkThreeDee+Viewer.cpp ends here
