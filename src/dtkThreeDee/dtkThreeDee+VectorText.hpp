// dtkThreeDee+VectorText.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkVectorText.h>

class dtkThreeDeeVectorText : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(QString text READ getText WRITE setText NOTIFY textChanged);

    QML_NAMED_ELEMENT(VectorText)

private:
    vtkSmartPointer<vtkVectorText> m_object = nullptr;

public:
    dtkThreeDeeVectorText();
    auto setText(const QString&) -> void;
    auto getText() -> QString;

signals:
    void textChanged();
};

//
// dtkThreeDee+VectorText.hpp ends here
