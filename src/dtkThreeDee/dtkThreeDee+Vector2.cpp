// dtkThreeDee+Vector2.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Vector2.hpp"

dtkThreeDeeVector2::dtkThreeDeeVector2(cb_t&& callback, array_t values) : m_callback(callback), m_values(values)
{

}

void dtkThreeDeeVector2::notify(void)
{
    this->m_callback.operator()();
}

double dtkThreeDeeVector2::getX(void)
{
    return this->m_values[0];
}

double dtkThreeDeeVector2::getY(void)
{
    return this->m_values[1];
}

void dtkThreeDeeVector2::setX(double x)
{
    this->m_values[0] = x;
    emit this->xChanged();
    this->notify();
}

void dtkThreeDeeVector2::setY(double y)
{
    this->m_values[1] = y;
    emit this->yChanged();
    this->notify();
}

dtkThreeDeeVector2::array_t dtkThreeDeeVector2::getValues(void)
{
    return this->m_values;
}

//
// dtkThreeDee+Vector2.cpp ends here
