// dtkThreeDee+PLYReader.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+PLYReader.hpp"

dtkThreeDeePLYReader::dtkThreeDeePLYReader() : dtkThreeDeeAbstractPolyDataReader(vtkSmartPointer<vtkPLYReader>::New())
{

}

//
// dtkThreeDee+PLYReader.cpp ends here
