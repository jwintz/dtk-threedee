// dtkThreeDee+VectorText.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+VectorText.hpp"

dtkThreeDeeVectorText::dtkThreeDeeVectorText() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkVectorText>::New()) {
    this->m_object = vtkVectorText::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeVectorText::setText(const QString& text) -> void {
    this->m_object->SetText(text.toStdString().c_str());
    this->update();
    emit this->textChanged();
}

auto dtkThreeDeeVectorText::getText() -> QString {
    return this->m_object->GetText();
}

//
// dtkThreeDee+VectorText.cpp ends here
