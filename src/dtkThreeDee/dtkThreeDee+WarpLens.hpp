// dtkThreeDee+WarpLens.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PointSetAlgorithm.hpp"
#include "dtkThreeDee+Vector2.hpp"

#include <QQmlEngine>

#include <vtkWarpLens.h>

class dtkThreeDeeWarpLens : public dtkThreeDeePointSetAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(dtkThreeDeeVector2* center READ getCenter CONSTANT);
    Q_PROPERTY(dtkThreeDeeVector2* principalPoint READ getPrincipalPoint CONSTANT);
    Q_PROPERTY(qreal k1 READ getK1 WRITE setK1 NOTIFY k1Changed);
    Q_PROPERTY(qreal k2 READ getK2 WRITE setK2 NOTIFY k2Changed);
    Q_PROPERTY(qreal p1 READ getP1 WRITE setP1 NOTIFY p1Changed);
    Q_PROPERTY(qreal p2 READ getP2 WRITE setP2 NOTIFY p2Changed);

    QML_NAMED_ELEMENT(WarpLens)

private:
    vtkSmartPointer<vtkWarpLens> m_object = nullptr;
    dtkThreeDeeVector2* m_center = nullptr;
    dtkThreeDeeVector2* m_principalPoint = nullptr;

public:
    dtkThreeDeeWarpLens();
    auto getCenter() -> dtkThreeDeeVector2*;
    auto getPrincipalPoint() -> dtkThreeDeeVector2*;
    auto setK1(qreal) -> void;
    auto getK1() -> qreal;
    auto setK2(qreal) -> void;
    auto getK2() -> qreal;
    auto setP1(qreal) -> void;
    auto getP1() -> qreal;
    auto setP2(qreal) -> void;
    auto getP2() -> qreal;

signals:
    void k1Changed();
    void k2Changed();
    void p1Changed();
    void p2Changed();
};

//
// dtkThreeDee+WarpLens.hpp ends here
