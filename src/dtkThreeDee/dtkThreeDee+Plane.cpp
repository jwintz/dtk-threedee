// dtkThreeDee+Plane.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Plane.hpp"

dtkThreeDeePlane::dtkThreeDeePlane() : dtkThreeDeeImplicitFunction(vtkSmartPointer<vtkPlane>::New())
{
    this->m_object = vtkPlane::SafeDownCast(dtkThreeDeeImplicitFunction::get());

    this->m_origin = new dtkThreeDeeVector3([this](){
        this->m_object->SetOrigin(this->m_origin->getValues().data());
        this->update();
    });

    this->m_normal = new dtkThreeDeeVector3([this](){
        this->m_object->SetNormal(this->m_normal->getValues().data());
    });
}

auto dtkThreeDeePlane::getOrigin() -> dtkThreeDeeVector3* {
    return this->m_origin;
}

auto dtkThreeDeePlane::getNormal() -> dtkThreeDeeVector3* {
    return this->m_origin;
}

//
// dtkThreeDee+Plane.cpp ends here
