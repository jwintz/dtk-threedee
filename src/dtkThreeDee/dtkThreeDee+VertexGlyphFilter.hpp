// dtkThreeDee+VertexGlyphFilter.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkVertexGlyphFilter.h>

class dtkThreeDeeVertexGlyphFilter : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(VertexGlyphFilter)

public:
    dtkThreeDeeVertexGlyphFilter();
};

//
// dtkThreeDee+VertexGlyphFilter.hpp ends here
