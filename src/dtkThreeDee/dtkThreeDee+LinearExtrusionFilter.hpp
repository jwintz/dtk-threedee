// dtkThreeDee+LinearExtrusionFilter.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"
#include "dtkThreeDee+Vector3.hpp"

#include <QQmlEngine>

#include <vtkLinearExtrusionFilter.h>

class dtkThreeDeeLinearExtrusionFilter : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(LinearExtrusionFilter)

public:
    enum ExtrusionType
    {
        VectorExtrusion = VTK_VECTOR_EXTRUSION,
        NormalExtrusion = VTK_NORMAL_EXTRUSION,
        PointExtrusion = VTK_POINT_EXTRUSION
    };
private:
    Q_ENUM(ExtrusionType);
    Q_PROPERTY(dtkThreeDeeVector3* vector READ getVector CONSTANT);
    Q_PROPERTY(dtkThreeDeeVector3* extrusionPoint READ getExtrusionPoint CONSTANT);
    Q_PROPERTY(ExtrusionType extrusionType READ getExtrusionType WRITE setExtrusionType NOTIFY extrusionTypeChanged);
    Q_PROPERTY(qreal scaleFactor READ getScaleFactor WRITE setScaleFactor NOTIFY scaleFactorChanged);
    Q_PROPERTY(bool capping READ getCapping WRITE setCapping NOTIFY cappingChanged);
private:
    vtkSmartPointer<vtkLinearExtrusionFilter> m_object = nullptr;
    dtkThreeDeeVector3* m_extrusionPoint = nullptr;
    dtkThreeDeeVector3* m_vector = nullptr;
public:
    dtkThreeDeeLinearExtrusionFilter();
    auto setExtrusionType(ExtrusionType) -> void;
    auto getExtrusionType() -> ExtrusionType;
    auto setScaleFactor(qreal) -> void;
    auto getScaleFactor() -> qreal;
    auto setCapping(bool) -> void;
    auto getCapping() -> bool;
    auto getVector() -> dtkThreeDeeVector3*;
    auto getExtrusionPoint() -> dtkThreeDeeVector3*;
signals:
    void extrusionTypeChanged();
    void scaleFactorChanged();
    void cappingChanged();
};

//
// dtkThreeDee+LinearExtrusionFilter.hpp ends here
