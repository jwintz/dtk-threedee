// dtkThreeDee+Volume.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QtCore>
#include <QQmlEngine>

#include "dtkThreeDee+AbstractVolumeMapper.hpp"
#include "dtkThreeDee+Prop3D.hpp"
#include "dtkThreeDee+VolumeProperty.hpp"

#include <vtkVolume.h>

class dtkThreeDeeVolume : public dtkThreeDeeProp3D
{
    Q_OBJECT

    Q_PROPERTY(dtkThreeDeeAbstractVolumeMapper *mapper READ getMapper WRITE setMapper NOTIFY mapperChanged);
    Q_PROPERTY(dtkThreeDeeVolumeProperty *property READ getProperty CONSTANT);

    Q_CLASSINFO("DefaultProperty", "mapper");

    QML_NAMED_ELEMENT(Volume)

private:
    dtkThreeDeeVolumeProperty *m_property = nullptr;
    dtkThreeDeeAbstractVolumeMapper *m_mapper = nullptr;
    vtkSmartPointer<vtkVolume> m_volume = nullptr;

public:
    dtkThreeDeeVolume();
    ~dtkThreeDeeVolume();
    auto setMapper(dtkThreeDeeAbstractVolumeMapper *) -> void;
    auto getMapper() -> dtkThreeDeeAbstractVolumeMapper *;
    auto getProperty() -> dtkThreeDeeVolumeProperty *;
    auto get() -> vtkSmartPointer<vtkVolume>;

signals:
    void mapperChanged();
};

//
// dtkThreeDee+Volume.hpp ends here
