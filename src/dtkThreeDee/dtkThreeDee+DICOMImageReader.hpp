// dtkThreeDee+DICOMImageReader.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+ImageReader2.hpp"

#include <QQmlEngine>

#include <vtkDICOMImageReader.h>

class dtkThreeDeeDICOMImageReader : public dtkThreeDeeImageReader2
{
    Q_OBJECT

    Q_PROPERTY(QString directoryName READ getDirectoryName WRITE setDirectoryName NOTIFY directoryNameChanged);

    QML_NAMED_ELEMENT(DICOMImageReader)

private:
    QString m_directoryName;
    vtkSmartPointer<vtkDICOMImageReader> m_object = nullptr;
public:
    dtkThreeDeeDICOMImageReader();
    auto setDirectoryName(const QString&) -> void;
    auto getDirectoryName() -> QString;
signals:
    void directoryNameChanged();
};

//
// dtkThreeDee+DICOMImageReader.hpp ends here
