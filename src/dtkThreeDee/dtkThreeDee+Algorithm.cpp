// dtkThreeDee+Algorithm.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Algorithm.hpp"

#include "dtkThreeDee+Prop.hpp"

dtkThreeDeeAlgorithm::dtkThreeDeeAlgorithm(vtkSmartPointer<vtkAlgorithm> vtkObject) : dtkThreeDeeObject(dtkThreeDeeObject::Type::Algorithm), m_object(vtkObject)
{

}

dtkThreeDeeAlgorithm::~dtkThreeDeeAlgorithm(void)
{
    this->m_prop = nullptr;
}

auto dtkThreeDeeAlgorithm::setVtkAlgorithm(vtkSmartPointer<vtkAlgorithm> vtkObject) -> void
{
    this->m_object = vtkObject;
}

auto dtkThreeDeeAlgorithm::get() -> vtkSmartPointer<vtkAlgorithm>
{
    return this->m_object;
}

auto dtkThreeDeeAlgorithm::setProp(dtkThreeDeeProp *prop) -> void
{
    this->m_prop = prop;

    for (auto alg : this->m_input) {
        alg->setProp(prop);
    }
}

auto dtkThreeDeeAlgorithm::update() -> void
{
    if (this->m_input.empty()) {
        if (this->m_prop) {
            this->m_prop->update();
        }
    } else {
        for (dtkThreeDeeAlgorithm* alg : this->m_input) {
            alg->update();
        }
    }
}

auto dtkThreeDeeAlgorithm::isValid() -> bool
{
    return true;
}

auto dtkThreeDeeAlgorithm::getInput() -> QQmlListProperty<dtkThreeDeeAlgorithm>
{
    return QQmlListProperty<dtkThreeDeeAlgorithm>(this, 0, &appendInput, &inputCount, &inputAt, &clearInputs);
}

auto dtkThreeDeeAlgorithm::appendInput(QQmlListProperty<dtkThreeDeeAlgorithm>* list, dtkThreeDeeAlgorithm* algorithm) -> void
{
    auto parent = qobject_cast<dtkThreeDeeAlgorithm*>(list->object);

    if(parent && algorithm) {
        parent->m_input.append(algorithm);
        int count = parent->m_input.count() - 1;

        parent->get()->SetInputConnection(count, algorithm->get()->GetOutputPort());

        emit parent->inputChanged();

        parent->setProp(algorithm->m_prop);
    }
}

#if QT_VERSION >= 0x060000
auto dtkThreeDeeAlgorithm::inputCount(QQmlListProperty<dtkThreeDeeAlgorithm> *list) -> qsizetype
#else
auto dtkThreeDeeAlgorithm::inputCount(QQmlListProperty<dtkThreeDeeAlgorithm> *list) -> int
#endif
{
    auto parent = qobject_cast<dtkThreeDeeAlgorithm*>(list->object);

    if (parent) {
        return parent->m_input.count();
    }

    return 0;
}

#if QT_VERSION >= 0x060000
auto dtkThreeDeeAlgorithm::inputAt(QQmlListProperty<dtkThreeDeeAlgorithm> *list, qsizetype i) -> dtkThreeDeeAlgorithm *
#else
auto dtkThreeDeeAlgorithm::inputAt(QQmlListProperty<dtkThreeDeeAlgorithm> *list, int i) -> dtkThreeDeeAlgorithm *
#endif
{
    auto parent = qobject_cast<dtkThreeDeeAlgorithm *>(list->object);

    if (parent)
        return parent->m_input.at(i);

    return 0;
}

auto dtkThreeDeeAlgorithm::clearInputs(QQmlListProperty<dtkThreeDeeAlgorithm> *list) -> void
{
    auto parent = qobject_cast<dtkThreeDeeAlgorithm*>(list->object);

    if (parent) {
        parent->m_input.clear();
        emit parent->inputChanged();
        parent->update();
    }
}

//
// dtkThreeDee+Algorithm.cpp ends here
