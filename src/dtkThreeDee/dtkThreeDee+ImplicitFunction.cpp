// dtkThreeDee+ImplicitFunction.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ImplicitFunction.hpp"

dtkThreeDeeImplicitFunction::dtkThreeDeeImplicitFunction(vtkSmartPointer<vtkImplicitFunction> vtkObject) : dtkThreeDeeObject(dtkThreeDeeObject::Type::Other), m_object(vtkObject)
{

}

dtkThreeDeeImplicitFunction::~dtkThreeDeeImplicitFunction()
{

}

auto dtkThreeDeeImplicitFunction::update() -> void
{
    for (auto callback : this->m_callbacks) {
        callback->operator()();
    }
}

auto dtkThreeDeeImplicitFunction::addCallback(cb_t&& callback) -> void
{
    if (this->m_callbacks.contains(&callback)) {
        return;
    }

    this->m_callbacks.append(&callback);
}

auto dtkThreeDeeImplicitFunction::removeCallback(cb_t&& callback) -> void
{
    this->m_callbacks.removeOne(&callback);
}

auto dtkThreeDeeImplicitFunction::get() -> vtkSmartPointer<vtkImplicitFunction>
{
    return this->m_object;
}

//
// dtkThreeDee+ImplicitFunction.cpp ends here
