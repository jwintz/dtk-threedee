// dtkThreeDee+ImplicitFunction.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QList>
#include <QQmlEngine>

#include <vtkImplicitFunction.h>
#include <vtkSmartPointer.h>

#include "dtkThreeDee+Object.hpp"

class dtkThreeDeeImplicitFunction : public dtkThreeDeeObject
{
    Q_OBJECT

    QML_NAMED_ELEMENT(ImplicitFunction)
    QML_UNCREATABLE("")

public:
    using cb_t = std::function<void(void)>;

private:
    vtkSmartPointer<vtkImplicitFunction> m_object = nullptr;
    QList<cb_t *> m_callbacks;

public:
    dtkThreeDeeImplicitFunction() = delete;
    dtkThreeDeeImplicitFunction(vtkSmartPointer<vtkImplicitFunction>);
    ~dtkThreeDeeImplicitFunction();
    auto update() -> void;
    auto addCallback(cb_t &&) -> void;
    auto removeCallback(cb_t &&) -> void;
    auto get() -> vtkSmartPointer<vtkImplicitFunction>;
};

//
// dtkThreeDee+ImplicitFunction.hpp ends here
