// dtkThreeDee+UnstructuredGridReader.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+DataReader.hpp"

#include <QQmlEngine>

class dtkThreeDeeUnstructuredGridReader : public dtkThreeDeeDataReader
{
    Q_OBJECT

    QML_NAMED_ELEMENT(UnstructuredGridReader)

public:
    dtkThreeDeeUnstructuredGridReader();
};

//
// dtkThreeDee+UnstructuredGridReader.hpp ends here
