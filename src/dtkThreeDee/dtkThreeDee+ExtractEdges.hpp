// dtkThreeDee+ExtractEdges.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkExtractEdges.h>

class dtkThreeDeeExtractEdges : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(ExtractEdges)

public:
    dtkThreeDeeExtractEdges();

private:
    vtkSmartPointer<vtkExtractEdges> m_object = nullptr;
};

//
// dtkThreeDee+ExtractEdges.hpp ends here
