// dtkThreeDee+DensifyPolyData.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkDensifyPolyData.h>

class dtkThreeDeeDensifyPolyData : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(int numberOfSubdivisions READ getNumberOfSubdivisions WRITE setNumberOfSubdivisions NOTIFY numberOfSubdivisionsChanged);

    QML_NAMED_ELEMENT(DensifyPolyData)

private:
    vtkSmartPointer<vtkDensifyPolyData> m_object = nullptr;

public:
    dtkThreeDeeDensifyPolyData();
    auto setNumberOfSubdivisions(int) -> void;
    auto getNumberOfSubdivisions() -> int;

signals:
    void numberOfSubdivisionsChanged();
};

//
// dtkThreeDee+DensifyPolyData.hpp ends here
