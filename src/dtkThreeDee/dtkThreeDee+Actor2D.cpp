// dtkThreeDee+Actor2D.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Actor2D.hpp"
#include "dtkThreeDee+Mapper2D.hpp"
#include "dtkThreeDee+Property2D.hpp"

dtkThreeDeeActor2D::dtkThreeDeeActor2D(void) : dtkThreeDeeProp(vtkSmartPointer<vtkActor2D>::New())
{
    this->m_object = vtkActor2D::SafeDownCast(dtkThreeDeeProp::get());
    this->m_property = new dtkThreeDeeProperty2D(this);
}

dtkThreeDeeActor2D::dtkThreeDeeActor2D(vtkSmartPointer<vtkActor2D> vtkObject) : dtkThreeDeeProp(vtkObject)
{
    this->m_object = vtkActor2D::SafeDownCast(vtkObject);
    this->m_property = new dtkThreeDeeProperty2D(this);
}

dtkThreeDeeActor2D::~dtkThreeDeeActor2D(void)
{
    delete this->m_property;
}

vtkSmartPointer<vtkActor2D> dtkThreeDeeActor2D::get(void)
{
    return vtkActor2D::SafeDownCast(dtkThreeDeeProp::get());
}

void dtkThreeDeeActor2D::setMapper(dtkThreeDeeMapper2D *mapper)
{
    this->m_mapper = mapper;
    this->m_object->SetMapper(mapper->get());
    mapper->setProp(this);

    emit this->mapperChanged();

    this->update();
}

dtkThreeDeeMapper2D *dtkThreeDeeActor2D::getMapper(void)
{
    return this->m_mapper;
}

dtkThreeDeeProperty2D *dtkThreeDeeActor2D::getProperty(void)
{
    return this->m_property;
}

void dtkThreeDeeActor2D::setWidth(qreal width)
{
    this->m_object->SetWidth(width);
    emit this->widthChanged();
    this->update();
}

qreal dtkThreeDeeActor2D::getWidth(void)
{
    return this->m_object->GetWidth();
}

void dtkThreeDeeActor2D::setHeight(qreal height)
{
    this->m_object->SetHeight(height);
    emit this->heightChanged();
    this->update();
}

qreal dtkThreeDeeActor2D::getHeight(void)
{
    return this->m_object->GetHeight();
}

//
// dtkThreeDee+Actor2D.cpp ends here
