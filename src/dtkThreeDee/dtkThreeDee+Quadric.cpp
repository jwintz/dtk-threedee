// dtkThreeDee+Quadric.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Quadric.hpp"

dtkThreeDeeQuadric::dtkThreeDeeQuadric(void) : dtkThreeDeeImplicitFunction(vtkSmartPointer<vtkQuadric>::New())
{
    this->m_object = vtkQuadric::SafeDownCast(dtkThreeDeeImplicitFunction::get());
}

dtkThreeDeeQuadric::dtkThreeDeeQuadric(vtkSmartPointer<vtkQuadric> object) : dtkThreeDeeImplicitFunction(object)
{
    this->m_object = vtkQuadric::SafeDownCast(dtkThreeDeeImplicitFunction::get());
}

auto dtkThreeDeeQuadric::get(void) -> vtkSmartPointer<vtkQuadric>
{
    return this->m_object;
}

dtkThreeDeeQuadric::~dtkThreeDeeQuadric(void)
{
    this->m_object = nullptr;
}

//
// dtkThreeDee+Quadric.cpp ends here
