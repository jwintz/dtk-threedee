// dtkThreeDee+MinimalStandardRandomSequence.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+RandomSequence.hpp"

#include <QQmlEngine>

#include <vtkMinimalStandardRandomSequence.h>

class dtkThreeDeeMinimalStandardRandomSequence : public dtkThreeDeeRandomSequence
{
    Q_OBJECT

    QML_NAMED_ELEMENT(MinimalStandardRandomSequence)

private:
    vtkSmartPointer<vtkMinimalStandardRandomSequence> m_object = nullptr;

public:
    dtkThreeDeeMinimalStandardRandomSequence();
};

//
// dtkThreeDee+MinimalStandardRandomSequence.hpp ends here
