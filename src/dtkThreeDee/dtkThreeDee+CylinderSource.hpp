// dtkThreeDee+CylinderSource.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"
#include "dtkThreeDee+Vector3.hpp"

#include <QQmlEngine>

#include <vtkCylinderSource.h>

class dtkThreeDeeCylinderSource : public dtkThreeDeePolyDataAlgorithm
{
  Q_OBJECT

  Q_PROPERTY(dtkThreeDeeVector3 *center READ getCenter CONSTANT);
  Q_PROPERTY(qreal height READ getHeight WRITE setHeight NOTIFY heightChanged);
  Q_PROPERTY(qreal radius READ getRadius WRITE setRadius NOTIFY radiusChanged);
  Q_PROPERTY(int resolution READ getResolution WRITE setResolution NOTIFY resolutionChanged)
  Q_PROPERTY(bool capping READ getCapping WRITE setCapping NOTIFY cappingChanged);

  QML_NAMED_ELEMENT(CylinderSource)

private:
  vtkSmartPointer<vtkCylinderSource> m_object = nullptr;
  dtkThreeDeeVector3 *m_center = nullptr;

public:
  dtkThreeDeeCylinderSource();
  auto getCenter() -> dtkThreeDeeVector3 *;
  auto setHeight(qreal) -> void;
  auto getHeight() -> qreal;
  auto setRadius(qreal) -> void;
  auto getRadius() -> qreal;
  auto setResolution(int) -> void;
  auto getResolution() -> int;
  auto setCapping(bool) -> void;
  auto getCapping() -> bool;
  
signals:
  void heightChanged();
  void radiusChanged();
  void resolutionChanged();
  void cappingChanged();
};

//
// dtkThreeDee+CylinderSource.hpp ends here
