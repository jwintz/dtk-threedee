// dtkThreeDee+ContourFilter.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QObject>

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkSmartPointer.h>
#include <vtkContourFilter.h>

class dtkThreeDeeContourFilter : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(ContourFilter)

public:
             dtkThreeDeeContourFilter(void);
             dtkThreeDeeContourFilter(vtkSmartPointer<vtkContourFilter>);
    virtual ~dtkThreeDeeContourFilter(void);

public:
    auto get(void) -> vtkSmartPointer<vtkContourFilter>;

public slots:
    void generateValues(qreal v1, qreal v2, qreal v3)
    {
        this->m_object->GenerateValues(v1, v2, v3);
        this->update();
    }

private:
    vtkSmartPointer<vtkContourFilter> m_object = nullptr;
};

//
// dtkThreeDee+ContourFilter.hpp ends here
