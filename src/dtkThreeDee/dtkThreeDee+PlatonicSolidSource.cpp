// dtkThreeDee+PlatonicSolidSource.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+PlatonicSolidSource.hpp"

dtkThreeDeePlatonicSolidSource::dtkThreeDeePlatonicSolidSource() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkPlatonicSolidSource>::New())
{
    this->m_object = vtkPlatonicSolidSource::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeePlatonicSolidSource::setSolidType(SolidType solidType) -> void {
    this->m_object->SetSolidType(solidType);
    this->update();
    emit this->solidTypeChanged();
}

auto dtkThreeDeePlatonicSolidSource::getSolidType() -> SolidType {
    return static_cast<SolidType>(this->m_object->GetSolidType());
}

//
// dtkThreeDee+PlatonicSolidSource.cpp ends here
