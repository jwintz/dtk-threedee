// dtkThreeDee+ArrowSource.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ArrowSource.hpp"

dtkThreeDeeArrowSource::dtkThreeDeeArrowSource() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkArrowSource>::New()) {
    this->m_object = vtkArrowSource::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeArrowSource::setTipResolution(int tipResolution) -> void {
    this->m_object->SetTipResolution(tipResolution);
    emit this->tipResolutionChanged();
    this->update();
}

auto dtkThreeDeeArrowSource::getTipResolution() -> int {
    return this->m_object->GetTipResolution();
}

auto dtkThreeDeeArrowSource::setTipLength(qreal tipLength) -> void {
    this->m_object->SetTipLength(tipLength);
    emit this->tipLengthChanged();
    this->update();
}

auto dtkThreeDeeArrowSource::getTipLength() -> qreal {
    return this->m_object->GetTipLength();
}

auto dtkThreeDeeArrowSource::setTipRadius(qreal tipRadius) -> void {
    this->m_object->SetTipRadius(tipRadius);
    emit this->tipRadiusChanged();
    this->update();
}

auto dtkThreeDeeArrowSource::getTipRadius() -> qreal {
    return this->m_object->GetTipRadius();
}

auto dtkThreeDeeArrowSource::setShaftResolution(int shaftResolution) -> void {
    this->m_object->SetShaftResolution(shaftResolution);
    emit this->shaftResolutionChanged();
    this->update();
}

auto dtkThreeDeeArrowSource::getShaftResolution() -> int {
    return this->m_object->GetShaftResolution();
}

auto dtkThreeDeeArrowSource::setShaftRadius(qreal shaftRadius) -> void {
    this->m_object->SetShaftRadius(shaftRadius);
    emit this->shaftRadiusChanged();
    this->update();
}

auto dtkThreeDeeArrowSource::getShaftRadius() -> qreal {
    return this->m_object->GetShaftRadius();
}

auto dtkThreeDeeArrowSource::setInvert(bool invert) -> void {
    this->m_object->SetInvert(invert);
    emit this->invertChanged();
    this->update();
}

auto dtkThreeDeeArrowSource::getInvert() -> bool {
    return this->m_object->GetInvert();
}

//
// dtkThreeDee+ArrowSource.cpp ends here
