// dtkThreeDee+AbstractVolumeMapper.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+AbstractMapper3D.hpp"

#include <QQmlEngine>

#include <vtkAbstractVolumeMapper.h>

class dtkThreeDeeAbstractVolumeMapper : public dtkThreeDeeAbstractMapper3D
{
    Q_OBJECT

    QML_NAMED_ELEMENT(AbstractVolumeMapper)
    QML_UNCREATABLE("")

public:
    dtkThreeDeeAbstractVolumeMapper(vtkSmartPointer<vtkAbstractVolumeMapper>);
    vtkSmartPointer<vtkAbstractVolumeMapper> get(void);
};

//
// dtkThreeDee+AbstractVolumeMapper.hpp ends here
