// dtkThreeDee+TextSource.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+TextSource.hpp"

dtkThreeDeeTextSource::dtkThreeDeeTextSource() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkTextSource>::New()) {
    this->m_object = vtkTextSource::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeTextSource::setText(const QString& text) -> void {
    this->m_object->SetText(text.toStdString().c_str());
    this->update();
    emit this->textChanged();
}

auto dtkThreeDeeTextSource::getText() -> QString {
    return this->m_object->GetText();
}

auto dtkThreeDeeTextSource::setBacking(bool backing) -> void {
    this->m_object->SetBacking(backing);
    this->update();
    emit this->backingChanged();
}

auto dtkThreeDeeTextSource::getBacking() -> bool {
    return this->m_object->GetBacking();
}

auto dtkThreeDeeTextSource::setForegroundColor(const QColor& foregroundColor) -> void {
    this->m_foregroundColor = foregroundColor;
    this->m_object->SetForegroundColor(foregroundColor.redF(), foregroundColor.greenF(), foregroundColor.blueF());
    this->update();
    emit this->foregroundColorChanged();
}

auto dtkThreeDeeTextSource::getForegroundColor() -> QColor {
    return this->m_foregroundColor;
}

auto dtkThreeDeeTextSource::setBackgroundColor(const QColor& backgroundColor) -> void {
    this->m_backgroundColor = backgroundColor;
    this->m_object->SetBackgroundColor(backgroundColor.redF(), backgroundColor.greenF(), backgroundColor.blueF());
    this->update();
    emit this->backgroundColorChanged();
}

auto dtkThreeDeeTextSource::getBackgroundColor() -> QColor {
    return this->m_backgroundColor;
}

//
// dtkThreeDee+TextSource.cpp ends here
