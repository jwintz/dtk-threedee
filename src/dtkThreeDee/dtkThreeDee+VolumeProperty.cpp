// dtkThreeDee+VolumeProperty.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+VolumeProperty.hpp"
#include "dtkThreeDee+Volume.hpp"
#include "dtkThreeDee+PiecewiseFunction.hpp"
#include "dtkThreeDee+ColorTransferFunction.hpp"

dtkThreeDeeVolumeProperty::dtkThreeDeeVolumeProperty(dtkThreeDeeVolume* volume) : m_volume(volume), m_volume_ptr(volume->get())
{
    auto property = this->m_volume_ptr->GetProperty();

    this->m_gradientOpacityFunction = new dtkThreeDeePiecewiseFunction(property->GetGradientOpacity(), [this](){ this->update(); });
    this->m_scalarOpacityFunction = new dtkThreeDeePiecewiseFunction(property->GetScalarOpacity(), [this](){ this->update(); });
    this->m_transferFunction = new dtkThreeDeeColorTransferFunction(property->GetRGBTransferFunction(), [this](){ this->update(); });
}

auto dtkThreeDeeVolumeProperty::update() -> void {
    this->m_volume_ptr->Update();
}

auto dtkThreeDeeVolumeProperty::setShade(bool shade) -> void {
    this->m_volume_ptr->GetProperty()->SetShade(shade);
    emit this->shadeChanged();
    this->update();
}

auto dtkThreeDeeVolumeProperty::getShade() -> bool {
    return this->m_volume_ptr->GetProperty()->GetShade();
}

auto dtkThreeDeeVolumeProperty::setAmbient(qreal ambient) -> void {
    this->m_volume_ptr->GetProperty()->SetAmbient(ambient);
    emit this->ambientChanged();
    this->update();
}

auto dtkThreeDeeVolumeProperty::getAmbient() -> qreal {
    return this->m_volume_ptr->GetProperty()->GetAmbient();
}

auto dtkThreeDeeVolumeProperty::setDiffuse(qreal diffuse) -> void {
    this->m_volume_ptr->GetProperty()->SetDiffuse(diffuse);
    emit this->diffuseChanged();
    this->update();
}

auto dtkThreeDeeVolumeProperty::getDiffuse() -> qreal {
    return this->m_volume_ptr->GetProperty()->GetDiffuse();
}

auto dtkThreeDeeVolumeProperty::setSpecular(qreal specular) -> void {
    this->m_volume_ptr->GetProperty()->SetSpecular(specular);
    emit this->specularChanged();
    this->update();
}

auto dtkThreeDeeVolumeProperty::getSpecular() -> qreal {
    return this->m_volume_ptr->GetProperty()->GetSpecular();
}

auto dtkThreeDeeVolumeProperty::setSpecularPower(qreal specularPower) -> void {
    this->m_volume_ptr->GetProperty()->SetSpecularPower(specularPower);
    emit this->specularPowerChanged();
    this->update();
}

auto dtkThreeDeeVolumeProperty::getSpecularPower() -> qreal {
    return this->m_volume_ptr->GetProperty()->GetSpecularPower();
}

auto dtkThreeDeeVolumeProperty::getGradientOpacityFunction() -> dtkThreeDeePiecewiseFunction* {
    return this->m_gradientOpacityFunction;
}

auto dtkThreeDeeVolumeProperty::getScalarOpacityFunction() -> dtkThreeDeePiecewiseFunction* {
    return this->m_scalarOpacityFunction;
}

auto dtkThreeDeeVolumeProperty::getTransferFunction() -> dtkThreeDeeColorTransferFunction* {
    return this->m_transferFunction;
}

//
// dtkThreeDee+VolumeProperty.cpp ends here
