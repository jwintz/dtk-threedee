// dtkThreeDee+InterpolatingSubdivisionFilter.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QQmlEngine>

#include <vtkInterpolatingSubdivisionFilter.h>

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

class dtkThreeDeeInterpolatingSubdivisionFilter : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(int numberOfSubdivisions READ getNumberOfSubdivisions WRITE setNumberOfSubdivisions NOTIFY numberOfSubdivisionsChanged);

    QML_NAMED_ELEMENT(InterpolatingSubdivisionFilter)
    QML_UNCREATABLE("")

private:
    vtkSmartPointer<vtkInterpolatingSubdivisionFilter> m_object = nullptr;

public:
    dtkThreeDeeInterpolatingSubdivisionFilter(vtkSmartPointer<vtkInterpolatingSubdivisionFilter>);
    auto setNumberOfSubdivisions(int) -> void;
    auto getNumberOfSubdivisions() -> int;

signals:
    void numberOfSubdivisionsChanged();
};

//
// dtkThreeDee+InterpolatingSubdivisionFilter.hpp ends here
