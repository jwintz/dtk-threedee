// dtkThreeDee+DataReader.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QQmlEngine>

#include <vtkDataReader.h>

#include "dtkThreeDee+Algorithm.hpp"

class dtkThreeDeeDataReader : public dtkThreeDeeAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(QString fileName READ getFileName WRITE setFileName NOTIFY fileNameChanged);

    QML_NAMED_ELEMENT(DataReader)
    QML_UNCREATABLE("")

private:
    vtkSmartPointer<vtkDataReader> m_object = nullptr;

public:
    dtkThreeDeeDataReader(vtkSmartPointer<vtkDataReader>);
    auto setFileName(const QString&) -> void;
    auto getFileName() -> QString;

signals:
    void fileNameChanged();
};

//
// dtkThreeDee+DataReader.hpp ends here
