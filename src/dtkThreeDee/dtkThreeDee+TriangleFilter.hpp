// dtkThreeDee+TriangleFilter.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkTriangleFilter.h>

class dtkThreeDeeTriangleFilter : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(bool passVerts READ getPassVerts WRITE setPassVerts NOTIFY passVertsChanged);
    Q_PROPERTY(bool passLines READ getPassLines WRITE setPassLines NOTIFY passLinesChanged);

    QML_NAMED_ELEMENT(TriangleFilter)

private:
    vtkSmartPointer<vtkTriangleFilter> m_object = nullptr;

public:
    dtkThreeDeeTriangleFilter();
    auto setPassVerts(bool) -> void;
    auto setPassLines(bool) -> void;
    auto getPassVerts() -> bool;
    auto getPassLines() -> bool;

signals:
    void passVertsChanged();
    void passLinesChanged();
};

//
// dtkThreeDee+TriangleFilter.hpp ends here
