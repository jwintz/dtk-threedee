// dtkThreeDee+ThreadedImageAlgorithm.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+ImageAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkThreadedImageAlgorithm.h>

class dtkThreeDeeThreadedImageAlgorithm : public dtkThreeDeeImageAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(ThreadedImageAlgorithm)
    QML_UNCREATABLE("")

public:
    dtkThreeDeeThreadedImageAlgorithm(vtkSmartPointer<vtkThreadedImageAlgorithm>);
};

//
// dtkThreeDee+ThreadedImageAlgorithm.hpp ends here
