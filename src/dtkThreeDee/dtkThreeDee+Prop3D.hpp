// dtkThreeDee+Prop3D.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Prop.hpp"
#include "dtkThreeDee+Vector3.hpp"

#include <QQmlEngine>

#include <vtkSmartPointer.h>
#include <vtkProp3D.h>

class dtkThreeDeeProp3D : public dtkThreeDeeProp
{
    Q_OBJECT

    Q_PROPERTY(dtkThreeDeeVector3 *scale READ getScale CONSTANT);
    Q_PROPERTY(dtkThreeDeeVector3 *origin READ getOrigin CONSTANT);
    Q_PROPERTY(dtkThreeDeeVector3 *position READ getPosition CONSTANT);
    Q_PROPERTY(dtkThreeDeeVector3 *orientation READ getOrientation CONSTANT);

    QML_NAMED_ELEMENT(Prop3D)
    QML_UNCREATABLE("")

public:
    dtkThreeDeeProp3D(vtkSmartPointer<vtkProp3D>);
    dtkThreeDeeVector3 *getScale(void);
    dtkThreeDeeVector3 *getOrigin(void);
    dtkThreeDeeVector3 *getPosition(void);
    dtkThreeDeeVector3 *getOrientation(void);

private:
    dtkThreeDeeVector3 *m_scale = nullptr;
    dtkThreeDeeVector3 *m_origin = nullptr;
    dtkThreeDeeVector3 *m_position = nullptr;
    dtkThreeDeeVector3 *m_orientation = nullptr;
    vtkSmartPointer<vtkProp3D> m_object = nullptr;
};

//
// dtkThreeDee+Prop3D.hpp ends here
