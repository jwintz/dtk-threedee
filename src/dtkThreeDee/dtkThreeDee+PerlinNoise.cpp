// dtkThreeDee+PerlinNoise.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+PerlinNoise.hpp"

dtkThreeDeePerlinNoise::dtkThreeDeePerlinNoise() : dtkThreeDeeImplicitFunction(vtkSmartPointer<vtkPerlinNoise>::New()) {
    this->m_object = vtkPerlinNoise::SafeDownCast(dtkThreeDeeImplicitFunction::get());

    this->m_frequency = new dtkThreeDeeVector3([this](){
        this->m_object->SetFrequency(this->m_frequency->getValues().data());
        this->update();
    });

    this->m_phase = new dtkThreeDeeVector3([this](){
        this->m_object->SetPhase(this->m_phase->getValues().data());
        this->update();
    });
}

auto dtkThreeDeePerlinNoise::setAmplitude(qreal amplitude) -> void {
    this->m_object->SetAmplitude(amplitude);
    this->update();
}

auto dtkThreeDeePerlinNoise::getAmplitude() -> qreal {
    return this->m_object->GetAmplitude();
}

auto dtkThreeDeePerlinNoise::getFrequency() -> dtkThreeDeeVector3* {
    return this->m_frequency;
}

auto dtkThreeDeePerlinNoise::getPhase() -> dtkThreeDeeVector3* {
    return this->m_phase;
}

//
// dtkThreeDee+PerlinNoise.cpp ends here
