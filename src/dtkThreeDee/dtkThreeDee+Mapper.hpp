// dtkThreeDee+Mapper.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QQmlEngine>

#include <vtkMapper.h>

#include "dtkThreeDee+AbstractMapper3D.hpp"

class dtkThreeDeeScalarsToColors;

class dtkThreeDeeMapper : public dtkThreeDeeAbstractMapper3D
{
    Q_OBJECT

    QML_NAMED_ELEMENT(Mapper)
    QML_UNCREATABLE("")

public:
    enum ScalarMode {
        ScalarModeDefault = VTK_SCALAR_MODE_DEFAULT,
        ScalarModeUsePointData = VTK_SCALAR_MODE_USE_POINT_DATA,
        ScalarModeUseCellData = VTK_SCALAR_MODE_USE_CELL_DATA,
        ScalarModeUsePointFieldData = VTK_SCALAR_MODE_USE_POINT_FIELD_DATA,
        ScalarModeUseCellFieldData = VTK_SCALAR_MODE_USE_CELL_FIELD_DATA
    };

private:
    Q_ENUM(ScalarMode);
    Q_PROPERTY(ScalarMode scalarMode READ getScalarMode WRITE setScalarMode NOTIFY scalarModeChanged);
    Q_PROPERTY(bool scalarVisibility READ getScalarVisibility WRITE setScalarVisibility NOTIFY scalarVisibilityChanged);

private:
    vtkSmartPointer<vtkMapper> m_object = nullptr;

public:
    dtkThreeDeeMapper(vtkSmartPointer<vtkMapper>);
    auto get() -> vtkSmartPointer<vtkMapper>;
    auto setScalarMode(ScalarMode) -> void;
    auto getScalarMode() -> ScalarMode;
    auto setScalarVisibility(bool) -> void;
    auto getScalarVisibility() -> bool;

signals:
    void scalarVisibilityChanged();
    void scalarModeChanged();
};

//
// dtkThreeDee+Mapper.hpp ends here
