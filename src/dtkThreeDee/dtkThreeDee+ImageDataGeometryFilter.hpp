// dtkThreeDee+ImageDataGeometryFilter.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkImageDataGeometryFilter.h>

class dtkThreeDeeImageDataGeometryFilter : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(ImageDataGeometryFilter)

private:
    vtkSmartPointer<vtkImageDataGeometryFilter> m_object = nullptr;

public:
    dtkThreeDeeImageDataGeometryFilter();
};

//
// dtkThreeDee+ImageDataGeometryFilter.hpp ends here
