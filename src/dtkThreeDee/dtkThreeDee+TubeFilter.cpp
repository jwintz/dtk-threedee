// dtkThreeDee+TubeFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+TubeFilter.hpp"

dtkThreeDeeTubeFilter::dtkThreeDeeTubeFilter() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkTubeFilter>::New()) {
    this->m_object = vtkTubeFilter::SafeDownCast(dtkThreeDeeAlgorithm::get());

    this->m_defaultNormal = new dtkThreeDeeVector3([this](){
        this->m_object->SetDefaultNormal(this->m_defaultNormal->getValues().data());
        this->update();
    });
}

auto dtkThreeDeeTubeFilter::setVaryRadius(VaryRadius varyRadius) -> void {
    this->m_object->SetVaryRadius(varyRadius);
    this->update();
    emit this->varyRadiusChanged();
}

auto dtkThreeDeeTubeFilter::getVaryRadius() -> VaryRadius {
    return static_cast<VaryRadius>(this->m_object->GetVaryRadius());
}

auto dtkThreeDeeTubeFilter::setGenerateTCoords(TCoords tCoords) -> void {
    this->m_object->SetGenerateTCoords(tCoords);
    this->update();
    emit this->generateTCoordsChanged();
}

auto dtkThreeDeeTubeFilter::getGenerateTCoords() -> TCoords {
    return static_cast<TCoords>(this->m_object->GetGenerateTCoords());
}

auto dtkThreeDeeTubeFilter::setRadius(qreal radius) -> void {
    this->m_object->SetRadius(radius);
    this->update();
    emit this->radiusChanged();
}

auto dtkThreeDeeTubeFilter::getRadius() -> qreal {
    return this->m_object->GetRadius();
}

auto dtkThreeDeeTubeFilter::setTextureLength(qreal textureLength) -> void {
    this->m_object->SetTextureLength(textureLength);
    this->update();
    emit this->textureLengthChanged();
}

auto dtkThreeDeeTubeFilter::getTextureLength() -> qreal {
    return this->m_object->GetTextureLength();
}

auto dtkThreeDeeTubeFilter::setNumberOfSides(int numberOfSides) -> void {
    this->m_object->SetNumberOfSides(numberOfSides);
    this->update();
    emit this->numberOfSidesChanged();
}

auto dtkThreeDeeTubeFilter::getNumberOfSides() -> int {
    return this->m_object->GetNumberOfSides();
}

auto dtkThreeDeeTubeFilter::setRadiusFactor(qreal radiusFactor) -> void {
    this->m_object->SetRadiusFactor(radiusFactor);
    this->update();
    emit this->radiusFactorChanged();
}

auto dtkThreeDeeTubeFilter::getRadiusFactor() -> qreal {
    return this->m_object->GetRadiusFactor();
}

auto dtkThreeDeeTubeFilter::setUseDefaultNormal(bool useDefaultNormal) -> void {
    this->m_object->SetUseDefaultNormal(useDefaultNormal);
    this->update();
    emit this->useDefaultNormalChanged();
}

auto dtkThreeDeeTubeFilter::getUseDefaultNormal() -> bool {
    return this->m_object->GetUseDefaultNormal();
}

auto dtkThreeDeeTubeFilter::setSidesShareVertices(bool sidesShareVertices) -> void {
    this->m_object->SetSidesShareVertices(sidesShareVertices);
    this->update();
    emit this->sidesShareVerticesChanged();
}

auto dtkThreeDeeTubeFilter::getSidesShareVertices() -> bool {
    return this->m_object->GetSidesShareVertices();
}

auto dtkThreeDeeTubeFilter::setCapping(bool capping) -> void {
    this->m_object->SetCapping(capping);
    this->update();
    emit this->cappingChanged();
}

auto dtkThreeDeeTubeFilter::getCapping() -> bool {
    return this->m_object->GetCapping();
}

auto dtkThreeDeeTubeFilter::setOnRatio(int onRatio) -> void {
    this->m_object->SetOnRatio(onRatio);
    this->update();
    emit this->onRatioChanged();
}

auto dtkThreeDeeTubeFilter::getOnRatio() -> int {
    return this->m_object->GetOnRatio();
}

auto dtkThreeDeeTubeFilter::setOffset(int offset) -> void {
    this->m_object->SetOffset(offset);
    this->update();
    emit this->offsetChanged();
}

auto dtkThreeDeeTubeFilter::getOffset() -> int {
    return this->m_object->GetOffset();
}

auto dtkThreeDeeTubeFilter::getDefaultNormal() -> dtkThreeDeeVector3* {
    return this->m_defaultNormal;
}

//
// dtkThreeDee+TubeFilter.cpp ends here
