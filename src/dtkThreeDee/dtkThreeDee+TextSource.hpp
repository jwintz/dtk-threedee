// dtkThreeDee+TextSource.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QColor>
#include <QQmlEngine>

#include <vtkTextSource.h>

class dtkThreeDeeTextSource : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(QString text READ getText WRITE setText NOTIFY textChanged);
    Q_PROPERTY(bool backing READ getBacking WRITE setBacking NOTIFY backingChanged);
    Q_PROPERTY(QColor foregroundColor READ getForegroundColor WRITE setForegroundColor NOTIFY foregroundColorChanged);
    Q_PROPERTY(QColor backgroundColor READ getBackgroundColor WRITE setBackgroundColor NOTIFY backgroundColorChanged);

    QML_NAMED_ELEMENT(TextSource)

private:
    vtkSmartPointer<vtkTextSource> m_object = nullptr;
    QColor m_foregroundColor;
    QColor m_backgroundColor;

public:
    dtkThreeDeeTextSource();
    auto setText(const QString&) -> void;
    auto getText() -> QString;
    auto setBacking(bool) -> void;
    auto getBacking() -> bool;
    auto setForegroundColor(const QColor&) -> void;
    auto getForegroundColor() -> QColor;
    auto setBackgroundColor(const QColor&) -> void;
    auto getBackgroundColor() -> QColor;

signals:
    void textChanged();
    void backingChanged();
    void foregroundColorChanged();
    void backgroundColorChanged();
};

//
// dtkThreeDee+TextSource.hpp ends here
