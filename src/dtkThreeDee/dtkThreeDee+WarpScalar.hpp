// dtkThreeDee+WarpScalar.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PointSetAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkWarpScalar.h>

class dtkThreeDeeWarpScalar : public dtkThreeDeePointSetAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(qreal scaleFactor READ getScaleFactor WRITE setScaleFactor NOTIFY scaleFactorChanged);
    Q_PROPERTY(bool useNormal READ getUseNormal WRITE setUseNormal NOTIFY useNormalChanged);

    QML_NAMED_ELEMENT(WarpScalar)

private:
    vtkSmartPointer<vtkWarpScalar> m_object = nullptr;

public:
    dtkThreeDeeWarpScalar();
    auto setScaleFactor(qreal) -> void;
    auto getScaleFactor() -> qreal;
    auto setUseNormal(bool) -> void;
    auto getUseNormal() -> bool;

signals:
    void scaleFactorChanged();
    void useNormalChanged();
};

//
// dtkThreeDee+WarpScalar.hpp ends here
