// dtkThreeDee+AbstractMapper.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Prop.hpp"
#include "dtkThreeDee+AbstractMapper.hpp"

dtkThreeDeeAbstractMapper::dtkThreeDeeAbstractMapper(vtkSmartPointer<vtkAbstractMapper> vtkObject) : dtkThreeDeeAlgorithm(vtkObject)
{

}

//
// dtkThreeDee+AbstractMapper.cpp ends here
