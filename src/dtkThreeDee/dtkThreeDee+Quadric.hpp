// dtkThreeDee+Quadric.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+ImplicitFunction.hpp"

#include <QObject>
#include <QQmlEngine>

#include <vtkSmartPointer.h>
#include <vtkQuadric.h>

class dtkThreeDeeQuadric : public dtkThreeDeeImplicitFunction
{
    Q_OBJECT

    QML_NAMED_ELEMENT(Quadric)

public:
             dtkThreeDeeQuadric(void);
             dtkThreeDeeQuadric(vtkSmartPointer<vtkQuadric>);
    virtual ~dtkThreeDeeQuadric(void);

public:
    auto get(void) -> vtkSmartPointer<vtkQuadric>;

public slots:
    void setCoefficients(qreal a, qreal b, qreal c, qreal d, qreal e, qreal f, qreal g, qreal h, qreal i, qreal j)
    {
        this->m_object->SetCoefficients(a, b, c, d, e, f, g, h, i, j);
        this->update();
    }

private:
    vtkSmartPointer<vtkQuadric> m_object = nullptr;
};

//
// dtkThreeDee+Quadric.hpp ends here
