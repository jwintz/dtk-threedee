// dtkThreeDee+Prop3D.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Prop3D.hpp"

dtkThreeDeeProp3D::dtkThreeDeeProp3D(vtkSmartPointer<vtkProp3D> vtkObject) : dtkThreeDeeProp(vtkObject)
{
    this->m_object = vtkProp3D::SafeDownCast(vtkObject);

    this->m_position = new dtkThreeDeeVector3([this] ()
    {
        this->m_object->SetPosition(this->m_position->getValues().data());
        this->update();
    });

    this->m_origin = new dtkThreeDeeVector3([this] ()
    {
        this->m_object->SetOrigin(this->m_origin->getValues().data());
        this->update();
    });

    this->m_orientation = new dtkThreeDeeVector3([this]()
    {
        this->m_object->SetOrientation(this->m_orientation->getValues().data());
        this->update();
    });

    this->m_scale = new dtkThreeDeeVector3([this] ()
    {
        this->m_object->SetScale(this->m_scale->getValues().data());
        this->update();
    });
}

dtkThreeDeeVector3 *dtkThreeDeeProp3D::getPosition(void)
{
    return this->m_position;
}

dtkThreeDeeVector3 *dtkThreeDeeProp3D::getOrientation(void)
{
    return this->m_orientation;
}

dtkThreeDeeVector3 *dtkThreeDeeProp3D::getOrigin(void)
{
    return this->m_origin;
}

dtkThreeDeeVector3 *dtkThreeDeeProp3D::getScale(void)
{
    return this->m_scale;
}

//
// dtkThreeDee+Prop3D.cpp ends here
