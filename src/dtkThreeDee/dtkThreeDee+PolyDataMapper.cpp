// dtkThreeDee+PolyDataMapper.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+PolyDataMapper.hpp"
#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <vtkAlgorithm.h>

dtkThreeDeePolyDataMapper::dtkThreeDeePolyDataMapper() : dtkThreeDeeMapper(vtkSmartPointer<vtkPolyDataMapper>::New())
{

}

//
// dtkThreeDee+PolyDataMapper.cpp ends here
