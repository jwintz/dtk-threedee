// dtkThreeDee+LoopSubdivisionFilter.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+ApproximatingSubdivisionFilter.hpp"

#include <QQmlEngine>

#include <vtkLoopSubdivisionFilter.h>

class dtkThreeDeeLoopSubdivisionFilter : public dtkThreeDeeApproximatingSubdivisionFilter
{
    Q_OBJECT

    QML_NAMED_ELEMENT(LoopSubdivisionFilter)

public:
    dtkThreeDeeLoopSubdivisionFilter();
};

//
// dtkThreeDee+LoopSubdivisionFilter.hpp ends here
