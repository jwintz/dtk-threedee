// dtkThreeDee+SmartVolumeMapper.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+VolumeMapper.hpp"

#include <QQmlEngine>

#include <vtkSmartVolumeMapper.h>

class dtkThreeDeeSmartVolumeMapper : public dtkThreeDeeVolumeMapper
{
    Q_OBJECT

    Q_PROPERTY(qreal finalColorWindow READ getFinalColorWindow WRITE setFinalColorWindow NOTIFY finalColorWindowChanged);
    Q_PROPERTY(qreal finalColorLevel READ getFinalColorLevel WRITE setFinalColorLevel NOTIFY finalColorLevelChanged);

    QML_NAMED_ELEMENT(SmartVolumeMapper)

private:
    vtkSmartPointer<vtkSmartVolumeMapper> m_object = nullptr;

public:
    dtkThreeDeeSmartVolumeMapper();
    auto setFinalColorWindow(qreal) -> void;
    auto getFinalColorWindow() -> qreal;
    auto setFinalColorLevel(qreal) -> void;
    auto getFinalColorLevel() -> qreal;

signals:
    void finalColorWindowChanged();
    void finalColorLevelChanged();
};

//
// dtkThreeDee+SmartVolumeMapper.hpp ends here
