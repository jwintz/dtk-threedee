// dtkThreeDee+SampleFunction.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+ImageAlgorithm.hpp"
#include "dtkThreeDee+ImplicitFunction.hpp"
#include "dtkThreeDee+Vector3.hpp"

#include <QObject>
#include <QQmlEngine>

#include <vtkSmartPointer.h>
#include <vtkSampleFunction.h>

class dtkThreeDeeSampleFunction : public dtkThreeDeeImageAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(dtkThreeDeeVector3* sampleDimension READ getSampleDimension CONSTANT);
    Q_PROPERTY(dtkThreeDeeImplicitFunction *implicitFunction READ getImplicitFunction WRITE setImplicitFunction)

    QML_NAMED_ELEMENT(SampleFunction)

public:
             dtkThreeDeeSampleFunction(void);
             dtkThreeDeeSampleFunction(vtkSmartPointer<vtkSampleFunction>);
    virtual ~dtkThreeDeeSampleFunction(void);

public:
    auto get(void) -> vtkSmartPointer<vtkSampleFunction>;

public:
    auto getSampleDimension()
    {
        return m_sampleDimension;
    }

    auto getImplicitFunction(void)
    {
        return m_implicitFunction;
    }

public:
    void setImplicitFunction(dtkThreeDeeImplicitFunction *function)
    {
        this->m_object->SetImplicitFunction(function->get());
        this->update();
    }

private:
    vtkSmartPointer<vtkSampleFunction> m_object = nullptr;

    dtkThreeDeeImplicitFunction *m_implicitFunction = nullptr;

private:
    dtkThreeDeeVector3* m_sampleDimension = nullptr;

};

//
// dtkThreeDee+SampleFunction.hpp ends here
