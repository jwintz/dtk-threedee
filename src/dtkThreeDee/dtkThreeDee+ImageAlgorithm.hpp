// dtkThreeDee+ImageAlgorithm.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Algorithm.hpp"

#include <QQmlEngine>

#include <vtkImageAlgorithm.h>

class dtkThreeDeeImageAlgorithm : public dtkThreeDeeAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(ImageAlgorithm)
    QML_UNCREATABLE("")

public:
    dtkThreeDeeImageAlgorithm(vtkSmartPointer<vtkImageAlgorithm>);
};

//
// dtkThreeDee+ImageAlgorithm.hpp ends here
