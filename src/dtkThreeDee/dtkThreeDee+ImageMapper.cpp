// dtkThreeDee+ImageMapper.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ImageMapper.hpp"

dtkThreeDeeImageMapper::dtkThreeDeeImageMapper() : dtkThreeDeeMapper2D(vtkSmartPointer<vtkImageMapper>::New()) {
    this->m_object = vtkImageMapper::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeImageMapper::setColorWindow(qreal colorWindow) -> void {
    this->m_object->SetColorWindow(colorWindow);
    emit this->colorWindowChanged();
    emit this->colorScaleChanged();
    emit this->colorShiftChanged();

    this->update();
}

auto dtkThreeDeeImageMapper::getColorWindow() -> qreal {
    return this->m_object->GetColorWindow();
}

auto dtkThreeDeeImageMapper::setColorLevel(qreal colorLevel) -> void {
    this->m_object->SetColorLevel(colorLevel);
    emit this->colorLevelChanged();
    emit this->colorScaleChanged();
    emit this->colorShiftChanged();

    this->update();
}

auto dtkThreeDeeImageMapper::getColorLevel() -> qreal {
    return this->m_object->GetColorLevel();
}

auto dtkThreeDeeImageMapper::getColorShift() -> qreal {
    return this->m_object->GetColorShift();
}

auto dtkThreeDeeImageMapper::getColorScale() -> qreal {
    return this->m_object->GetColorScale();
}

//
// dtkThreeDee+ImageMapper.cpp ends here
