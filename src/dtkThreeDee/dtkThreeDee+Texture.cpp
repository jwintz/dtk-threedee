// dtkThreeDee+Texture.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Texture.hpp"

dtkThreeDeeTexture::dtkThreeDeeTexture(vtkSmartPointer<vtkTexture> vtkObject, cb_t&& callback) : dtkThreeDeeImageAlgorithm(vtkObject), m_object(vtkObject), m_callback(callback)
{

}

auto dtkThreeDeeTexture::setQuality(Quality quality) -> void {
    this->m_object->SetQuality(quality);
    emit this->qualityChanged();
    this->update();
}

auto dtkThreeDeeTexture::getQuality() -> Quality {
    return static_cast<Quality>(this->m_object->GetQuality());
}

auto dtkThreeDeeTexture::setBlendingMode(BlendingMode blendingMode) -> void {
    this->m_object->SetBlendingMode(blendingMode);
    emit this->blendingModeChanged();
    this->update();
}

auto dtkThreeDeeTexture::getBlendingMode() -> BlendingMode {
    return static_cast<BlendingMode>(this->m_object->GetBlendingMode());
}

auto dtkThreeDeeTexture::setRepeat(bool repeat) -> void {
    this->m_object->SetRepeat(repeat);
    emit this->repeatChanged();
    this->update();
}

auto dtkThreeDeeTexture::getRepeat() -> bool {
    return this->m_object->GetRepeat();
}

auto dtkThreeDeeTexture::setEdgeClamp(bool edgeClamp) -> void {
    this->m_object->SetEdgeClamp(edgeClamp);
    emit this->edgeClampChanged();
    this->update();
}

auto dtkThreeDeeTexture::getEdgeClamp() -> bool {
    return this->m_object->GetEdgeClamp();
}

auto dtkThreeDeeTexture::setInterpolate(bool interpolate) -> void {
    this->m_object->SetInterpolate(interpolate);
    emit this->interpolateChanged();
    this->update();
}

auto dtkThreeDeeTexture::getInterpolate() -> bool {
    return this->m_object->GetInterpolate();
}

//
// dtkThreeDee+Texture.cpp ends here
