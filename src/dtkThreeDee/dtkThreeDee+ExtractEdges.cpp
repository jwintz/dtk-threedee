// dtkThreeDee+ExtractEdges.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ExtractEdges.hpp"

dtkThreeDeeExtractEdges::dtkThreeDeeExtractEdges() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkExtractEdges>::New())
{
    this->m_object = vtkExtractEdges::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

//
// dtkThreeDee+ExtractEdges.cpp ends here
