// dtkThreeDee+Property2D.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Property2D.hpp"
#include "dtkThreeDee+Actor2D.hpp"

dtkThreeDeeProperty2D::dtkThreeDeeProperty2D(dtkThreeDeeActor2D* actor2D) : m_actor2D(actor2D), m_vtkActor2D(actor2D->get())
{

}

auto dtkThreeDeeProperty2D::update() -> void
{
    this->m_actor2D->update();
}

auto dtkThreeDeeProperty2D::setColor(const QColor& color) -> void
{
    this->m_color = color;

    this->m_vtkActor2D->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
    this->update();

    emit this->colorChanged();
}

auto dtkThreeDeeProperty2D::getColor() -> QColor
{
    return this->m_color;
}

auto dtkThreeDeeProperty2D::setOpacity(qreal opacity) -> void
{
    this->m_vtkActor2D->GetProperty()->SetOpacity(opacity);
    this->update();

    emit this->opacityChanged();
}

auto dtkThreeDeeProperty2D::getOpacity() -> qreal
{
    return this->m_vtkActor2D->GetProperty()->GetOpacity();
}

//
// dtkThreeDee+Property2D.cpp ends here
