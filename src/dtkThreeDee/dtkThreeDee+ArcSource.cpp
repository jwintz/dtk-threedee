// dtkThreeDee+ArcSource.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ArcSource.hpp"

dtkThreeDeeArcSource::dtkThreeDeeArcSource(void) : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkArcSource>::New())
{
    this->m_object = vtkArcSource::SafeDownCast(dtkThreeDeeAlgorithm::get());

    this->m_center = new dtkThreeDeeVector3([this]() {
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->update();
    });

    this->m_point1 = new dtkThreeDeeVector3([this]() {
        this->m_object->SetPoint1(this->m_point1->getValues().data());
        this->update();
    });

    this->m_point2 = new dtkThreeDeeVector3([this]() {
        this->m_object->SetPoint2(this->m_point2->getValues().data());
        this->update();
    });

    this->m_center = new dtkThreeDeeVector3([this]() {
        this->m_object->SetCenter(this->m_center->getValues().data());
    });

    this->m_polarVector = new dtkThreeDeeVector3([this]() {
        this->m_object->SetPolarVector(this->m_polarVector->getValues().data());
    });
}

auto dtkThreeDeeArcSource::getPoint1() -> dtkThreeDeeVector3* {
    return this->m_point1;
}

auto dtkThreeDeeArcSource::getPoint2() -> dtkThreeDeeVector3* {
    return this->m_point2;
}

auto dtkThreeDeeArcSource::getCenter() -> dtkThreeDeeVector3* {
    return this->m_center;
}

auto dtkThreeDeeArcSource::getPolarVector() -> dtkThreeDeeVector3* {
    return this->m_polarVector;
}

auto dtkThreeDeeArcSource::setAngle(qreal angle) -> void {
    this->m_object->SetAngle(angle);
    emit this->angleChanged();
    this->update();
}

auto dtkThreeDeeArcSource::getAngle() -> qreal {
    return this->m_object->GetAngle();
}

auto dtkThreeDeeArcSource::setResolution(int resolution) -> void {
    this->m_object->SetResolution(resolution);
    emit this->resolutionChanged();
    this->update();
}

auto dtkThreeDeeArcSource::getResolution() -> int {
    return this->m_object->GetResolution();
}

auto dtkThreeDeeArcSource::setNegative(bool negative) -> void {
    this->m_object->SetNegative(negative);
    emit this->negativeChanged();
    this->update();
}

auto dtkThreeDeeArcSource::getNegative() -> bool {
    return this->m_object->GetNegative();
}

auto dtkThreeDeeArcSource::setUseNormalAndAngle(bool useNormalAndAngle) -> void {
    this->m_object->SetUseNormalAndAngle(useNormalAndAngle);
    emit this->useNormalAndAngleChanged();
    this->update();
}

auto dtkThreeDeeArcSource::getUseNormalAndAngle() -> bool {
    return this->m_object->GetUseNormalAndAngle();
}

//
// dtkThreeDee+ArcSource.cpp ends here
