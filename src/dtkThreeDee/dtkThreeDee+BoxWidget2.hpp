// dtkThreeDee+BoxWidget2.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+AbstractWidget.hpp"

#include <QQmlEngine>

#include <vtkBoxWidget2.h>

class dtkThreeDeeBoxWidget2 : public dtkThreeDeeAbstractWidget
{
    Q_OBJECT

    QML_NAMED_ELEMENT(BoxWidget2)

public:
    dtkThreeDeeBoxWidget2(void);

private:
    vtkSmartPointer<vtkAbstractWidget> m_object = nullptr;
};

//
// dtkThreeDee+BoxWidget2.hpp ends here
