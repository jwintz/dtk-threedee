// dtkThreeDee+DensifyPolyData.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+DensifyPolyData.hpp"

dtkThreeDeeDensifyPolyData::dtkThreeDeeDensifyPolyData() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkDensifyPolyData>::New()) {
    this->m_object = vtkDensifyPolyData::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeDensifyPolyData::setNumberOfSubdivisions(int numberOfSubdivisions) -> void {
    this->m_object->SetNumberOfSubdivisions(numberOfSubdivisions);
    this->update();
    emit this->numberOfSubdivisionsChanged();
}

auto dtkThreeDeeDensifyPolyData::getNumberOfSubdivisions() -> int {
    return this->m_object->GetNumberOfSubdivisions();
}

//
// dtkThreeDee+DensifyPolyData.cpp ends here
