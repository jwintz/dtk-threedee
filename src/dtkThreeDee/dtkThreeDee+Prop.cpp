// dtkThreeDee+Prop.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Prop.hpp"
#include "dtkThreeDee+Viewer.hpp"

dtkThreeDeeProp::dtkThreeDeeProp(vtkSmartPointer<vtkProp> vtkObject) : dtkThreeDeeObject(dtkThreeDeeObject::Type::Prop), m_object(vtkObject)
{

}

dtkThreeDeeProp::~dtkThreeDeeProp(void)
{
    this->m_initialized = false;
    this->m_object = nullptr;
}

vtkSmartPointer<vtkProp> dtkThreeDeeProp::get(void)
{
    return this->m_object;
}

void dtkThreeDeeProp::linkViewer(dtkThreeDeeViewer* viewer)
{
    this->m_viewers.append(viewer);
    this->m_initialized = true;

    this->update();
}

void dtkThreeDeeProp::unlinkViewer(dtkThreeDeeViewer* viewer)
{
    this->m_viewers.removeOne(viewer);
}

void dtkThreeDeeProp::update(void)
{
    if (!this->m_initialized)
        return;

    for (auto v : this->m_viewers)
        v->update();
}

void dtkThreeDeeProp::setVisibility(bool visible)
{
    this->m_object->SetVisibility(visible);
    emit this->visibilityChanged();
    update();
}

bool dtkThreeDeeProp::getVisibility(void)
{
    return this->m_object->GetVisibility();
}

//
// dtkThreeDee+Prop.cpp ends here
