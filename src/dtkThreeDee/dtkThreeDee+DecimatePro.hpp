// dtkThreeDee+DecimatePro.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkDecimatePro.h>

class dtkThreeDeeDecimatePro : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(qreal splitAngle READ getSplitAngle WRITE setSplitAngle NOTIFY splitAngleChanged);
    Q_PROPERTY(qreal featureAngle READ getFeatureAngle WRITE setFeatureAngle NOTIFY featureAngleChanged);
    Q_PROPERTY(qreal targetReduction READ getTargetReduction WRITE setTargetReduction NOTIFY targetReductionChanged);
    Q_PROPERTY(bool preserveTopology READ getPreserveTopology WRITE setPreserveTopology NOTIFY preserveTopologyChanged);
    Q_PROPERTY(bool splitting READ getSplitting WRITE setSplitting NOTIFY splittingChanged);
    Q_PROPERTY(bool preSplitMesh READ getPreSplitMesh WRITE setPreSplitMesh NOTIFY preSplitMeshChanged);
    Q_PROPERTY(int degree READ getDegree WRITE setDegree NOTIFY degreeChanged);

    QML_NAMED_ELEMENT(DecimatePro)

private:
    vtkSmartPointer<vtkDecimatePro> m_object = nullptr;

public:
    dtkThreeDeeDecimatePro();
    auto setDegree(int) -> void;
    auto getDegree() -> int;
    auto setSplitting(bool) -> void;
    auto getSplitting() -> bool;
    auto setSplitAngle(qreal) -> void;
    auto getSplitAngle() -> qreal;
    auto setFeatureAngle(qreal) -> void;
    auto getFeatureAngle() -> qreal;
    auto setPreSplitMesh(bool) -> void;
    auto getPreSplitMesh() -> bool;
    auto setTargetReduction(qreal) -> void;
    auto getTargetReduction() -> qreal;
    auto setPreserveTopology(bool) -> void;
    auto getPreserveTopology() ->bool;

signals:
    void degreeChanged();
    void splittingChanged();
    void splitAngleChanged();
    void preSplitMeshChanged();
    void featureAngleChanged();
    void targetReductionChanged();
    void preserveTopologyChanged();
};

//
// dtkThreeDee+DecimatePro.hpp ends here
