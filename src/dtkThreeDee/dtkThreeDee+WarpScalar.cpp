// dtkThreeDee+WarpScalar.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+WarpScalar.hpp"

dtkThreeDeeWarpScalar::dtkThreeDeeWarpScalar() : dtkThreeDeePointSetAlgorithm(vtkSmartPointer<vtkWarpScalar>::New()) {
    this->m_object = vtkWarpScalar::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeWarpScalar::setScaleFactor(qreal scaleFactor) -> void {
    this->m_object->SetScaleFactor(scaleFactor);
    emit this->scaleFactorChanged();
    this->update();
}

auto dtkThreeDeeWarpScalar::getScaleFactor() -> qreal {
    return this->m_object->GetScaleFactor();
}

auto dtkThreeDeeWarpScalar::setUseNormal(bool useNormal) -> void {
    this->m_object->SetUseNormal(useNormal);
    emit this->useNormalChanged();
    this->update();
}

auto dtkThreeDeeWarpScalar::getUseNormal() -> bool {
    return this->m_object->GetUseNormal();
}

//
// dtkThreeDee+WarpScalar.cpp ends here
