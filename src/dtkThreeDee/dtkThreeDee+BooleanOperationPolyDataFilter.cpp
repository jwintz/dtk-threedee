// dtkThreeDee+BooleanOperationPolyDataFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+BooleanOperationPolyDataFilter.hpp"

dtkThreeDeeBooleanOperationPolyDataFilter::dtkThreeDeeBooleanOperationPolyDataFilter() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New()) {
    this->m_object = vtkBooleanOperationPolyDataFilter::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeBooleanOperationPolyDataFilter::setOperation(Operation operation) -> void {
    this->m_object->SetOperation(operation);
    emit this->operationChanged();
    this->update();
}

auto dtkThreeDeeBooleanOperationPolyDataFilter::getOperation() -> Operation {
    return static_cast<Operation>(this->m_object->GetOperation());
}

auto dtkThreeDeeBooleanOperationPolyDataFilter::setTolerance(qreal tolerance) -> void {
    this->m_object->SetTolerance(tolerance);
    emit this->toleranceChanged();
    this->update();
}

auto dtkThreeDeeBooleanOperationPolyDataFilter::getTolerance() -> qreal {
    return this->m_object->GetTolerance();
}

auto dtkThreeDeeBooleanOperationPolyDataFilter::setReorientDifferentCells(bool reorientDifferentCells) -> void {
    this->m_object->SetReorientDifferenceCells(reorientDifferentCells);
    emit this->reorientDifferentCellsChanged();
    this->update();
}

auto dtkThreeDeeBooleanOperationPolyDataFilter::getReorientDifferentCells() -> bool {
    return this->m_object->GetReorientDifferenceCells();
}

//
// dtkThreeDee+BooleanOperationPolyDataFilter.cpp ends here
