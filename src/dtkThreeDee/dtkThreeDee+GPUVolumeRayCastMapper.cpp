// dtkThreeDee+GPUVolumeRayCastMapper.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+GPUVolumeRayCastMapper.hpp"

dtkThreeDeeGPUVolumeRayCastMapper::dtkThreeDeeGPUVolumeRayCastMapper() : dtkThreeDeeVolumeMapper(vtkSmartPointer<vtkGPUVolumeRayCastMapper>::New()) {
    this->m_object = vtkGPUVolumeRayCastMapper::SafeDownCast(this->get());
}

auto dtkThreeDeeGPUVolumeRayCastMapper::setAutoAdjustSampleDistances(bool autoAdjustSampleDistances) -> void {
    this->m_object->SetAutoAdjustSampleDistances(autoAdjustSampleDistances);
    emit this->autoAdjustSampleDistancesChanged();
    this->update();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::getAutoAdjustSampleDistances() -> bool {
    return this->m_object->GetAutoAdjustSampleDistances();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::setUseJittering(bool useJittering) -> void {
    this->m_object->SetUseJittering(useJittering);
    emit this->useJitteringChanged();
    this->update();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::getUseJittering() -> bool {
    return this->m_object->GetUseJittering();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::setUseDepthPass(bool useDepthPass) -> void {
    this->m_object->SetUseDepthPass(useDepthPass);
    emit this->useDepthPassChanged();
    this->update();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::getUseDepthPass() -> bool {
    return this->m_object->GetUseDepthPass();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::setSampleDistance(qreal sampleDistance) -> void {
    this->m_object->SetSampleDistance(sampleDistance);
    emit this->sampleDistanceChanged();
    this->update();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::getSampleDistance() -> qreal {
    return this->m_object->GetSampleDistance();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::setImageSampleDistance(qreal imageSampleDistance) -> void {
    this->m_object->SetImageSampleDistance(imageSampleDistance);
    emit this->imageSampleDistanceChanged();
    this->update();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::getImageSampleDistance() -> qreal {
    return this->m_object->GetImageSampleDistance();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::setMinimumImageSampleDistance(qreal minimumImageSampleDistance) -> void {
    this->m_object->SetMinimumImageSampleDistance(minimumImageSampleDistance);
    emit this->minimumImageSampleDistanceChanged();
    this->update();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::getMinimumImageSampleDistance() -> qreal {
    return this->m_object->GetMinimumImageSampleDistance();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::setMaximumImageSampleDistance(qreal maximumImageSampleDistance) -> void {
    this->m_object->SetMaximumImageSampleDistance(maximumImageSampleDistance);
    emit this->maximumImageSampleDistanceChanged();
    this->update();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::getMaximumImageSampleDistance() -> qreal {
    return this->m_object->GetMaximumImageSampleDistance();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::setFinalColorWindow(qreal finalColorWindow) -> void {
    this->m_object->SetFinalColorWindow(finalColorWindow);
    emit this->finalColorWindowChanged();
    this->update();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::getFinalColorWindow() -> qreal {
    return this->m_object->GetFinalColorWindow();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::setFinalColorLevel(qreal finalColorLevel) -> void {
    this->m_object->SetFinalColorLevel(finalColorLevel);
    emit this->finalColorLevelChanged();
    this->update();
}

auto dtkThreeDeeGPUVolumeRayCastMapper::getFinalColorLevel() -> qreal {
    return this->m_object->GetFinalColorLevel();
}

//
// dtkThreeDee+GPUVolumeRayCastMapper.cpp ends here
