// dtkThreeDee+AbstractPolyDataReader.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+AbstractPolyDataReader.hpp"

#include <QtCore>

dtkThreeDeeAbstractPolyDataReader::dtkThreeDeeAbstractPolyDataReader(vtkSmartPointer<vtkAbstractPolyDataReader> vtkObject) : dtkThreeDeePolyDataAlgorithm(vtkObject) {
    this->m_vtkObject = vtkObject;
}

auto dtkThreeDeeAbstractPolyDataReader::isValid() -> bool {
    if (!QFileInfo::exists(this->m_fileName)) {
        return false;
    }

    return true;
}

auto dtkThreeDeeAbstractPolyDataReader::setFileName(const QString& fileName) -> void {
    this->m_fileName = fileName;

    emit this->fileNameChanged();

    if (QFileInfo::exists(fileName)) {
        this->m_vtkObject->SetFileName(fileName.toStdString().c_str());
        this->m_vtkObject->Update();
        this->update();
    }
}

auto dtkThreeDeeAbstractPolyDataReader::getFileName() -> QString {
    return this->m_fileName;
}

//
// dtkThreeDee+AbstractPolyDataReader.cpp ends here
