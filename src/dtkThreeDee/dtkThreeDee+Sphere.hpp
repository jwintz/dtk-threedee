// dtkThreeDee+Sphere.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Vector3.hpp"
#include "dtkThreeDee+ImplicitFunction.hpp"

#include <QQmlEngine>

#include <vtkSphere.h>

class dtkThreeDeeSphere : public dtkThreeDeeImplicitFunction
{
    Q_OBJECT

    Q_PROPERTY(qreal radius READ getRadius WRITE setRadius NOTIFY radiusChanged);
    Q_PROPERTY(dtkThreeDeeVector3* center READ getCenter CONSTANT);

    QML_NAMED_ELEMENT(Sphere)

private:
    vtkSmartPointer<vtkSphere> m_object = nullptr;
    dtkThreeDeeVector3* m_center = nullptr;

public:
    dtkThreeDeeSphere();
    auto setRadius(qreal) -> void;
    auto getRadius() -> qreal;
    auto setCenter(dtkThreeDeeVector3*) -> void;
    auto getCenter() -> dtkThreeDeeVector3*;

public slots:
    qreal evaluateFunction(qreal, qreal, qreal);

signals:
    void radiusChanged();
};

//
// dtkThreeDee+Sphere.hpp ends here
