// dtkThreeDee+ConeSource.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"
#include "dtkThreeDee+Vector3.hpp"

#include <QQmlEngine>

#include <vtkConeSource.h>

class dtkThreeDeeConeSource : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(dtkThreeDeeVector3 *center READ getCenter CONSTANT);
    Q_PROPERTY(dtkThreeDeeVector3 *direction READ getDirection CONSTANT);
    Q_PROPERTY(qreal height READ getHeight WRITE setHeight NOTIFY heightChanged);
    Q_PROPERTY(qreal radius READ getRadius WRITE setRadius NOTIFY radiusChanged);
    Q_PROPERTY(qreal angle READ getAngle WRITE setAngle NOTIFY angleChanged);
    Q_PROPERTY(int resolution READ getResolution WRITE setResolution NOTIFY resolutionChanged)
    Q_PROPERTY(bool capping READ getCapping WRITE setCapping NOTIFY cappingChanged);

    QML_NAMED_ELEMENT(ConeSource)

public:
    dtkThreeDeeConeSource();
    auto getCenter() -> dtkThreeDeeVector3 *;
    auto getDirection() -> dtkThreeDeeVector3 *;
    auto setHeight(qreal) -> void;
    auto getHeight() -> qreal;
    auto setRadius(qreal) -> void;
    auto getRadius() -> qreal;
    auto setAngle(qreal) -> void;
    auto getAngle() -> qreal;
    auto setResolution(int) -> void;
    auto getResolution() -> int;
    auto setCapping(bool) -> void;
    auto getCapping() -> bool;

signals:
    void heightChanged();
    void angleChanged();
    void radiusChanged();
    void resolutionChanged();
    void cappingChanged();

private:
    vtkSmartPointer<vtkConeSource> m_object = nullptr;
    dtkThreeDeeVector3 *m_center = nullptr;
    dtkThreeDeeVector3 *m_direction = nullptr;
};

//
// dtkThreeDee+ConeSource.hpp ends here
