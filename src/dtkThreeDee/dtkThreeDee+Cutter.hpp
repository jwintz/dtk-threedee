// dtkThreeDee+Cutter.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+ImplicitFunction.hpp"
#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkCutter.h>

class dtkThreeDeeImplicitFunction;

class dtkThreeDeeCutter : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(dtkThreeDeeImplicitFunction* cutFunction READ getCutFunction WRITE setCutFunction NOTIFY cutFunctionChanged);

    QML_NAMED_ELEMENT(Cutter)

private:
    dtkThreeDeeImplicitFunction::cb_t m_cutFunctionCb;
    dtkThreeDeeImplicitFunction* m_cutFunction = nullptr;
    vtkSmartPointer<vtkCutter> m_object = nullptr;

public:
     dtkThreeDeeCutter();
    ~dtkThreeDeeCutter();

    auto updateCutFunction() -> void;
    auto setCutFunction(dtkThreeDeeImplicitFunction*) -> void;
    auto getCutFunction() -> dtkThreeDeeImplicitFunction*;

signals:
    void cutFunctionChanged();
};

//
// dtkThreeDee+Cutter.hpp ends here
