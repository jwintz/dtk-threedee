// dtkThreeDee+AbstractMapper3D.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+AbstractMapper.hpp"

#include <QQmlEngine>

#include <vtkAbstractMapper3D.h>

class dtkThreeDeeAbstractMapper3D : public dtkThreeDeeAbstractMapper
{
    Q_OBJECT
    QML_NAMED_ELEMENT(AbstractMapper3D)
    QML_UNCREATABLE("")

public:
    dtkThreeDeeAbstractMapper3D(vtkSmartPointer<vtkAbstractMapper3D>);
};

//
// dtkThreeDee+AbstractMapper3D.hpp ends here
