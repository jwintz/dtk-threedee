// dtkThreeDee+ImageReader2.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QQmlEngine>

#include <vtkImageReader2.h>

#include "dtkThreeDee+ImageAlgorithm.hpp"

class dtkThreeDeeImageReader2 : public dtkThreeDeeImageAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(QString fileName READ getFileName WRITE setFileName NOTIFY fileNameChanged);

    QML_NAMED_ELEMENT(ImageReader2)
    QML_UNCREATABLE("")

private:
    vtkSmartPointer<vtkImageReader2> m_object = nullptr;
    QString m_fileName;

public:
    dtkThreeDeeImageReader2(vtkSmartPointer<vtkImageReader2>);
    auto setFileName(const QString&) -> void;
    auto getFileName() -> QString;
    virtual auto isValid() -> bool;

signals:
    void fileNameChanged();
};

//
// dtkThreeDee+ImageReader2.hpp ends here
