// dtkThreeDee+STLReader.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+STLReader.hpp"

dtkThreeDeeSTLReader::dtkThreeDeeSTLReader() : dtkThreeDeeAbstractPolyDataReader(vtkSmartPointer<vtkSTLReader>::New())
{

}

//
// dtkThreeDee+STLReader.cpp ends here
