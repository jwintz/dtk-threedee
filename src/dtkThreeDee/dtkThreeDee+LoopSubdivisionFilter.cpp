// dtkThreeDee+LoopSubdivisionFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+LoopSubdivisionFilter.hpp"

dtkThreeDeeLoopSubdivisionFilter::dtkThreeDeeLoopSubdivisionFilter() : dtkThreeDeeApproximatingSubdivisionFilter(vtkSmartPointer<vtkLoopSubdivisionFilter>::New())
{

}

//
// dtkThreeDee+LoopSubdivisionFilter.cpp ends here
