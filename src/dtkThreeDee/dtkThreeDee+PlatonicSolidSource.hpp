// dtkThreeDee+PlatonicSolidSource.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkPlatonicSolidSource.h>

class dtkThreeDeePlatonicSolidSource : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(PlatonicSolidSource)

public:
    enum SolidType {
        SolidTetrahedron = VTK_SOLID_TETRAHEDRON,
        SolidCube = VTK_SOLID_CUBE,
        SolidOctahedron = VTK_SOLID_OCTAHEDRON,
        SolidIcosahedron = VTK_SOLID_ICOSAHEDRON,
        SolidDodecahedron = VTK_SOLID_DODECAHEDRON
    };

private:
    Q_ENUM(SolidType);
    Q_PROPERTY(SolidType solidType READ getSolidType WRITE setSolidType NOTIFY solidTypeChanged);

private:
    vtkSmartPointer<vtkPlatonicSolidSource> m_object = nullptr;

public:
    dtkThreeDeePlatonicSolidSource();
    auto setSolidType(SolidType) -> void;
    auto getSolidType() -> SolidType;

signals:
    void solidTypeChanged();
};

//
// dtkThreeDee+PlatonicSolidSource.hpp ends here
