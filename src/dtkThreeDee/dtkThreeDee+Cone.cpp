// dtkThreeDee+Cone.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Cone.hpp"

dtkThreeDeeCone::dtkThreeDeeCone() : dtkThreeDeeImplicitFunction(vtkSmartPointer<vtkCone>::New()) {
    this->m_object = vtkCone::SafeDownCast(dtkThreeDeeImplicitFunction::get());
}

auto dtkThreeDeeCone::setAngle(qreal angle) -> void {
    this->m_object->SetAngle(angle);
    emit this->angleChanged();
}

auto dtkThreeDeeCone::getAngle() -> qreal {
    return this->m_object->GetAngle();
}

//
// dtkThreeDee+Cone.cpp ends here
