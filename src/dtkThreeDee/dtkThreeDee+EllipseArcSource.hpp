// dtkThreeDee+EllipseArcSource.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Vector3.hpp"
#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkEllipseArcSource.h>

class dtkThreeDeeEllipseArcSource : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(dtkThreeDeeVector3* center READ getCenter CONSTANT);
    Q_PROPERTY(dtkThreeDeeVector3* normal READ getNormal CONSTANT);
    Q_PROPERTY(dtkThreeDeeVector3* majorRadiusVector READ getMajorRadiusVector CONSTANT);
    Q_PROPERTY(qreal startAngle READ getStartAngle WRITE setStartAngle NOTIFY startAngleChanged);
    Q_PROPERTY(qreal segmentAngle READ getSegmentAngle WRITE setSegmentAngle NOTIFY segmentAngleChanged);
    Q_PROPERTY(int resolution READ getResolution WRITE setResolution NOTIFY resolutionChanged);
    Q_PROPERTY(qreal ratio READ getRatio WRITE setRatio NOTIFY ratioChanged);

    QML_NAMED_ELEMENT(EllipseArcSource)

public:
    dtkThreeDeeEllipseArcSource();
    auto getCenter() -> dtkThreeDeeVector3*;
    auto getNormal() -> dtkThreeDeeVector3*;
    auto getMajorRadiusVector() -> dtkThreeDeeVector3*;
    auto setStartAngle(qreal) -> void;
    auto getStartAngle() -> qreal;
    auto setSegmentAngle(qreal) -> void;
    auto getSegmentAngle() -> qreal;
    auto setResolution(int) -> void;
    auto getResolution() -> int;
    auto setRatio(qreal) -> void;
    auto getRatio() -> qreal;

signals:
    void startAngleChanged();
    void segmentAngleChanged();
    void resolutionChanged();
    void ratioChanged();

private:
    vtkSmartPointer<vtkEllipseArcSource> m_object = nullptr;
    dtkThreeDeeVector3* m_center = nullptr;
    dtkThreeDeeVector3* m_normal = nullptr;
    dtkThreeDeeVector3* m_majorRadiusVector = nullptr;
};

//
// dtkThreeDee+EllipseArcSource.hpp ends here
