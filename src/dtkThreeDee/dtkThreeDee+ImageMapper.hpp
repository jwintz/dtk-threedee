// dtkThreeDee+ImageMapper.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Mapper2D.hpp"

#include <QQmlEngine>

#include <vtkImageMapper.h>

class dtkThreeDeeImageMapper : public dtkThreeDeeMapper2D
{
    Q_OBJECT

    Q_PROPERTY(qreal colorWindow READ getColorWindow WRITE setColorWindow NOTIFY colorWindowChanged);
    Q_PROPERTY(qreal colorLevel READ getColorLevel WRITE setColorLevel NOTIFY colorLevelChanged);
    Q_PROPERTY(qreal colorShift READ getColorShift NOTIFY colorShiftChanged);
    Q_PROPERTY(qreal colorScale READ getColorScale NOTIFY colorScaleChanged);

    QML_NAMED_ELEMENT(ImageMapper)

private:
    vtkSmartPointer<vtkImageMapper> m_object = nullptr;

public:
    dtkThreeDeeImageMapper();
    auto setColorWindow(qreal) -> void;
    auto getColorWindow() -> qreal;
    auto setColorLevel(qreal) -> void;
    auto getColorLevel() -> qreal;
    auto getColorShift() -> qreal;
    auto getColorScale() -> qreal;

signals:
    void colorWindowChanged();
    void colorLevelChanged();
    void colorShiftChanged();
    void colorScaleChanged();
};

//
// dtkThreeDee+ImageMapper.hpp ends here
