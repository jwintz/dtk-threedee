// dtkThreeDee+PointSource.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+PointSource.hpp"

dtkThreeDeePointSource::dtkThreeDeePointSource() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkPointSource>::New()) {
    this->m_object = vtkPointSource::SafeDownCast(dtkThreeDeeAlgorithm::get());

    this->m_center = new dtkThreeDeeVector3([this](){
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->resetSeed();
        this->update();
    });
}

auto dtkThreeDeePointSource::setDistribution(Distribution distribution) -> void {
    this->m_object->SetDistribution(distribution);
    emit this->distributionChanged();
    this->resetSeed();
    this->update();
}

auto dtkThreeDeePointSource::getDistribution() -> Distribution {
    return static_cast<Distribution>(this->m_object->GetDistribution());
}

auto dtkThreeDeePointSource::setNumberOfPoints(int numberOfPoints) -> void {
    this->m_object->SetNumberOfPoints(numberOfPoints);
    emit this->numberOfPointsChanged();
    this->resetSeed();
    this->update();
}

auto dtkThreeDeePointSource::getNumberOfPoints() -> int {
    return this->m_object->GetNumberOfPoints();
}

auto dtkThreeDeePointSource::setRadius(qreal radius) -> void {
    this->m_object->SetRadius(radius);
    emit this->radiusChanged();
    this->resetSeed();
    this->update();
}

auto dtkThreeDeePointSource::getRadius() -> qreal {
    return this->m_object->GetRadius();
}

auto dtkThreeDeePointSource::getCenter() -> dtkThreeDeeVector3* {
    return this->m_center;
}

auto dtkThreeDeePointSource::setRandomSequence(dtkThreeDeeRandomSequence* randomSequence) -> void {
    this->m_randomSequence = randomSequence;

    if (randomSequence == nullptr) {
        this->m_object->SetRandomSequence(nullptr);
    } else {
        this->m_object->SetRandomSequence(randomSequence->get());
        randomSequence->initialize();
    }

    emit this->randomSequenceChanged();
    this->update();
}

auto dtkThreeDeePointSource::getRandomSequence() -> dtkThreeDeeRandomSequence* {
    return this->m_randomSequence;
}

auto dtkThreeDeePointSource::resetSeed() -> void {
    if (this->m_randomSequence != nullptr) {
        this->m_randomSequence->initialize();
    }
}

//
// dtkThreeDee+PointSource.cpp ends here
