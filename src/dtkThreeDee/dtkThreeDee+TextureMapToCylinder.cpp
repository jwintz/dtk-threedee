// dtkThreeDee+TextureMapToCylinder.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+TextureMapToCylinder.hpp"

dtkThreeDeeTextureMapToCylinder::dtkThreeDeeTextureMapToCylinder() : dtkThreeDeeDataSetAlgorithm(vtkSmartPointer<vtkTextureMapToCylinder>::New())
{
    this->m_object = vtkTextureMapToCylinder::SafeDownCast(this->get());
}

//
// dtkThreeDee+TextureMapToCylinder.cpp ends here
