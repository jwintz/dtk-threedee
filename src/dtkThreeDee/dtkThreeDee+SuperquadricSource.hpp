// dtkThreeDee+SuperquadricSource.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Vector3.hpp"
#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkSuperquadricSource.h>

class dtkThreeDeeSuperquadricSource : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(SuperquadricSource)

public:
    enum Axis {
        X,
        Y,
        Z
    };

private:
    Q_ENUM(Axis);
    Q_PROPERTY(Axis axisOfSymmetry READ getAxisOfSymmetry WRITE setAxisOfSymmetry NOTIFY axisOfSymmetryChanged);
    Q_PROPERTY(bool toroidal READ getToroidal WRITE setToroidal NOTIFY toroidalChanged);
    Q_PROPERTY(dtkThreeDeeVector3* center READ getCenter CONSTANT);
    Q_PROPERTY(dtkThreeDeeVector3* sccale READ getScale CONSTANT);
    Q_PROPERTY(qreal thickness READ getThickness WRITE setThickness NOTIFY thicknessChanged);
    Q_PROPERTY(qreal size READ getSize WRITE setSize NOTIFY sizeChanged);
    Q_PROPERTY(qreal phiRoundness READ getPhiRoundness WRITE setPhiRoundness NOTIFY phiRoundnessChanged);
    Q_PROPERTY(qreal thetaRoundness READ getThetaRoundness WRITE setThetaRoundness NOTIFY thetaRoundnessChanged);
    Q_PROPERTY(qreal phiResolution READ getPhiResolution WRITE setPhiResolution NOTIFY phiResolutionChanged);
    Q_PROPERTY(qreal thetaResolution READ getThetaResolution WRITE setThetaResolution NOTIFY thetaResolutionChanged);

private:
    vtkSmartPointer<vtkSuperquadricSource> m_object = nullptr;
    dtkThreeDeeVector3* m_center = nullptr;
    dtkThreeDeeVector3* m_scale = nullptr;
    Axis m_axisOfSymmetry;

public:
    dtkThreeDeeSuperquadricSource();
    auto setToroidal(bool) -> void;
    auto getToroidal() -> bool;
    auto setThickness(qreal) -> void;
    auto getThickness() -> qreal;
    auto setSize(qreal) -> void;
    auto getSize() -> qreal;
    auto setPhiRoundness(qreal) -> void;
    auto getPhiRoundness() -> qreal;
    auto setThetaRoundness(qreal) -> void;
    auto getThetaRoundness() -> qreal;
    auto setPhiResolution(qreal) -> void;
    auto getPhiResolution() -> qreal;
    auto setThetaResolution(qreal) -> void;
    auto getThetaResolution() -> qreal;
    auto setAxisOfSymmetry(Axis) -> void;
    auto getAxisOfSymmetry() -> Axis;
    auto getCenter() -> dtkThreeDeeVector3*;
    auto getScale() -> dtkThreeDeeVector3*;

signals:
    void toroidalChanged();
    void thicknessChanged();
    void sizeChanged();
    void phiRoundnessChanged();
    void thetaRoundnessChanged();
    void phiResolutionChanged();
    void thetaResolutionChanged();
    void axisOfSymmetryChanged();
};

//
// dtkThreeDee+SuperquadricSource.hpp ends here
