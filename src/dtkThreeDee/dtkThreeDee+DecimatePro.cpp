// dtkThreeDee+DecimatePro.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+DecimatePro.hpp"

dtkThreeDeeDecimatePro::dtkThreeDeeDecimatePro() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkDecimatePro>::New()) {
    this->m_object = vtkDecimatePro::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeDecimatePro::setDegree(int val) -> void {
    this->m_object->SetDegree(val);
    emit this->degreeChanged();
    this->update();
}

auto dtkThreeDeeDecimatePro::getDegree() -> int {
    return this->m_object->GetDegree();
}

auto dtkThreeDeeDecimatePro::setSplitting(bool val) -> void {
    this->m_object->SetSplitting(val);
    emit this->splittingChanged();
    this->update();
}

auto dtkThreeDeeDecimatePro::getSplitting() -> bool {
    return this->m_object->GetSplitting();
}

auto dtkThreeDeeDecimatePro::setPreSplitMesh(bool val) -> void {
    this->m_object->SetPreSplitMesh(val);
    emit this->preSplitMeshChanged();
    this->update();
}

auto dtkThreeDeeDecimatePro::getPreSplitMesh() -> bool {
    return this->m_object->GetPreSplitMesh();
}

auto dtkThreeDeeDecimatePro::setSplitAngle(qreal val) -> void {
    this->m_object->SetSplitAngle(val);
    emit this->splittingChanged();
    this->update();
}

auto dtkThreeDeeDecimatePro::getSplitAngle() -> qreal {
    return this->m_object->GetSplitAngle();
}

auto dtkThreeDeeDecimatePro::setFeatureAngle(qreal val) -> void {
    this->m_object->SetFeatureAngle(val);
    emit this->featureAngleChanged();
    this->update();
}

auto dtkThreeDeeDecimatePro::getFeatureAngle() -> qreal {
    return this->m_object->GetFeatureAngle();
}

auto dtkThreeDeeDecimatePro::setPreserveTopology(bool val) -> void {
    this->m_object->SetPreserveTopology(val);
    emit this->preserveTopologyChanged();
    this->update();
}

auto dtkThreeDeeDecimatePro::getPreserveTopology() -> bool {
    return this->m_object->GetPreserveTopology();
}

auto dtkThreeDeeDecimatePro::setTargetReduction(qreal val) -> void {
    this->m_object->SetTargetReduction(val);
    emit this->targetReductionChanged();
    this->update();
}

auto dtkThreeDeeDecimatePro::getTargetReduction() -> qreal {
    return this->m_object->GetTargetReduction();
}

//
// dtkThreeDee+DecimatePro.cpp ends here
