// dtkThreeDee+LinearSubdivisionFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+LinearSubdivisionFilter.hpp"

dtkThreeDeeLinearSubdivisionFilter::dtkThreeDeeLinearSubdivisionFilter() : dtkThreeDeeInterpolatingSubdivisionFilter(vtkSmartPointer<vtkLinearSubdivisionFilter>::New())
{

}

//
// dtkThreeDee+LinearSubdivisionFilter.cpp ends here
