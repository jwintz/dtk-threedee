// dtkThreeDee+ExtractSurface.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkExtractSurface.h>

class dtkThreeDeeExtractSurface : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(qreal radius READ getRadius WRITE setRadius NOTIFY radiusChanged);
    Q_PROPERTY(bool holeFilling READ getHoleFilling WRITE setHoleFilling NOTIFY holeFillingChanged);
    Q_PROPERTY(bool computeNormals READ getComputeNormals WRITE setComputeNormals NOTIFY computeNormalsChanged);
    Q_PROPERTY(bool computeGradients READ getComputeGradients WRITE setComputeGradients NOTIFY computeGradientsChanged);

    QML_NAMED_ELEMENT(ExtractSurface)

public:
    dtkThreeDeeExtractSurface();
    auto setRadius(qreal) -> void;
    auto getRadius() -> qreal;
    auto setHoleFilling(bool) -> void;
    auto getHoleFilling() -> bool;
    auto setComputeNormals(bool) -> void;
    auto getComputeNormals() -> bool;
    auto setComputeGradients(bool) -> void;
    auto getComputeGradients() -> bool;

signals:
    void radiusChanged();
    void holeFillingChanged();
    void computeNormalsChanged();
    void computeGradientsChanged();

private:
    vtkSmartPointer<vtkExtractSurface> m_object = nullptr;
};

//
// dtkThreeDee+ExtractSurface.hpp ends here
