#pragma once

#include <QImage>

#include <QQuickFramebufferObject>

#include <vtkSmartPointer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkImageData.h>
#include <vtkSmartPointer.h>
#include <vtkWindowToImageFilter.h>

#include <QMouseEvent>

class dtkThreeDeeFboOffscreenWindow;

class dtkThreeDeeFboRenderer : public QQuickFramebufferObject::Renderer {
    friend class dtkThreeDeeFboOffscreenWindow;
private:
    dtkThreeDeeFboOffscreenWindow* m_fboOffscreenWindow = nullptr;
    QOpenGLFramebufferObject* m_fbo = nullptr;
public:
    vtkSmartPointer<vtkRenderWindowInteractor> m_interactor = nullptr;
    vtkSmartPointer<vtkInteractorStyleTrackballCamera> m_interactorStyle;
public:
    dtkThreeDeeFboRenderer(dtkThreeDeeFboOffscreenWindow*);
    auto synchronize(QQuickFramebufferObject*) -> void override;
    auto render() -> void override;
    auto onMouseMoveEvent(QMouseEvent*) -> void;
    auto onMouseEvent(QMouseEvent*) -> void;
    auto onWheelEvent(QWheelEvent*) -> void;
    auto createFramebufferObject(const QSize&) -> QOpenGLFramebufferObject* override;
    ~dtkThreeDeeFboRenderer();
};
