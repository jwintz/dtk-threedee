#include "dtkThreeDee+Win32Interactor.hpp"

#if defined(Q_OS_WIN32)

#include <vtkWin32OpenGLRenderWindow.h>
#include <vtkObjectFactory.h>

vtkStandardNewMacro(Win32Interactor);

void dtkThreeDeeWin32Interactor::Initialize(void)
{
    vtkWin32OpenGLRenderWindow *ren;

    int *size;

    if (!this->RenderWindow) {
        vtkErrorMacro(<< "No renderer defined!");
        return;
    }

    if (this->Initialized)
        return;

    this->Initialized = 1;

    ren = (vtkWin32OpenGLRenderWindow *)(this->RenderWindow);
    ren->Start();

    size = ren->GetSize();
    ren->GetPosition();

    this->WindowId = 0;
    this->Enable();
    this->Size[0] = size[0];
    this->Size[1] = size[1];
}

#endif
