// dtkThreeDee+LinearExtrusionFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+LinearExtrusionFilter.hpp"

dtkThreeDeeLinearExtrusionFilter::dtkThreeDeeLinearExtrusionFilter() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkLinearExtrusionFilter>::New()) {
    this->m_object = vtkLinearExtrusionFilter::SafeDownCast(dtkThreeDeeAlgorithm::get());

    this->m_vector = new dtkThreeDeeVector3([this](){
        this->m_object->SetVector(this->m_vector->getValues().data());
        this->update();
    });

    this->m_extrusionPoint = new dtkThreeDeeVector3([this](){
        this->m_object->SetExtrusionPoint(this->m_extrusionPoint->getValues().data());
        this->update();
    });
}

auto dtkThreeDeeLinearExtrusionFilter::setExtrusionType(ExtrusionType extrusionType) -> void {
    this->m_object->SetExtrusionType(extrusionType);
    emit this->extrusionTypeChanged();
    this->update();
}

auto dtkThreeDeeLinearExtrusionFilter::getExtrusionType() -> ExtrusionType {
    return static_cast<ExtrusionType>(this->m_object->GetExtrusionType());
}

auto dtkThreeDeeLinearExtrusionFilter::setScaleFactor(qreal scaleFactor) -> void {
    this->m_object->SetScaleFactor(scaleFactor);
    emit this->scaleFactorChanged();
    this->update();
}

auto dtkThreeDeeLinearExtrusionFilter::getScaleFactor() -> qreal {
    return this->m_object->GetScaleFactor();
}

auto dtkThreeDeeLinearExtrusionFilter::setCapping(bool capping) -> void {
    this->m_object->SetCapping(capping);
    emit this->cappingChanged();
    this->update();
}

auto dtkThreeDeeLinearExtrusionFilter::getCapping() -> bool {
    return this->m_object->GetCapping();
}

auto dtkThreeDeeLinearExtrusionFilter::getVector() -> dtkThreeDeeVector3* {
    return this->m_vector;
}

auto dtkThreeDeeLinearExtrusionFilter::getExtrusionPoint() -> dtkThreeDeeVector3* {
    return this->m_extrusionPoint;
}

//
// dtkThreeDee+LinearExtrusionFilter.cpp ends here
