// dtkThreeDee+SuperquadricSource.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+SuperquadricSource.hpp"

dtkThreeDeeSuperquadricSource::dtkThreeDeeSuperquadricSource() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkSuperquadricSource>::New()) {

    this->m_object = vtkSuperquadricSource::SafeDownCast(dtkThreeDeeAlgorithm::get());

    this->m_center = new dtkThreeDeeVector3([this](){
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->update();
    });

    this->m_scale = new dtkThreeDeeVector3([this](){
        this->m_object->SetScale(this->m_scale->getValues().data());
        this->update();
    });
}

auto dtkThreeDeeSuperquadricSource::setToroidal(bool toroidal) -> void {
    this->m_object->SetToroidal(toroidal);
    this->update();
    emit this->toroidalChanged();
}

auto dtkThreeDeeSuperquadricSource::getToroidal() -> bool {
    return this->m_object->GetToroidal();
}

auto dtkThreeDeeSuperquadricSource::setThickness(qreal thickness) -> void {
    this->m_object->SetThickness(thickness);
    this->update();
    emit this->thicknessChanged();
}

auto dtkThreeDeeSuperquadricSource::getThickness() -> qreal {
    return this->m_object->GetThickness();
}

auto dtkThreeDeeSuperquadricSource::setSize(qreal size) -> void {
    this->m_object->SetSize(size);
    this->update();
    emit this->sizeChanged();
}

auto dtkThreeDeeSuperquadricSource::getSize() -> qreal {
    return this->m_object->GetSize();
}

auto dtkThreeDeeSuperquadricSource::setPhiRoundness(qreal phiRoundness) -> void {
    this->m_object->SetPhiRoundness(phiRoundness);
    this->update();
    emit this->phiRoundnessChanged();
}

auto dtkThreeDeeSuperquadricSource::getPhiRoundness() -> qreal {
    return this->m_object->GetPhiRoundness();
}

auto dtkThreeDeeSuperquadricSource::setThetaRoundness(qreal thetaRoundness) -> void {
    this->m_object->SetThetaRoundness(thetaRoundness);
    this->update();
    emit this->thetaRoundnessChanged();
}

auto dtkThreeDeeSuperquadricSource::getThetaRoundness() -> qreal {
    return this->m_object->GetThetaRoundness();
}

auto dtkThreeDeeSuperquadricSource::setPhiResolution(qreal phiResolution) -> void {
    this->m_object->SetPhiResolution(phiResolution);
    this->update();
    emit this->phiResolutionChanged();
}

auto dtkThreeDeeSuperquadricSource::getPhiResolution() -> qreal {
    return this->m_object->GetPhiResolution();
}

auto dtkThreeDeeSuperquadricSource::setThetaResolution(qreal thetaResolution) -> void {
    this->m_object->SetThetaResolution(thetaResolution);
    this->update();
    emit this->thetaResolutionChanged();
}

auto dtkThreeDeeSuperquadricSource::getThetaResolution() -> qreal {
    return this->m_object->GetThetaResolution();
}

auto dtkThreeDeeSuperquadricSource::setAxisOfSymmetry(Axis axis) -> void {
    switch (axis) {
        case Axis::X: this->m_object->SetXAxisOfSymmetry(); break;
        case Axis::Y: this->m_object->SetYAxisOfSymmetry(); break;
        case Axis::Z: this->m_object->SetZAxisOfSymmetry(); break;
        default: break;
    }

    this->update();
    emit this->axisOfSymmetryChanged();
}

auto dtkThreeDeeSuperquadricSource::getAxisOfSymmetry() -> Axis {
    return this->m_axisOfSymmetry;
}

auto dtkThreeDeeSuperquadricSource::getCenter() -> dtkThreeDeeVector3* {
    return this->m_center;
}

auto dtkThreeDeeSuperquadricSource::getScale() -> dtkThreeDeeVector3* {
    return this->m_scale;
}

//
// dtkThreeDee+SuperquadricSource.cpp ends here
