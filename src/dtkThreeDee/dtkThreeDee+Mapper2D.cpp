// dtkThreeDee+Mapper2D.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Mapper2D.hpp"

dtkThreeDeeMapper2D::dtkThreeDeeMapper2D(vtkSmartPointer<vtkMapper2D> vtkObject) : dtkThreeDeeAbstractMapper(vtkObject) {
}

auto dtkThreeDeeMapper2D::get() -> vtkSmartPointer<vtkMapper2D> {
    return vtkMapper2D::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

//
// dtkThreeDee+Mapper2D.cpp ends here
