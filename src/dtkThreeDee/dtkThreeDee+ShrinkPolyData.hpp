// dtkThreeDee+ShrinkPolyData.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkShrinkPolyData.h>

class dtkThreeDeeShrinkPolyData : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(qreal shrinkFactor READ getShrinkFactor WRITE setShrinkFactor NOTIFY shrinkFactorChanged);

    QML_NAMED_ELEMENT(ShrinkPolyData)

private:
    vtkSmartPointer<vtkShrinkPolyData> m_object = nullptr;

public:
    dtkThreeDeeShrinkPolyData();
    auto setShrinkFactor(qreal) -> void;
    auto getShrinkFactor() -> qreal;

signals:
    void shrinkFactorChanged();
};

//
// dtkThreeDee+ShrinkPolyData.hpp ends here
