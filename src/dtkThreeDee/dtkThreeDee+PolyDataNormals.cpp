// dtkThreeDee+PolyDataNormals.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+PolyDataNormals.hpp"

dtkThreeDeePolyDataNormals::dtkThreeDeePolyDataNormals() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkPolyDataNormals>::New()) {
    this->m_object = vtkPolyDataNormals::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeePolyDataNormals::setFeatureAngle(qreal featureAngle) -> void {
    this->m_object->SetFeatureAngle(featureAngle);
    emit this->featureAngleChanged();
    this->update();
}

auto dtkThreeDeePolyDataNormals::getFeatureAngle() -> qreal {
    return this->m_object->GetFeatureAngle();
}

auto dtkThreeDeePolyDataNormals::setSplitting(bool splitting) -> void {
    this->m_object->SetSplitting(splitting);
    emit this->splittingChanged();
    this->update();
}

auto dtkThreeDeePolyDataNormals::getSplitting() -> bool {
    return this->m_object->GetSplitting();
}

auto dtkThreeDeePolyDataNormals::setConsistency(bool consistency) -> void {
    this->m_object->SetConsistency(consistency);
    emit this->consistencyChanged();
    this->update();
}

auto dtkThreeDeePolyDataNormals::getConsistency() -> bool {
    return this->m_object->GetConsistency();
}

auto dtkThreeDeePolyDataNormals::setAutoOrientNormals(bool autoOrientNormals) -> void {
    this->m_object->SetAutoOrientNormals(autoOrientNormals);
    emit this->autoOrientNormalsChanged();
    this->update();
}

auto dtkThreeDeePolyDataNormals::getAutoOrientNormals() -> bool {
    return this->m_object->GetAutoOrientNormals();
}

auto dtkThreeDeePolyDataNormals::setComputePointNormals(bool computePointNormals) -> void {
    this->m_object->SetComputePointNormals(computePointNormals);
    emit this->computePointNormalsChanged();
    this->update();
}

auto dtkThreeDeePolyDataNormals::getComputePointNormals() -> bool {
    return this->m_object->GetComputePointNormals();
}

auto dtkThreeDeePolyDataNormals::setComputeCellNormals(bool computeCellNormals) -> void {
    this->m_object->SetComputeCellNormals(computeCellNormals);
    emit this->computeCellNormalsChanged();
    this->update();
}

auto dtkThreeDeePolyDataNormals::getComputeCellNormals() -> bool {
    return this->m_object->GetComputeCellNormals();
}

auto dtkThreeDeePolyDataNormals::setFlipNormals(bool flipNormals) -> void {
    this->m_object->SetFlipNormals(flipNormals);
    emit this->flipNormalsChanged();
    this->update();
}

auto dtkThreeDeePolyDataNormals::getFlipNormals() -> bool {
    return this->m_object->GetFlipNormals();
}

auto dtkThreeDeePolyDataNormals::setNonManifoldTraversal(bool nonManifoldTraversal) -> void {
    this->m_object->SetNonManifoldTraversal(nonManifoldTraversal);
    emit this->nonManifoldTraversalChanged();
    this->update();
}

auto dtkThreeDeePolyDataNormals::getNonManifoldTraversal() -> bool {
    return this->m_object->GetNonManifoldTraversal();
}

//
// dtkThreeDee+PolyDataNormals.cpp ends here
