// dtkThreeDee+Vector3.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <array>
#include <functional>

#include <QObject>
#include <QQmlEngine>

class dtkThreeDeeVector3 : public QObject
{
    Q_OBJECT

    Q_PROPERTY(double x READ getX WRITE setX NOTIFY xChanged);
    Q_PROPERTY(double y READ getY WRITE setY NOTIFY yChanged);
    Q_PROPERTY(double z READ getZ WRITE setZ NOTIFY zChanged);

    QML_NAMED_ELEMENT(Vector3)
    QML_UNCREATABLE("")

public:
    using cb_t = std::function<void()>;
    using array_t = std::array<double, 3>;

public:
    dtkThreeDeeVector3(void) = delete;
    dtkThreeDeeVector3(cb_t&&, array_t = {{0,0,0}});

    double getX(void);
    double getY(void);
    double getZ(void);

    void setX(double);
    void setY(double);
    void setZ(double);

    array_t getValues(void);

signals:
    void xChanged(void);
    void yChanged(void);
    void zChanged(void);

private:
    cb_t m_callback;
    array_t m_values;
    void notify(void);
};

//
// dtkThreeDee+Vector3.hpp ends here
