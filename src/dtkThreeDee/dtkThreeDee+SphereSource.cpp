// dtkThreeDee+SphereSource.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+SphereSource.hpp"

dtkThreeDeeSphereSource::dtkThreeDeeSphereSource() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkSphereSource>::New()) {
    this->m_object = vtkSphereSource::SafeDownCast(dtkThreeDeeAlgorithm::get());

    this->m_center = new dtkThreeDeeVector3([this](){
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->update();
    });
}

auto dtkThreeDeeSphereSource::getCenter() -> dtkThreeDeeVector3* {
    return this->m_center;
}

auto dtkThreeDeeSphereSource::setLatLongTessellation(bool value) -> void {
    this->m_object->SetLatLongTessellation(value);
    emit this->latLongTessellationChanged();
    this->update();
}

auto dtkThreeDeeSphereSource::setRadius(qreal radius) -> void {
    this->m_object->SetRadius(radius);
    emit this->radiusChanged();
    this->update();
}

auto dtkThreeDeeSphereSource::setThetaResolution(int value) -> void {
    this->m_object->SetThetaResolution(value);
    emit this->thetaResolutionChanged();
    this->update();
}

auto dtkThreeDeeSphereSource::setPhiResolution(int value) -> void {
    this->m_object->SetPhiResolution(value);
    emit this->phiResolutionChanged();
    this->update();
}

auto dtkThreeDeeSphereSource::setStartTheta(qreal value) -> void {
    this->m_object->SetStartTheta(value);
    emit this->startThetaChanged();
    this->update();
}

auto dtkThreeDeeSphereSource::setStartPhi(qreal value) -> void {
    this->m_object->SetStartPhi(value);
    emit this->startPhiChanged();
    this->update();
}

auto dtkThreeDeeSphereSource::setEndTheta(qreal value) -> void {
    this->m_object->SetEndTheta(value);
    emit this->endThetaChanged();
    this->update();
}

auto dtkThreeDeeSphereSource::setEndPhi(qreal value) -> void {
    this->m_object->SetEndPhi(value);
    emit this->endPhiChanged();
    this->update();
}

auto dtkThreeDeeSphereSource::getLatLongTessellation() -> bool {
    return this->m_object->GetLatLongTessellation();
}

auto dtkThreeDeeSphereSource::getThetaResolution() -> int {
    return this->m_object->GetThetaResolution();
}

auto dtkThreeDeeSphereSource::getPhiResolution() -> int {
    return this->m_object->GetPhiResolution();
}

auto dtkThreeDeeSphereSource::getStartTheta() -> qreal {
    return this->m_object->GetStartTheta();
}

auto dtkThreeDeeSphereSource::getStartPhi() -> qreal {
    return this->m_object->GetStartPhi();
}

auto dtkThreeDeeSphereSource::getEndTheta() -> qreal {
    return this->m_object->GetEndTheta();
}

auto dtkThreeDeeSphereSource::getEndPhi() -> qreal {
    return this->m_object->GetEndPhi();
}

auto dtkThreeDeeSphereSource::getRadius() -> qreal {
    return this->m_object->GetRadius();
}

//
// dtkThreeDee+SphereSource.cpp ends here
