// dtkThreeDee+AbstractVolumeMapper.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+AbstractVolumeMapper.hpp"

dtkThreeDeeAbstractVolumeMapper::dtkThreeDeeAbstractVolumeMapper(vtkSmartPointer<vtkAbstractVolumeMapper> vtkObject) : dtkThreeDeeAbstractMapper3D(vtkObject)
{

}

vtkSmartPointer<vtkAbstractVolumeMapper> dtkThreeDeeAbstractVolumeMapper::get(void)
{
    return vtkAbstractVolumeMapper::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

//
// dtkThreeDee+AbstractVolumeMapper.cpp ends here
