// dtkThreeDee+ArrowSource.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkArrowSource.h>

class dtkThreeDeeArrowSource : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(int tipResolution READ getTipResolution WRITE setTipResolution NOTIFY tipResolutionChanged);
    Q_PROPERTY(qreal tipLength READ getTipLength WRITE setTipLength NOTIFY tipLengthChanged);
    Q_PROPERTY(qreal tipRadius READ getTipRadius WRITE setTipRadius NOTIFY tipRadiusChanged);
    Q_PROPERTY(int shaftResolution READ getShaftResolution WRITE setShaftResolution NOTIFY shaftResolutionChanged);
    Q_PROPERTY(qreal shaftRadius READ getShaftRadius WRITE setShaftRadius NOTIFY shaftRadiusChanged);
    Q_PROPERTY(bool invert READ getInvert WRITE setInvert NOTIFY invertChanged);

    QML_NAMED_ELEMENT(ArrowSource)

public:
    dtkThreeDeeArrowSource();
    auto setTipResolution(int) -> void;
    auto getTipResolution() -> int;
    auto setTipLength(qreal) -> void;
    auto getTipLength() -> qreal;
    auto setTipRadius(qreal) -> void;
    auto getTipRadius() -> qreal;
    auto setShaftResolution(int) -> void;
    auto getShaftResolution() -> int;
    auto setShaftRadius(qreal) -> void;
    auto getShaftRadius() -> qreal;
    auto setInvert(bool) -> void;
    auto getInvert() -> bool;

signals:
    void tipResolutionChanged();
    void tipLengthChanged();
    void tipRadiusChanged();
    void shaftResolutionChanged();
    void shaftRadiusChanged();
    void invertChanged();

private:
    vtkSmartPointer<vtkArrowSource> m_object = nullptr;
};

//
// dtkThreeDee+ArrowSource.hpp ends here
