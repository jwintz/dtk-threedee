// dtkThreeDee+WarpLens.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+WarpLens.hpp"

dtkThreeDeeWarpLens::dtkThreeDeeWarpLens() : dtkThreeDeePointSetAlgorithm(vtkSmartPointer<vtkWarpLens>::New()) {
    this->m_object = vtkWarpLens::SafeDownCast(dtkThreeDeeAlgorithm::get());

    this->m_center = new dtkThreeDeeVector2([this](){
        this->m_object->SetCenter(this->m_center->getX(), this->m_center->getY());
        this->update();
    });

    this->m_principalPoint = new dtkThreeDeeVector2([this](){
        this->m_object->SetPrincipalPoint(this->m_principalPoint->getX(), this->m_principalPoint->getY());
        this->update();
    });
}

auto dtkThreeDeeWarpLens::getCenter() -> dtkThreeDeeVector2* {
    return this->m_center;
}

auto dtkThreeDeeWarpLens::getPrincipalPoint() -> dtkThreeDeeVector2* {
    return this->m_principalPoint;
}

auto dtkThreeDeeWarpLens::setK1(qreal k1) -> void {
    this->m_object->SetK1(k1);
    emit this->k1Changed();
    this->update();
}

auto dtkThreeDeeWarpLens::getK1() -> qreal {
    return this->m_object->GetK1();
}

auto dtkThreeDeeWarpLens::setK2(qreal k2) -> void {
    this->m_object->SetK2(k2);
    emit this->k2Changed();
    this->update();
}

auto dtkThreeDeeWarpLens::getK2() -> qreal {
    return this->m_object->GetK2();
}

auto dtkThreeDeeWarpLens::setP1(qreal p1) -> void {
    this->m_object->SetP1(p1);
    emit this->p1Changed();
    this->update();
}

auto dtkThreeDeeWarpLens::getP1() -> qreal {
    return this->m_object->GetP1();
}

auto dtkThreeDeeWarpLens::setP2(qreal p2) -> void {
    this->m_object->SetP2(p2);
    emit this->p2Changed();
    this->update();
}

auto dtkThreeDeeWarpLens::getP2() -> qreal {
    return this->m_object->GetP2();
}

//
// dtkThreeDee+WarpLens.cpp ends here
