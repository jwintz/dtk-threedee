// dtkThreeDee+BooleanOperationPolyDataFilter.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+PolyDataAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkBooleanOperationPolyDataFilter.h>

class dtkThreeDeeBooleanOperationPolyDataFilter : public dtkThreeDeePolyDataAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(BooleanOperationPolyDataFilter)

public:
    enum Operation {
        Union = vtkBooleanOperationPolyDataFilter::VTK_UNION,
        Intersection = vtkBooleanOperationPolyDataFilter::VTK_INTERSECTION,
        Difference = vtkBooleanOperationPolyDataFilter::VTK_DIFFERENCE
    };

private:
    Q_ENUM(Operation);
    Q_PROPERTY(Operation operation READ getOperation WRITE setOperation NOTIFY operationChanged);
    Q_PROPERTY(qreal tolerance READ getTolerance WRITE setTolerance NOTIFY toleranceChanged);
    Q_PROPERTY(bool reorientDifferentCells READ getReorientDifferentCells WRITE setReorientDifferentCells NOTIFY reorientDifferentCellsChanged);

private:
    vtkSmartPointer<vtkBooleanOperationPolyDataFilter> m_object = nullptr;

public:
    dtkThreeDeeBooleanOperationPolyDataFilter();
    auto setOperation(Operation) -> void;
    auto getOperation() -> Operation;
    auto setTolerance(qreal) -> void;
    auto getTolerance() -> qreal;
    auto setReorientDifferentCells(bool) -> void;
    auto getReorientDifferentCells() -> bool;

signals:
    void operationChanged();
    void toleranceChanged();
    void reorientDifferentCellsChanged();
};

//
// dtkThreeDee+BooleanOperationPolyDataFilter.hpp ends here
