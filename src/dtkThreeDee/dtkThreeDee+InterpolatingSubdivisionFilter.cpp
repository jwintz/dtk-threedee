// dtkThreeDee+InterpolatingSubdivisionFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+InterpolatingSubdivisionFilter.hpp"

dtkThreeDeeInterpolatingSubdivisionFilter::dtkThreeDeeInterpolatingSubdivisionFilter(vtkSmartPointer<vtkInterpolatingSubdivisionFilter> vtkObject) : dtkThreeDeePolyDataAlgorithm(vtkObject)
{
    this->m_object = vtkObject;
}

auto dtkThreeDeeInterpolatingSubdivisionFilter::setNumberOfSubdivisions(int value) -> void
{
    this->m_object->SetNumberOfSubdivisions(value);
    emit this->numberOfSubdivisionsChanged();
    this->update();
}

auto dtkThreeDeeInterpolatingSubdivisionFilter::getNumberOfSubdivisions() -> int
{
    return this->m_object->GetNumberOfSubdivisions();
}

//
// dtkThreeDee+InterpolatingSubdivisionFilter.cpp ends here
