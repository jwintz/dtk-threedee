// dtkThreeDee+ImageLaplacian.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+ThreadedImageAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkImageLaplacian.h>

class dtkThreeDeeImageLaplacian : public dtkThreeDeeThreadedImageAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(ImageLaplacian)

public:
    dtkThreeDeeImageLaplacian();
};

//
// dtkThreeDee+ImageLaplacian.hpp ends here
