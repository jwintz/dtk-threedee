// dtkThreeDee+Utils.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include <QtGui>
#include <QtQml>
#include <QtQuick>

#include "dtkThreeDee+AbstractMapper.hpp"
#include "dtkThreeDee+AbstractMapper3D.hpp"
#include "dtkThreeDee+AbstractPolyDataReader.hpp"
#include "dtkThreeDee+AbstractVolumeMapper.hpp"
#include "dtkThreeDee+AbstractWidget.hpp"
#include "dtkThreeDee+Actor.hpp"
#include "dtkThreeDee+Actor2D.hpp"
#include "dtkThreeDee+Algorithm.hpp"
#include "dtkThreeDee+AdaptiveSubdivisionFilter.hpp"
#include "dtkThreeDee+ApproximatingSubdivisionFilter.hpp"
#include "dtkThreeDee+ArcSource.hpp"
#include "dtkThreeDee+ArrowSource.hpp"
#include "dtkThreeDee+BooleanOperationPolyDataFilter.hpp"
#include "dtkThreeDee+BoxWidget2.hpp"
#include "dtkThreeDee+ButterflySubdivisionFilter.hpp"
#include "dtkThreeDee+ColorTransferFunction.hpp"
#include "dtkThreeDee+Cone.hpp"
#include "dtkThreeDee+ConeSource.hpp"
#include "dtkThreeDee+ContourFilter.hpp"
#include "dtkThreeDee+Cutter.hpp"
#include "dtkThreeDee+CylinderSource.hpp"
#include "dtkThreeDee+DataReader.hpp"
#include "dtkThreeDee+DataSetAlgorithm.hpp"
#include "dtkThreeDee+DataSetMapper.hpp"
#include "dtkThreeDee+DecimatePro.hpp"
#include "dtkThreeDee+DiskSource.hpp"
#include "dtkThreeDee+DensifyPolyData.hpp"
#include "dtkThreeDee+DICOMImageReader.hpp"
#include "dtkThreeDee+EllipseArcSource.hpp"
#include "dtkThreeDee+ExtractEdges.hpp"
#include "dtkThreeDee+ExtractSurface.hpp"
#include "dtkThreeDee+FboOffscreenWindow.hpp"
#include "dtkThreeDee+Glyph3D.hpp"
#include "dtkThreeDee+GPUVolumeRayCastMapper.hpp"
#include "dtkThreeDee+HedgeHog.hpp"
#include "dtkThreeDee+ImageAlgorithm.hpp"
#include "dtkThreeDee+ImageDataGeometryFilter.hpp"
#include "dtkThreeDee+ImageLaplacian.hpp"
#include "dtkThreeDee+ImageMapper.hpp"
#include "dtkThreeDee+ImageReader2.hpp"
#include "dtkThreeDee+ImplicitFunction.hpp"
#include "dtkThreeDee+InterpolatingSubdivisionFilter.hpp"
#include "dtkThreeDee+LinearExtrusionFilter.hpp"
#include "dtkThreeDee+LinearSubdivisionFilter.hpp"
#include "dtkThreeDee+LoopSubdivisionFilter.hpp"
#include "dtkThreeDee+Mapper.hpp"
#include "dtkThreeDee+Mapper2D.hpp"
#include "dtkThreeDee+MinimalStandardRandomSequence.hpp"
#include "dtkThreeDee+Object.hpp"
#include "dtkThreeDee+OBJReader.hpp"
#include "dtkThreeDee+PerlinNoise.hpp"
#include "dtkThreeDee+PiecewiseFunction.hpp"
#include "dtkThreeDee+Plane.hpp"
#include "dtkThreeDee+PlatonicSolidSource.hpp"
#include "dtkThreeDee+PLYReader.hpp"
#include "dtkThreeDee+PNGReader.hpp"
#include "dtkThreeDee+PointSetAlgorithm.hpp"
#include "dtkThreeDee+PointSource.hpp"
#include "dtkThreeDee+PolyDataAlgorithm.hpp"
#include "dtkThreeDee+PolyDataMapper.hpp"
#include "dtkThreeDee+PolyDataNormals.hpp"
#include "dtkThreeDee+Prop.hpp"
#include "dtkThreeDee+Prop3D.hpp"
#include "dtkThreeDee+Property.hpp"
#include "dtkThreeDee+Property2D.hpp"
#include "dtkThreeDee+Quadric.hpp"
#include "dtkThreeDee+RandomSequence.hpp"
#include "dtkThreeDee+ReverseSense.hpp"
#include "dtkThreeDee+RibbonFilter.hpp"
#include "dtkThreeDee+RotationalExtrusionFilter.hpp"
#include "dtkThreeDee+SampleFunction.hpp"
#include "dtkThreeDee+SectorSource.hpp"
#include "dtkThreeDee+ShrinkPolyData.hpp"
#include "dtkThreeDee+SmartVolumeMapper.hpp"
#include "dtkThreeDee+SmoothPolyDataFilter.hpp"
#include "dtkThreeDee+Sphere.hpp"
#include "dtkThreeDee+SphereSource.hpp"
#include "dtkThreeDee+STLReader.hpp"
#include "dtkThreeDee+Stripper.hpp"
#include "dtkThreeDee+StructuredPointsReader.hpp"
#include "dtkThreeDee+SuperquadricSource.hpp"
#include "dtkThreeDee+TextSource.hpp"
#include "dtkThreeDee+Texture.hpp"
#include "dtkThreeDee+TextureMapToCylinder.hpp"
#include "dtkThreeDee+ThreadedImageAlgorithm.hpp"
#include "dtkThreeDee+TriangleFilter.hpp"
#include "dtkThreeDee+TubeFilter.hpp"
#include "dtkThreeDee+UnstructuredGridReader.hpp"
#include "dtkThreeDee+Vector2.hpp"
#include "dtkThreeDee+Vector3.hpp"
#include "dtkThreeDee+VectorText.hpp"
#include "dtkThreeDee+VertexGlyphFilter.hpp"
#include "dtkThreeDee+Viewer.hpp"
#include "dtkThreeDee+Volume.hpp"
#include "dtkThreeDee+VolumeMapper.hpp"
#include "dtkThreeDee+VolumeProperty.hpp"
#include "dtkThreeDee+WarpLens.hpp"
#include "dtkThreeDee+WarpScalar.hpp"
#include "dtkThreeDee+WarpTo.hpp"

#include <stdlib.h>
#include <stdio.h>

void dtkThreeDeeInitialise(void)
{
    // qmlRegisterUncreatableType<dtkThreeDeeAbstractMapper>("dtkThreeDee", 1, 0, "AbstractMapper", "");
    // qmlRegisterUncreatableType<dtkThreeDeeAbstractMapper3D>("dtkThreeDee", 1, 0, "AbstractMapper3D", "");
    // qmlRegisterUncreatableType<dtkThreeDeeAbstractPolyDataReader>("dtkThreeDee", 1, 0, "AbstractPolyDataReader", "");
    // qmlRegisterUncreatableType<dtkThreeDeeAbstractVolumeMapper>("dtkThreeDee", 1, 0, "AbstractVolumeMapper", "");
    // qmlRegisterUncreatableType<dtkThreeDeeAbstractWidget>("dtkThreeDee", 1, 0, "AbstractWidget", "");
    // qmlRegisterUncreatableType<dtkThreeDeeAlgorithm>("dtkThreeDee", 1, 0, "Algorithm", "");
    // qmlRegisterUncreatableType<dtkThreeDeeApproximatingSubdivisionFilter>("dtkThreeDee", 1, 0, "ApproximatingSubdivisionFilter", "");
    // qmlRegisterUncreatableType<dtkThreeDeeColorTransferFunction>("dtkThreeDee", 1, 0, "ColorTransferFunction", "");
    // qmlRegisterUncreatableType<dtkThreeDeeDataReader>("dtkThreeDee", 1, 0, "DataReader", "");
    // qmlRegisterUncreatableType<dtkThreeDeeDataSetAlgorithm>("dtkThreeDee", 1, 0, "DataSetAlgorithm", "");
    // qmlRegisterUncreatableType<dtkThreeDeeImageAlgorithm>("dtkThreeDee", 1, 0, "ImageAlgorithm", "");
    // qmlRegisterUncreatableType<dtkThreeDeeImageReader2>("dtkThreeDee", 1, 0, "ImageReader2", "");
    // qmlRegisterUncreatableType<dtkThreeDeeImplicitFunction>("dtkThreeDee", 1, 0, "ImplicitFunction", "");
    // qmlRegisterUncreatableType<dtkThreeDeeInterpolatingSubdivisionFilter>("dtkThreeDee", 1, 0, "InterpolatingSubdivisionFilter", "");
    // qmlRegisterUncreatableType<dtkThreeDeeMapper>("dtkThreeDee", 1, 0, "Mapper", "");
    // qmlRegisterUncreatableType<dtkThreeDeeMapper2D>("dtkThreeDee", 1, 0, "Mapper2D", "");
    // qmlRegisterUncreatableType<dtkThreeDeeObject>("dtkThreeDee", 1, 0, "Object", "");
    // qmlRegisterUncreatableType<dtkThreeDeePiecewiseFunction>("dtkThreeDee", 1, 0, "PiecewiseFunction", "");
    // qmlRegisterUncreatableType<dtkThreeDeePointSetAlgorithm>("dtkThreeDee", 1, 0, "PointSetAlgorithm", "");
    // qmlRegisterUncreatableType<dtkThreeDeePolyDataAlgorithm>("dtkThreeDee", 1, 0, "PolyDataAlgorithm", "");
    // qmlRegisterUncreatableType<dtkThreeDeeProp>("dtkThreeDee", 1, 0, "Prop", "");
    // qmlRegisterUncreatableType<dtkThreeDeeProp3D>("dtkThreeDee", 1, 0, "Prop3D", "");
    // qmlRegisterUncreatableType<dtkThreeDeeProperty>("dtkThreeDee", 1, 0, "Property", "");
    // qmlRegisterUncreatableType<dtkThreeDeeProperty2D>("dtkThreeDee", 1, 0, "Property2D", "");
    // qmlRegisterUncreatableType<dtkThreeDeeRandomSequence>("dtkThreeDee", 1, 0, "RandomSequence", "");
    // qmlRegisterUncreatableType<dtkThreeDeeTexture>("dtkThreeDee", 1, 0, "Texture", "");
    // qmlRegisterUncreatableType<dtkThreeDeeThreadedImageAlgorithm>("dtkThreeDee", 1, 0, "ThreadedImageAlgorithm", "");
    // qmlRegisterUncreatableType<dtkThreeDeeVector2>("dtkThreeDee", 1, 0, "Vector2", "");
    // qmlRegisterUncreatableType<dtkThreeDeeVector3>("dtkThreeDee", 1, 0, "Vector3", "");
    // qmlRegisterUncreatableType<dtkThreeDeeVolumeMapper>("dtkThreeDee", 1, 0, "VolumeMapper", "");
    // qmlRegisterUncreatableType<dtkThreeDeeVolumeProperty>("dtkThreeDee", 1, 0, "VolumeProperty", "");

    // qmlRegisterType<dtkThreeDeeActor>("dtkThreeDee", 1, 0, "Actor");
    // qmlRegisterType<dtkThreeDeeActor2D>("dtkThreeDee", 1, 0, "Actor2D");
    // qmlRegisterType<dtkThreeDeeAdaptiveSubdivisionFilter>("dtkThreeDee", 1, 0, "AdaptiveSubdivisionFilter");
    // qmlRegisterType<dtkThreeDeeArcSource>("dtkThreeDee", 1, 0, "ArcSource");
    // qmlRegisterType<dtkThreeDeeArrowSource>("dtkThreeDee", 1, 0, "ArrowSource");
    // qmlRegisterType<dtkThreeDeeBooleanOperationPolyDataFilter>("dtkThreeDee", 1, 0, "BooleanOperationPolyDataFilter");
    // qmlRegisterType<dtkThreeDeeBoxWidget2>("dtkThreeDee", 1, 0, "BoxWidget2");
    // qmlRegisterType<dtkThreeDeeButterflySubdivisionFilter>("dtkThreeDee", 1, 0, "ButterflySubdivisionFilter");
    // qmlRegisterType<dtkThreeDeeCone>("dtkThreeDee", 1, 0, "Cone");
    // qmlRegisterType<dtkThreeDeeConeSource>("dtkThreeDee", 1, 0, "ConeSource");
    // qmlRegisterType<dtkThreeDeeContourFilter>("dtkThreeDee", 1, 0, "ContourFilter");
    // qmlRegisterType<dtkThreeDeeCutter>("dtkThreeDee", 1, 0, "Cutter");
    // qmlRegisterType<dtkThreeDeeCylinderSource>("dtkThreeDee", 1, 0, "CylinderSource");
    // qmlRegisterType<dtkThreeDeeDataSetMapper>("dtkThreeDee", 1, 0, "DataSetMapper");
    // qmlRegisterType<dtkThreeDeeDecimatePro>("dtkThreeDee", 1, 0, "DecimatePro");
    // qmlRegisterType<dtkThreeDeeDensifyPolyData>("dtkThreeDee", 1, 0, "DensifyPolyData");
    // qmlRegisterType<dtkThreeDeeDiskSource>("dtkThreeDee", 1, 0, "DiskSource");
    // qmlRegisterType<dtkThreeDeeDICOMImageReader>("dtkThreeDee", 1, 0, "DICOMImageReader");
    // qmlRegisterType<dtkThreeDeeEllipseArcSource>("dtkThreeDee", 1, 0, "EllipseArcSource");
    // qmlRegisterType<dtkThreeDeeExtractEdges>("dtkThreeDee", 1, 0, "ExtractEdges");
    // qmlRegisterType<dtkThreeDeeExtractSurface>("dtkThreeDee", 1, 0, "ExtractSurface");
    // qmlRegisterType<dtkThreeDeeGlyph3D>("dtkThreeDee", 1, 0, "Glyph3D");
    // qmlRegisterType<dtkThreeDeeGPUVolumeRayCastMapper>("dtkThreeDee", 1, 0, "GPUVolumeRayCastMapper");
    // qmlRegisterType<dtkThreeDeeHedgeHog>("dtkThreeDee", 1, 0, "HedgeHog");
    // qmlRegisterType<dtkThreeDeeImageDataGeometryFilter>("dtkThreeDee", 1, 0, "ImageDataGeometryFilter");
    // qmlRegisterType<dtkThreeDeeImageLaplacian>("dtkThreeDee", 1, 0, "ImageLaplacian");
    // qmlRegisterType<dtkThreeDeeImageMapper>("dtkThreeDee", 1, 0, "ImageMapper");
    // qmlRegisterType<dtkThreeDeeLinearExtrusionFilter>("dtkThreeDee", 1, 0, "LinearExtrusionFilter");
    // qmlRegisterType<dtkThreeDeeLinearSubdivisionFilter>("dtkThreeDee", 1, 0, "LinearSubdivisionFilter");
    // qmlRegisterType<dtkThreeDeeLoopSubdivisionFilter>("dtkThreeDee", 1, 0, "LoopSubdivisionFilter");
    // qmlRegisterType<dtkThreeDeeMinimalStandardRandomSequence>("dtkThreeDee", 1, 0, "MinimalStandardRandomSequence");
    // qmlRegisterType<dtkThreeDeeOBJReader>("dtkThreeDee", 1, 0, "OBJReader");
    // qmlRegisterType<dtkThreeDeePerlinNoise>("dtkThreeDee", 1, 0, "PerlinNoise");
    // qmlRegisterType<dtkThreeDeePlane>("dtkThreeDee", 1, 0, "Plane");
    // qmlRegisterType<dtkThreeDeePlatonicSolidSource>("dtkThreeDee", 1, 0, "PlatonicSolidSource");
    // qmlRegisterType<dtkThreeDeePLYReader>("dtkThreeDee", 1, 0, "PLYReader");
    // qmlRegisterType<dtkThreeDeePNGReader>("dtkThreeDee", 1, 0, "PNGReader");
    // qmlRegisterType<dtkThreeDeePointSource>("dtkThreeDee", 1, 0, "PointSource");
    // qmlRegisterType<dtkThreeDeePolyDataMapper>("dtkThreeDee", 1, 0, "PolyDataMapper");
    // qmlRegisterType<dtkThreeDeePolyDataNormals>("dtkThreeDee", 1, 0, "PolyDataNormals");
    // qmlRegisterType<dtkThreeDeeQuadric>("dtkThreeDee", 1, 0, "Quadric");
    // qmlRegisterType<dtkThreeDeeReverseSense>("dtkThreeDee", 1, 0, "ReverseSense");
    // qmlRegisterType<dtkThreeDeeRibbonFilter>("dtkThreeDee", 1, 0, "RibbonFilter");
    // qmlRegisterType<dtkThreeDeeRotationalExtrusionFilter>("dtkThreeDee", 1, 0, "RotationalExtrusionFilter");
    // qmlRegisterType<dtkThreeDeeSampleFunction>("dtkThreeDee", 1, 0, "SampleFunction");
    // qmlRegisterType<dtkThreeDeeSectorSource>("dtkThreeDee", 1, 0, "SectorSource");
    // qmlRegisterType<dtkThreeDeeShrinkPolyData>("dtkThreeDee", 1, 0, "ShrinkPolyData");
    // qmlRegisterType<dtkThreeDeeSmartVolumeMapper>("dtkThreeDee", 1, 0, "SmartVolumeMapper");
    // qmlRegisterType<dtkThreeDeeSmoothPolyDataFilter>("dtkThreeDee", 1, 0, "SmoothPolyDataFilter");
    // qmlRegisterType<dtkThreeDeeSphere>("dtkThreeDee", 1, 0, "Sphere");
    // qmlRegisterType<dtkThreeDeeSphereSource>("dtkThreeDee", 1, 0, "SphereSource");
    // qmlRegisterType<dtkThreeDeeSTLReader>("dtkThreeDee", 1, 0, "STLReader");
    // qmlRegisterType<dtkThreeDeeStripper>("dtkThreeDee", 1, 0, "Stripper");
    // qmlRegisterType<dtkThreeDeeStructuredPointsReader>("dtkThreeDee", 1, 0, "StructuredPointsReader");
    // qmlRegisterType<dtkThreeDeeSuperquadricSource>("dtkThreeDee", 1, 0, "SuperquadricSource");
    // qmlRegisterType<dtkThreeDeeTextSource>("dtkThreeDee", 1, 0, "TextSource");
    // qmlRegisterType<dtkThreeDeeTextureMapToCylinder>("dtkThreeDee", 1, 0, "TextureMapToCylinder");
    // qmlRegisterType<dtkThreeDeeTriangleFilter>("dtkThreeDee", 1, 0, "TriangleFilter");
    // qmlRegisterType<dtkThreeDeeTubeFilter>("dtkThreeDee", 1, 0, "TubeFilter");
    // qmlRegisterType<dtkThreeDeeUnstructuredGridReader>("dtkThreeDee", 1, 0, "UnstructuredGridReader");
    // qmlRegisterType<dtkThreeDeeVectorText>("dtkThreeDee", 1, 0, "VectorText");
    // qmlRegisterType<dtkThreeDeeVertexGlyphFilter>("dtkThreeDee", 1, 0, "VertexGlyphFilter");
    // qmlRegisterType<dtkThreeDeeViewer>("dtkThreeDee", 1, 0, "Viewer");
    // qmlRegisterType<dtkThreeDeeVolume>("dtkThreeDee", 1, 0, "Volume");
    // qmlRegisterType<dtkThreeDeeWarpLens>("dtkThreeDee", 1, 0, "WarpLens");
    // qmlRegisterType<dtkThreeDeeWarpScalar>("dtkThreeDee", 1, 0, "WarpScalar");
    // qmlRegisterType<dtkThreeDeeWarpTo>("dtkThreeDee", 1, 0, "WarpTo");

    // qDebug() << Q_FUNC_INFO << "HERE";

#if QT_VERSION >= 0x060000
    QQuickWindow::setGraphicsApi(QSGRendererInterface::OpenGL);
#endif

    dtkThreeDeeFboOffscreenWindow::setupGraphicsBackend();

    setenv("QML_BAD_GUI_RENDER_LOOP", "1", 1);
    setenv("QML_USE_GLYPHCACHE_WORKAROUND", "1", 1);
}

// Q_COREAPP_STARTUP_FUNCTION(dtkThreeDeeInitialise);

//
// dtkThreeDee+Utils.cpp ends here
