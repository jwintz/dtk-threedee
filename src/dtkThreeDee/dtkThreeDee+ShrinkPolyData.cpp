// dtkThreeDee+ShrinkPolyData.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ShrinkPolyData.hpp"

dtkThreeDeeShrinkPolyData::dtkThreeDeeShrinkPolyData() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkShrinkPolyData>::New()) {
    this->m_object = vtkShrinkPolyData::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeShrinkPolyData::setShrinkFactor(qreal val) -> void {
    this->m_object->SetShrinkFactor(val);
    emit this->shrinkFactorChanged();
    this->update();
}

auto dtkThreeDeeShrinkPolyData::getShrinkFactor() -> qreal {
    return this->m_object->GetShrinkFactor();
}

//
// dtkThreeDee+ShrinkPolyData.cpp ends here
