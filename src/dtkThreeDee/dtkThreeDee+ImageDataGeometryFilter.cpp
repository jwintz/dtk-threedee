// dtkThreeDee+ImageDataGeometryFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ImageDataGeometryFilter.hpp"

dtkThreeDeeImageDataGeometryFilter::dtkThreeDeeImageDataGeometryFilter() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkImageDataGeometryFilter>::New())
{
    this->m_object = vtkImageDataGeometryFilter::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

//
// dtkThreeDee+ImageDataGeometryFilter.cpp ends here
