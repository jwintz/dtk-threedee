// dtkThreeDee+WarpTo.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QtCore>
#include <QQmlEngine>

#include "dtkThreeDee+PointSetAlgorithm.hpp"
#include "dtkThreeDee+Vector3.hpp"

#include <vtkWarpTo.h>

class dtkThreeDeeWarpTo : public dtkThreeDeePointSetAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(dtkThreeDeeVector3 *position READ getPosition CONSTANT);
    Q_PROPERTY(qreal scaleFactor READ getScaleFactor WRITE setScaleFactor NOTIFY scaleFactorChanged);
    Q_PROPERTY(bool absolute READ getAbsolute WRITE setAbsolute NOTIFY absoluteChanged);

    QML_NAMED_ELEMENT(WarpTo)

private:
    vtkSmartPointer<vtkWarpTo> m_object = nullptr;
    dtkThreeDeeVector3 *m_position = nullptr;

public:
    dtkThreeDeeWarpTo();
    auto getPosition() -> dtkThreeDeeVector3 *;
    auto setScaleFactor(qreal) -> void;
    auto getScaleFactor() -> qreal;
    auto setAbsolute(bool) -> void;
    auto getAbsolute() -> bool;

signals:
    void scaleFactorChanged();
    void absoluteChanged();
};

//
// dtkThreeDee+WarpTo.hpp ends here
