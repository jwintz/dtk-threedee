// dtkThreeDee+VolumeProperty.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QColor>
#include <QObject>
#include <QQmlEngine>

#include <vtkSmartPointer.h>
#include <vtkVolumeProperty.h>
#include <vtkVolume.h>

class dtkThreeDeeVolume;
class dtkThreeDeePiecewiseFunction;
class dtkThreeDeeColorTransferFunction;

class dtkThreeDeeVolumeProperty : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool shade READ getShade WRITE setShade NOTIFY shadeChanged);
    Q_PROPERTY(dtkThreeDeePiecewiseFunction* scalarOpacityFunction READ getScalarOpacityFunction CONSTANT);
    Q_PROPERTY(dtkThreeDeePiecewiseFunction* gradientOpacityFunction READ getGradientOpacityFunction CONSTANT);
    Q_PROPERTY(dtkThreeDeeColorTransferFunction* transferFunction READ getTransferFunction CONSTANT);
    Q_PROPERTY(qreal ambient READ getAmbient WRITE setAmbient NOTIFY ambientChanged);
    Q_PROPERTY(qreal diffuse READ getDiffuse WRITE setDiffuse NOTIFY diffuseChanged);
    Q_PROPERTY(qreal specular READ getSpecular WRITE setSpecular NOTIFY specularChanged);
    Q_PROPERTY(qreal specularPower READ getSpecularPower WRITE setSpecularPower NOTIFY specularPowerChanged);

    QML_NAMED_ELEMENT(VolumeProperty)
    QML_UNCREATABLE("")

private:
    dtkThreeDeeVolume* m_volume = nullptr;
    dtkThreeDeePiecewiseFunction* m_gradientOpacityFunction = nullptr;
    dtkThreeDeePiecewiseFunction* m_scalarOpacityFunction = nullptr;
    dtkThreeDeeColorTransferFunction* m_transferFunction = nullptr;
    vtkSmartPointer<vtkVolume> m_volume_ptr = nullptr;

private:
    void update();

public:
    dtkThreeDeeVolumeProperty() = delete;
    dtkThreeDeeVolumeProperty(dtkThreeDeeVolume*);
    auto setShade(bool) -> void;
    auto getShade() -> bool;
    auto setAmbient(qreal) -> void;
    auto getAmbient() -> qreal;
    auto setDiffuse(qreal) -> void;
    auto getDiffuse() -> qreal;
    auto setSpecular(qreal) -> void;
    auto getSpecular() -> qreal;
    auto setSpecularPower(qreal) -> void;
    auto getSpecularPower() -> qreal;
    auto getGradientOpacityFunction() -> dtkThreeDeePiecewiseFunction*;
    auto getScalarOpacityFunction() -> dtkThreeDeePiecewiseFunction*;
    auto getTransferFunction() -> dtkThreeDeeColorTransferFunction*;

signals:
    void shadeChanged();
    void ambientChanged();
    void diffuseChanged();
    void specularChanged();
    void specularPowerChanged();
};

//
// dtkThreeDee+VolumeProperty.hpp ends here
