// dtkThreeDee+Property2D.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QColor>
#include <QObject>
#include <QQmlEngine>

#include <vtkSmartPointer.h>
#include <vtkProperty2D.h>
#include <vtkActor2D.h>

class dtkThreeDeeActor2D;

class dtkThreeDeeProperty2D : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QColor color READ getColor WRITE setColor NOTIFY colorChanged);
    Q_PROPERTY(qreal opacity READ getOpacity WRITE setOpacity NOTIFY opacityChanged);

    QML_NAMED_ELEMENT(Property2D)
    QML_UNCREATABLE("")

private:
    QColor m_color;
    dtkThreeDeeActor2D* m_actor2D = nullptr;
    vtkSmartPointer<vtkActor2D> m_vtkActor2D = nullptr;

private:
    void update();

public:
    dtkThreeDeeProperty2D() = delete;
    dtkThreeDeeProperty2D(dtkThreeDeeActor2D*);
    auto setColor(const QColor&) -> void;
    auto getColor() -> QColor;
    auto setOpacity(qreal) -> void;
    auto getOpacity() -> qreal;

signals:
    void colorChanged();
    void opacityChanged();
};

//
// dtkThreeDee+Property2D.hpp ends here
