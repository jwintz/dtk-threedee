// dtkThreeDee+Actor.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Mapper.hpp"
#include "dtkThreeDee+Property.hpp"
#include "dtkThreeDee+Prop3D.hpp"
#include "dtkThreeDee+Texture.hpp"

#include <QQmlEngine>

#include <vtkActor.h>

class dtkThreeDeeActor : public dtkThreeDeeProp3D
{
    Q_OBJECT

    Q_PROPERTY(dtkThreeDeeTexture* texture READ getTexture CONSTANT);
    Q_PROPERTY(dtkThreeDeeProperty* property READ getProperty CONSTANT);
    Q_PROPERTY(dtkThreeDeeMapper* mapper READ getMapper WRITE setMapper NOTIFY mapperChanged);

    Q_CLASSINFO("DefaultProperty", "mapper");

    QML_NAMED_ELEMENT(Actor)

public:
     dtkThreeDeeActor(void);
     dtkThreeDeeActor(vtkSmartPointer<vtkActor>);
    ~dtkThreeDeeActor(void);

    auto setVtkActor(vtkSmartPointer<vtkActor>) -> void;
    auto setMapper(dtkThreeDeeMapper*) -> void;

    dtkThreeDeeMapper *getMapper(void);
    dtkThreeDeeProperty *getProperty(void);
    dtkThreeDeeTexture *getTexture(void);
    vtkSmartPointer<vtkActor> get(void);

signals:
    void mapperChanged(void);

private:
    dtkThreeDeeProperty* m_property = nullptr;
    dtkThreeDeeTexture* m_texture = nullptr;
    dtkThreeDeeMapper* m_mapper = nullptr;

private:
    vtkSmartPointer<vtkActor> m_object = nullptr;
};

//
// dtkThreeDee+Actor.hpp ends here
