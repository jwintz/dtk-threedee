// dtkThreeDee+ImageLaplacian.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ImageLaplacian.hpp"

dtkThreeDeeImageLaplacian::dtkThreeDeeImageLaplacian() : dtkThreeDeeThreadedImageAlgorithm(vtkSmartPointer<vtkImageLaplacian>::New())
{

}

//
// dtkThreeDee+ImageLaplacian.cpp ends here
