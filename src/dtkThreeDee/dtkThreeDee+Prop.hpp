// dtkThreeDee+Prop.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Object.hpp"

#include <QQmlEngine>

#include <vtkSmartPointer.h>
#include <vtkProp.h>

class dtkThreeDeeViewer;

class dtkThreeDeeProp : public dtkThreeDeeObject
{
    Q_OBJECT

    Q_PROPERTY(bool visibility READ getVisibility WRITE setVisibility NOTIFY visibilityChanged);

    QML_NAMED_ELEMENT(Prop)
    QML_UNCREATABLE("")

public:
             dtkThreeDeeProp(void) = delete;
             dtkThreeDeeProp(vtkSmartPointer<vtkProp>);
    virtual ~dtkThreeDeeProp(void);

public:
    void update(void);
    void setVisibility(bool);
    bool getVisibility(void);
    void linkViewer(dtkThreeDeeViewer*);
    void unlinkViewer(dtkThreeDeeViewer*);
    vtkSmartPointer<vtkProp> get(void);

signals:
    void visibilityChanged(void);

private:
    bool m_initialized;
    vtkSmartPointer<vtkProp> m_object = nullptr;
    QList<dtkThreeDeeViewer*> m_viewers;
};

//
// dtkThreeDee+Prop.hpp ends here
