// dtkThreeDee+RandomSequence.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Object.hpp"

#include <QQmlEngine>

#include <vtkRandomSequence.h>
#include <vtkSmartPointer.h>

class dtkThreeDeeRandomSequence : public dtkThreeDeeObject
{
    Q_OBJECT

    Q_PROPERTY(int seed READ getSeed WRITE setSeed NOTIFY seedChanged);

    QML_NAMED_ELEMENT(RandomSequence)
    QML_UNCREATABLE("")

private:
    vtkSmartPointer<vtkRandomSequence> m_object = nullptr;
    int m_seed;

public:
    dtkThreeDeeRandomSequence() = delete;
    dtkThreeDeeRandomSequence(vtkSmartPointer<vtkRandomSequence>);
    auto get() -> vtkSmartPointer<vtkRandomSequence>;
    auto setSeed(int) -> void;
    auto getSeed() -> int;
    auto initialize() -> void;

signals:
    void seedChanged();
};

//
// dtkThreeDee+RandomSequence.hpp ends here
