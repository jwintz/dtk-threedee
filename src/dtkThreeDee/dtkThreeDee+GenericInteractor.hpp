#pragma once

#include <vtkGenericRenderWindowInteractor.h>

class dtkThreeDeeGenericInteractor : public vtkGenericRenderWindowInteractor
{
public:
    vtkTypeMacro(dtkThreeDeeGenericInteractor, vtkGenericRenderWindowInteractor);
    static dtkThreeDeeGenericInteractor *New(void);
    void Initialize(void) override;

protected:
             dtkThreeDeeGenericInteractor(void) = default;
    virtual ~dtkThreeDeeGenericInteractor(void) = default;
};
