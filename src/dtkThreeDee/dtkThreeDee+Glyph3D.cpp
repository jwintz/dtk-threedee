// dtkThreeDee+Glyph3D.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Glyph3D.hpp"

dtkThreeDeeGlyph3D::dtkThreeDeeGlyph3D() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkGlyph3D>::New()) {
    this->m_object = vtkGlyph3D::SafeDownCast(dtkThreeDeeAlgorithm::get());

    this->m_range = new dtkThreeDeeVector2([this](){
        this->m_object->SetRange(this->m_range->getX(), this->m_range->getY());
        this->update();
    });
}

auto dtkThreeDeeGlyph3D::getRange() -> dtkThreeDeeVector2* {
    return this->m_range;
}

auto dtkThreeDeeGlyph3D::setScaleMode(ScaleMode scaleMode) -> void {
    this->m_object->SetScaleMode(scaleMode);
    emit this->scaleModeChanged();
    this->update();
}

auto dtkThreeDeeGlyph3D::getScaleMode() -> dtkThreeDeeGlyph3D::ScaleMode {
    return (dtkThreeDeeGlyph3D::ScaleMode) this->m_object->GetScaleMode();
}

auto dtkThreeDeeGlyph3D::setColorMode(ColorMode colorMode) -> void {
    this->m_object->SetColorMode(colorMode);
    emit this->colorModeChanged();
    this->update();
}

auto dtkThreeDeeGlyph3D::getColorMode() -> ColorMode {
    return static_cast<ColorMode>(this->m_object->GetColorMode());
}

auto dtkThreeDeeGlyph3D::setIndexMode(IndexMode indexMode) -> void {
    this->m_object->SetIndexMode(indexMode);
    emit this->indexModeChanged();
    this->update();
}

auto dtkThreeDeeGlyph3D::getIndexMode() -> IndexMode {
    return static_cast<IndexMode>(this->m_object->GetIndexMode());
}

auto dtkThreeDeeGlyph3D::setVectorMode(VectorMode vectorMode) -> void {
    this->m_object->SetVectorMode(vectorMode);
    emit this->vectorModeChanged();
    this->update();
}

auto dtkThreeDeeGlyph3D::getVectorMode() -> VectorMode {
    return static_cast<VectorMode>(this->m_object->GetVectorMode());
}

auto dtkThreeDeeGlyph3D::setScaleFactor(qreal scaleFactor) -> void {
    this->m_object->SetScaleFactor(scaleFactor);
    emit this->scaleFactorChanged();
    this->update();
}

auto dtkThreeDeeGlyph3D::getScaleFactor() -> qreal {
    return this->m_object->GetScaleFactor();
}

auto dtkThreeDeeGlyph3D::setOrient(bool orient) -> void {
    this->m_object->SetOrient(orient);
    emit this->orientChanged();
    this->update();
}

auto dtkThreeDeeGlyph3D::getOrient() -> bool {
    return this->m_object->GetOrient();
}

auto dtkThreeDeeGlyph3D::setClamping(bool clamping) -> void {
    this->m_object->SetClamping(clamping);
    emit this->clampingChanged();
    this->update();
}

auto dtkThreeDeeGlyph3D::getClamping() -> bool {
    return this->m_object->GetClamping();
}

auto dtkThreeDeeGlyph3D::setScaling(bool scaling) -> void {
    this->m_object->SetScaling(scaling);
    emit this->scalingChanged();
    this->update();
}

auto dtkThreeDeeGlyph3D::getScaling() -> bool {
    return this->m_object->GetScaling();
}

auto dtkThreeDeeGlyph3D::setFillCellData(bool fillCellData) -> void {
    this->m_object->SetFillCellData(fillCellData);
    emit this->fillCellDataChanged();
    this->update();
}

auto dtkThreeDeeGlyph3D::getFillCellData() -> bool {
    return this->m_object->GetFillCellData();
}

auto dtkThreeDeeGlyph3D::setGeneratePointIds(bool generatePointIds) -> void {
    this->m_object->SetGeneratePointIds(generatePointIds);
    emit this->generatePointIdsChanged();
    this->update();
}

auto dtkThreeDeeGlyph3D::getGeneratePointIds() -> bool {
    return this->m_object->GetGeneratePointIds();
}

//
// dtkThreeDee+Glyph3D.cpp ends here
