// dtkThreeDee+ExtractSurface.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ExtractSurface.hpp"

dtkThreeDeeExtractSurface::dtkThreeDeeExtractSurface() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkExtractSurface>::New())
{
    this->m_object = vtkExtractSurface::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeExtractSurface::setRadius(qreal radius) -> void {
    this->m_object->SetRadius(radius);
    this->update();
    emit this->radiusChanged();
}

auto dtkThreeDeeExtractSurface::getRadius() -> qreal {
    return this->m_object->GetRadius();
}

auto dtkThreeDeeExtractSurface::setHoleFilling(bool holeFilling) -> void {
    this->m_object->SetHoleFilling(holeFilling);
    this->update();
    emit this->holeFillingChanged();
}

auto dtkThreeDeeExtractSurface::getHoleFilling() -> bool {
    return this->m_object->GetHoleFilling();
}

auto dtkThreeDeeExtractSurface::setComputeNormals(bool computeNormals) -> void {
    this->m_object->SetComputeNormals(computeNormals);
    this->update();
    emit this->computeNormalsChanged();
}

auto dtkThreeDeeExtractSurface::getComputeNormals() -> bool {
    return this->m_object->GetComputeNormals();
}

auto dtkThreeDeeExtractSurface::setComputeGradients(bool computeGradients) -> void {
    this->m_object->SetComputeGradients(computeGradients);
    this->update();
    emit this->computeGradientsChanged();
}

auto dtkThreeDeeExtractSurface::getComputeGradients() -> bool {
    return this->m_object->GetComputeGradients();
}

//
// dtkThreeDee+ExtractSurface.cpp ends here
