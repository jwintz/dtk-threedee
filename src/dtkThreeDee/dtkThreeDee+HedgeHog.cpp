// dtkThreeDee+HedgeHog.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+HedgeHog.hpp"

dtkThreeDeeHedgeHog::dtkThreeDeeHedgeHog() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkHedgeHog>::New()) {
    this->m_object = vtkHedgeHog::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeHedgeHog::setScaleFactor(qreal scaleFactor) -> void {
    this->m_object->SetScaleFactor(scaleFactor);
    emit this->scaleFactorChanged();
    this->update();
}

auto dtkThreeDeeHedgeHog::getScaleFactor() -> qreal {
    return this->m_object->GetScaleFactor();
}

auto dtkThreeDeeHedgeHog::setVectorMode(VectorMode vectorMode) -> void {
    this->m_object->SetVectorMode(vectorMode);
    emit this->vectorModeChanged();
    this->update();
}

auto dtkThreeDeeHedgeHog::getVectorMode() -> VectorMode {
    return static_cast<VectorMode>(this->m_object->GetVectorMode());
}

//
// dtkThreeDee+HedgeHog.cpp ends here
