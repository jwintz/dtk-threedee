// dtkThreeDee+Mapper2D.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include <QQmlEngine>

#include <vtkMapper2D.h>

#include "dtkThreeDee+AbstractMapper.hpp"

class dtkThreeDeeMapper2D : public dtkThreeDeeAbstractMapper
{
    Q_OBJECT

    QML_NAMED_ELEMENT(Mapper2D)
    QML_UNCREATABLE("")

public:
    dtkThreeDeeMapper2D(vtkSmartPointer<vtkMapper2D>);

public:
    auto get() -> vtkSmartPointer<vtkMapper2D>;
};

//
// dtkThreeDee+Mapper2D.hpp ends here
