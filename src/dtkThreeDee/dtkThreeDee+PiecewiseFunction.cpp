// dtkThreeDee+PiecewiseFunction.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+PiecewiseFunction.hpp"

dtkThreeDeePiecewiseFunction::dtkThreeDeePiecewiseFunction(vtk_t vtkObject, cb_t&& callback) : m_object(vtkObject), m_callback(callback)
{

}

auto dtkThreeDeePiecewiseFunction::update() -> void
{
    this->m_callback.operator()();
}

void dtkThreeDeePiecewiseFunction::clear()
{
    this->m_xValues.clear();
    this->m_yValues.clear();

    emit this->sizeChanged();
    this->m_object->RemoveAllPoints();
    this->update();
}

void dtkThreeDeePiecewiseFunction::add(double x, double y)
{
    this->m_xValues.append(x);
    this->m_yValues.append(y);

    emit this->sizeChanged();
    this->m_object->AddPoint(x, y);
    this->update();
}

auto dtkThreeDeePiecewiseFunction::getX(int i) -> double
{
    return i < this->m_xValues.length() && i >= 0 ? this->m_xValues.at(i) : 0;
}

auto dtkThreeDeePiecewiseFunction::getY(int i) -> double
{
    return i < this->m_yValues.length() && i >= 0 ? this->m_yValues.at(i) : 0;
}

auto dtkThreeDeePiecewiseFunction::setClamping(bool clamping) -> void
{
    this->m_object->SetClamping(clamping);
    this->update();
}

auto dtkThreeDeePiecewiseFunction::getClamping() -> bool
{
    return this->m_object->GetClamping();
}

auto dtkThreeDeePiecewiseFunction::getSize() -> int
{
    return this->m_xValues.length();
}

//
// dtkThreeDee+PiecewiseFunction.cpp ends here
