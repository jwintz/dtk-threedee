// dtkThreeDee+EllipseArcSource.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+EllipseArcSource.hpp"

dtkThreeDeeEllipseArcSource::dtkThreeDeeEllipseArcSource() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkEllipseArcSource>::New())
{
    this->m_object = vtkEllipseArcSource::SafeDownCast(dtkThreeDeeAlgorithm::get());

    this->m_center = new dtkThreeDeeVector3([this]() {
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->update();
    });

    this->m_normal = new dtkThreeDeeVector3([this]() {
        this->m_object->SetNormal(this->m_normal->getValues().data());
        this->update();
    });

    this->m_majorRadiusVector = new dtkThreeDeeVector3([this]() {
        this->m_object->SetMajorRadiusVector(this->m_majorRadiusVector->getValues().data());
        this->update();
    });
}

auto dtkThreeDeeEllipseArcSource::getCenter() -> dtkThreeDeeVector3* {
    return this->m_center;
}

auto dtkThreeDeeEllipseArcSource::getNormal() -> dtkThreeDeeVector3* {
    return this->m_normal;
}

auto dtkThreeDeeEllipseArcSource::getMajorRadiusVector() -> dtkThreeDeeVector3* {
    return this->m_majorRadiusVector;
}

auto dtkThreeDeeEllipseArcSource::setStartAngle(qreal startAngle) -> void {
    this->m_object->SetStartAngle(startAngle);
    emit this->startAngleChanged();
    this->update();
}

auto dtkThreeDeeEllipseArcSource::getStartAngle() -> qreal {
    return this->m_object->GetStartAngle();
}

auto dtkThreeDeeEllipseArcSource::setSegmentAngle(qreal segmentAngle) -> void {
    this->m_object->SetSegmentAngle(segmentAngle);
    emit this->segmentAngleChanged();
    this->update();
}

auto dtkThreeDeeEllipseArcSource::getSegmentAngle() -> qreal {
    return this->m_object->GetSegmentAngle();
}

auto dtkThreeDeeEllipseArcSource::setResolution(int resolution) -> void {
    this->m_object->SetResolution(resolution);
    emit this->resolutionChanged();
    this->update();
}

auto dtkThreeDeeEllipseArcSource::getResolution() -> int {
    return this->m_object->GetResolution();
}

auto dtkThreeDeeEllipseArcSource::setRatio(qreal ratio) -> void {
    this->m_object->SetRatio(ratio);
    emit this->ratioChanged();
    this->update();
}

auto dtkThreeDeeEllipseArcSource::getRatio() -> qreal {
    return this->m_object->GetRatio();
}

//
// dtkThreeDee+EllipseArcSource.cpp ends here
