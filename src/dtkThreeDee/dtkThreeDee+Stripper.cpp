// dtkThreeDee+Stripper.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Stripper.hpp"

dtkThreeDeeStripper::dtkThreeDeeStripper() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkStripper>::New()) {
    this->m_object = vtkStripper::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeStripper::setMaximumLength(int maximumLength) -> void {
    this->m_object->SetMaximumLength(maximumLength);
    this->update();
    emit this->passThroughCellIdsChanged();
}

auto dtkThreeDeeStripper::getMaximumLength() -> int {
    return this->m_object->GetMaximumLength();
}

auto dtkThreeDeeStripper::setPassCellDataAsFieldData(bool passCellDataAsFieldData) -> void {
    this->m_object->SetPassCellDataAsFieldData(passCellDataAsFieldData);
    this->update();
    emit this->passCellDataAsFieldDataChanged();
}

auto dtkThreeDeeStripper::getPassCellDataAsFieldData() -> bool {
    return this->m_object->GetPassCellDataAsFieldData();
}

auto dtkThreeDeeStripper::setPassThroughCellIds(bool passThroughCellIds) -> void {
    this->m_object->SetPassThroughCellIds(passThroughCellIds);
    this->update();
    emit this->passThroughCellIdsChanged();
}

auto dtkThreeDeeStripper::getPassThroughCellIds() -> bool {
    return this->m_object->GetPassThroughCellIds();
}

auto dtkThreeDeeStripper::setPassThroughPointIds(bool passThroughPointIds) -> void {
    this->m_object->SetPassThroughPointIds(passThroughPointIds);
    this->update();
    emit this->passThroughPointIdsChanged();
}

auto dtkThreeDeeStripper::getPassThroughPointIds() -> bool {
    return this->m_object->GetPassThroughPointIds();
}

auto dtkThreeDeeStripper::setJoinContiguousSegments(bool joinContiguousSegments) -> void {
    this->m_object->SetJoinContiguousSegments(joinContiguousSegments);
    this->update();
    emit this->joinContiguousSegmentsChanged();
}

auto dtkThreeDeeStripper::getJoinContiguousSegments() -> bool {
    return this->m_object->GetJoinContiguousSegments();
}

//
// dtkThreeDee+Stripper.cpp ends here
