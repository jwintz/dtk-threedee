// dtkThreeDee+Cutter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Cutter.hpp"

#include "dtkThreeDee+ImplicitFunction.hpp"

dtkThreeDeeCutter::dtkThreeDeeCutter() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkCutter>::New())
{
    this->m_object = vtkCutter::SafeDownCast(dtkThreeDeeAlgorithm::get());
    this->m_cutFunctionCb = [this] () {
        this->updateCutFunction();
    };
}

dtkThreeDeeCutter::~dtkThreeDeeCutter() {
    if (this->m_cutFunction) {
        this->m_cutFunction->removeCallback(std::move(this->m_cutFunctionCb));
    }
}

auto dtkThreeDeeCutter::updateCutFunction() -> void {
    if (this->m_cutFunction) {
        this->update();
    }
}

auto dtkThreeDeeCutter::setCutFunction(dtkThreeDeeImplicitFunction* cutFunction) -> void {
    if (this->m_cutFunction) {
        this->m_cutFunction->removeCallback(std::move(this->m_cutFunctionCb));
    }

    this->m_cutFunction = cutFunction;

    if (cutFunction) {
        this->m_object->SetCutFunction(cutFunction->get());
        cutFunction->addCallback(std::move(this->m_cutFunctionCb));
        this->updateCutFunction();
    }

    emit this->cutFunctionChanged();
}

auto dtkThreeDeeCutter::getCutFunction() -> dtkThreeDeeImplicitFunction* {
    return this->m_cutFunction;
}

//
// dtkThreeDee+Cutter.cpp ends here
