// dtkThreeDee+ImageReader2.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include <QtCore>

#include "dtkThreeDee+ImageReader2.hpp"

dtkThreeDeeImageReader2::dtkThreeDeeImageReader2(vtkSmartPointer<vtkImageReader2> vtkObject) : dtkThreeDeeImageAlgorithm(vtkObject) {
    this->m_object = vtkObject;
}

auto dtkThreeDeeImageReader2::isValid() -> bool {
    if (!QFileInfo::exists(this->m_fileName)) {
        return false;
    }

    return true;
}

auto dtkThreeDeeImageReader2::setFileName(const QString& fileName) -> void {
    this->m_fileName = fileName;

    emit this->fileNameChanged();

    if (QFileInfo::exists(fileName)) {
        this->m_object->SetFileName(fileName.toStdString().c_str());
        this->m_object->Update();
        this->update();
    }
}

auto dtkThreeDeeImageReader2::getFileName() -> QString {
    return this->m_fileName;
}

//
// dtkThreeDee+ImageReader2.cpp ends here
