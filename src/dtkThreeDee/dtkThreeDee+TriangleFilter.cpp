// dtkThreeDee+TriangleFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+TriangleFilter.hpp"

dtkThreeDeeTriangleFilter::dtkThreeDeeTriangleFilter() : dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkTriangleFilter>::New()) {
    this->m_object = vtkTriangleFilter::SafeDownCast(dtkThreeDeeAlgorithm::get());
}

auto dtkThreeDeeTriangleFilter::setPassVerts(bool pass) -> void {
    this->m_object->SetPassVerts(pass);
    this->update();
    emit this->passVertsChanged();
}

auto dtkThreeDeeTriangleFilter::setPassLines(bool pass) -> void {
    this->m_object->SetPassLines(pass);
    this->update();
    emit this->passLinesChanged();
}

auto dtkThreeDeeTriangleFilter::getPassVerts() -> bool {
    return this->m_object->GetPassVerts();
}

auto dtkThreeDeeTriangleFilter::getPassLines() -> bool {
    return this->m_object->GetPassLines();
}

//
// dtkThreeDee+TriangleFilter.cpp ends here
