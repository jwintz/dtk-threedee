// dtkThreeDee+ApproximatingSubdivisionFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ApproximatingSubdivisionFilter.hpp"

dtkThreeDeeApproximatingSubdivisionFilter::dtkThreeDeeApproximatingSubdivisionFilter(vtkSmartPointer<vtkApproximatingSubdivisionFilter> vtkObject) : dtkThreeDeePolyDataAlgorithm(vtkObject), m_object(vtkObject)
{
}

auto dtkThreeDeeApproximatingSubdivisionFilter::setNumberOfSubdivisions(int subdivisions) -> void
{
    this->m_object->SetNumberOfSubdivisions(subdivisions);
    emit this->numberOfSubdivisionsChanged();
    this->update();
}

auto dtkThreeDeeApproximatingSubdivisionFilter::getNumberOfSubdivisions() -> int
{
    return this->m_object->GetNumberOfSubdivisions();
}

//
// dtkThreeDee+ApproximatingSubdivisionFilter.cpp ends here
