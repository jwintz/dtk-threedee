// dtkThreeDee+Property.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Property.hpp"
#include "dtkThreeDee+Actor.hpp"

dtkThreeDeeProperty::dtkThreeDeeProperty(dtkThreeDeeActor* actor) : m_actor(actor), m_vtkActor(actor->get())
{

}

auto dtkThreeDeeProperty::update() -> void {
    this->m_actor->update();
}

auto dtkThreeDeeProperty::setDiffuseColor(const QColor& color) -> void {
    this->m_diffuseColor = color;

    this->m_vtkActor->GetProperty()->SetDiffuseColor(color.redF(), color.greenF(), color.blueF());
    this->update();

    emit this->diffuseColorChanged();
}

auto dtkThreeDeeProperty::setSpecularColor(const QColor& color) -> void {
    this->m_specularColor = color;

    this->m_vtkActor->GetProperty()->SetSpecularColor(color.redF(), color.greenF(), color.blueF());
    this->update();

    emit this->specularColorChanged();
}

auto dtkThreeDeeProperty::setAmbientColor(const QColor& color) -> void {
    this->m_ambientColor = color;

    this->m_vtkActor->GetProperty()->SetAmbientColor(color.redF(), color.greenF(), color.blueF());
    this->update();

    emit this->ambientColorChanged();
}

auto dtkThreeDeeProperty::setEdgeColor(const QColor& color) -> void {
    this->m_edgeColor = color;

    this->m_vtkActor->GetProperty()->SetEdgeColor(color.redF(), color.greenF(), color.blueF());
    this->update();

    emit this->edgeColorChanged();
}

auto dtkThreeDeeProperty::setFrontfaceCulling(bool enabled) -> void {
    this->m_vtkActor->GetProperty()->SetFrontfaceCulling(enabled);
    emit this->frontfaceCullingChanged();
    this->update();
}

auto dtkThreeDeeProperty::setBackfaceCulling(bool enabled) -> void {
    this->m_vtkActor->GetProperty()->SetBackfaceCulling(enabled);
    emit this->backfaceCullingChanged();
    this->update();
}

auto dtkThreeDeeProperty::setEdgeVisibility(bool visible) -> void {
    this->m_vtkActor->GetProperty()->SetEdgeVisibility(visible);
    this->edgeVisibilityChanged();
    this->update();
}

auto dtkThreeDeeProperty::setLighting(bool lighting) -> void {
    this->m_vtkActor->GetProperty()->SetLighting(lighting);
    emit this->lightingChanged();
    this->update();
}

auto dtkThreeDeeProperty::setShading(bool shading) -> void {
    this->m_vtkActor->GetProperty()->SetShading(shading);
    emit shadingChanged();
    this->update();
}

void dtkThreeDeeProperty::setOpacity(qreal opacity) {
    this->m_vtkActor->GetProperty()->SetOpacity(opacity);
    emit this->opacityChanged();
    this->update();
}

auto dtkThreeDeeProperty::setLineWidth(qreal lineWidth) -> void {
    this->m_vtkActor->GetProperty()->SetLineWidth(lineWidth);
    emit this->lineWidthChanged();
    this->update();
}

auto dtkThreeDeeProperty::setPointSize(qreal pointSize) -> void {
    this->m_vtkActor->GetProperty()->SetPointSize(pointSize);
    emit this->pointSizeChanged();
    this->update();
}

auto dtkThreeDeeProperty::setInterpolation(Interpolation interpolation) -> void {
    this->m_vtkActor->GetProperty()->SetInterpolation(interpolation);
    emit this->interpolationChanged();
    this->update();
}

auto dtkThreeDeeProperty::getInterpolation() -> Interpolation {
    return static_cast<Interpolation>(this->m_vtkActor->GetProperty()->GetInterpolation());
}

auto dtkThreeDeeProperty::setRepresentation(Representation representation) -> void {
    this->m_vtkActor->GetProperty()->SetRepresentation(representation);
    emit this->representationChanged();
    this->update();
}

auto dtkThreeDeeProperty::getRepresentation() -> Representation {
    return static_cast<Representation>(this->m_vtkActor->GetProperty()->GetRepresentation());
}

auto dtkThreeDeeProperty::setAmbient(qreal ambient) -> void {
    this->m_vtkActor->GetProperty()->SetAmbient(ambient);
    emit this->ambientChanged();
    this->update();
}

auto dtkThreeDeeProperty::setDiffuse(qreal diffuse) -> void {
    this->m_vtkActor->GetProperty()->SetDiffuse(diffuse);
    emit this->diffuseChanged();
    this->update();
}

auto dtkThreeDeeProperty::setSpecular(qreal specular) -> void {
    this->m_vtkActor->GetProperty()->SetSpecular(specular);
    emit this->specularChanged();
    this->update();
}

auto dtkThreeDeeProperty::setSpecularPower(qreal specularPower) -> void {
    this->m_vtkActor->GetProperty()->SetSpecularPower(specularPower);
    emit this->specularPowerChanged();
    this->update();
}

auto dtkThreeDeeProperty::getDiffuseColor() -> QColor {
    return this->m_diffuseColor;
}

auto dtkThreeDeeProperty::getSpecularColor() -> QColor {
    return this->m_specularColor;
}

auto dtkThreeDeeProperty::getAmbientColor() -> QColor {
    return this->m_ambientColor;
}

auto dtkThreeDeeProperty::getEdgeColor() -> QColor {
    return this->m_edgeColor;
}

auto dtkThreeDeeProperty::getEdgeVisibility() -> bool {
    return this->m_vtkActor->GetProperty()->GetEdgeVisibility();
}

auto dtkThreeDeeProperty::getLighting() -> bool {
    return this->m_vtkActor->GetProperty()->GetLighting();
}

auto dtkThreeDeeProperty::getShading() -> bool {
    return this->m_vtkActor->GetProperty()->GetShading();
}

auto dtkThreeDeeProperty::getFrontfaceCulling() -> bool {
    return this->m_vtkActor->GetProperty()->GetFrontfaceCulling();
}

auto dtkThreeDeeProperty::getBackfaceCulling() -> bool {
    return this->m_vtkActor->GetProperty()->GetBackfaceCulling();
}

auto dtkThreeDeeProperty::getOpacity() -> qreal {
    return this->m_vtkActor->GetProperty()->GetOpacity();
}

auto dtkThreeDeeProperty::getLineWidth() -> qreal {
    return this->m_vtkActor->GetProperty()->GetLineWidth();
}

auto dtkThreeDeeProperty::getPointSize() -> qreal {
    return this->m_vtkActor->GetProperty()->GetPointSize();
}

auto dtkThreeDeeProperty::getAmbient() -> qreal {
    return this->m_vtkActor->GetProperty()->GetAmbient();
}

auto dtkThreeDeeProperty::getDiffuse() -> qreal {
    return this->m_vtkActor->GetProperty()->GetDiffuse();
}

auto dtkThreeDeeProperty::getSpecular() -> qreal {
    return this->m_vtkActor->GetProperty()->GetSpecular();
}

auto dtkThreeDeeProperty::getSpecularPower() -> qreal {
    return this->m_vtkActor->GetProperty()->GetSpecularPower();
}

//
// dtkThreeDee+Property.cpp ends here
