// dtkThreeDee+TextureMapToCylinder.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+DataSetAlgorithm.hpp"

#include <QQmlEngine>

#include <vtkTextureMapToCylinder.h>

class dtkThreeDeeTextureMapToCylinder : public dtkThreeDeeDataSetAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(TextureMapToCylinder)

private:
    vtkSmartPointer<vtkTextureMapToCylinder> m_object = nullptr;

public:
    dtkThreeDeeTextureMapToCylinder();
};

//
// dtkThreeDee+TextureMapToCylinder.hpp ends here
