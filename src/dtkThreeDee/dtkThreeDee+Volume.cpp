// dtkThreeDee+Volume.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+Volume.hpp"
#include "dtkThreeDee+AbstractVolumeMapper.hpp"
#include "dtkThreeDee+VolumeProperty.hpp"

dtkThreeDeeVolume::dtkThreeDeeVolume() : dtkThreeDeeProp3D(vtkSmartPointer<vtkVolume>::New())
{
    this->m_volume = vtkVolume::SafeDownCast(dtkThreeDeeProp::get());
    this->m_property = new dtkThreeDeeVolumeProperty(this);
}

dtkThreeDeeVolume::~dtkThreeDeeVolume() {
    delete this->m_property;
}

auto dtkThreeDeeVolume::get() -> vtkSmartPointer<vtkVolume> {
    return this->m_volume;
}

auto dtkThreeDeeVolume::setMapper(dtkThreeDeeAbstractVolumeMapper* mapper) -> void {
    this->m_mapper = mapper;
    this->m_volume->SetMapper(mapper->get());
    mapper->setProp(this);

    emit this->mapperChanged();

    this->update();
}

auto dtkThreeDeeVolume::getMapper() -> dtkThreeDeeAbstractVolumeMapper* {
    return this->m_mapper;
}

auto dtkThreeDeeVolume::getProperty() -> dtkThreeDeeVolumeProperty* {
    return this->m_property;
}

//
// dtkThreeDee+Volume.cpp ends here
