// dtkThreeDee+PolyDataAlgorithm.hpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#pragma once

#include "dtkThreeDee+Algorithm.hpp"

#include <QQmlEngine>

#include <vtkPolyDataAlgorithm.h>

class dtkThreeDeePolyDataAlgorithm : public dtkThreeDeeAlgorithm
{
    Q_OBJECT

    QML_NAMED_ELEMENT(PolyDataAlgorithm)
    QML_UNCREATABLE("")

public:
    dtkThreeDeePolyDataAlgorithm(vtkSmartPointer<vtkPolyDataAlgorithm>);
};

//
// dtkThreeDee+PolyDataAlgorithm.hpp ends here
