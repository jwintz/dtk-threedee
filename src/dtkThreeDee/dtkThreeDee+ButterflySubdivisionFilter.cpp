// dtkThreeDee+ButterflySubdivisionFilter.cpp ---
//
//
//

// Commentary:
//
//
//
//

// Change Log:
//
//
//

// Code:

#include "dtkThreeDee+ButterflySubdivisionFilter.hpp"

dtkThreeDeeButterflySubdivisionFilter::dtkThreeDeeButterflySubdivisionFilter() : dtkThreeDeeInterpolatingSubdivisionFilter(vtkSmartPointer<vtkButterflySubdivisionFilter>::New())
{

}

//
// dtkThreeDee+ButterflySubdivisionFilter.cpp ends here
